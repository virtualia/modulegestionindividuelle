﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="VMenuIcones.ascx.vb" Inherits="Virtualia.Net.Controles_VMenuIcones" %>

<%@ Register Src="VCoupleIconeEti.ascx" TagName="VIcone" TagPrefix="Virtualia" %>

<asp:Panel ID="CadreMenu" runat="server" BackColor="#0F2F30" HorizontalAlign="Center" Width="1146px"
    BorderColor="#5E9598" BorderStyle="Solid" BorderWidth="2px" Height="500px" ScrollBars="Auto"
    Style="margin-top: 0px; text-align: left;">
    <asp:Table ID="CadreChoix" runat="server" HorizontalAlign="Left">
        <asp:TableRow>
            <asp:TableCell>
                <asp:Table ID="CadreTuile01" runat="server" HorizontalAlign="Left">
                    <asp:TableRow>
                        <asp:TableCell Width="310px" Height="150px">
                            <Virtualia:VIcone ID="Cmd01" runat="server" IconeBackColor="#1C5151" IconeURL="~/Images/General/VirImag_BleuVert.jpg"
                                EtiText="Gestion détaillée des effectifs" IconeTooltip="Gestion détaillée des effectifs et calcul des ETP"
                                NavigateURL="~/Fenetres/Module/FrmETPMasseSal.aspx" ParametreURL="VUE" ParametreFRM="Exec" VIndex="1" />
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell Width="310px" Height="150px">
                            <Virtualia:VIcone ID="Cmd02" runat="server" IconeBackColor="#1C5151" IconeURL="~/Images/General/VirImag_BleuVertGris.jpg"
                                EtiText="Synthèse des effectifs et bilan social" IconeTooltip="Synthèse des effectifs et bilan social"
                                NavigateURL="~/Fenetres/Module/FrmETPMasseSal.aspx" ParametreURL="VUE" ParametreFRM="Synthese" VIndex="2" />
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell Width="310px" Height="150px">
                            <Virtualia:VIcone ID="Cmd03" runat="server" IconeBackColor="#1C5151" IconeURL="~/Images/General/VirImag_Bleu.jpg"
                                EtiText="Synthèse des rémunérations" IconeTooltip="Synthèse des rémunérations"
                                NavigateURL="~/Fenetres/Module/FrmETPMasseSal.aspx" ParametreURL="VUE" ParametreFRM="Syntremu" VIndex="3" />
                        </asp:TableCell>
                    </asp:TableRow>
                </asp:Table>
            </asp:TableCell>
            <asp:TableCell>
                <asp:Table ID="CadreTuile02" runat="server" HorizontalAlign="Left">
                    <asp:TableRow>
                        <asp:TableCell Width="310px" Height="150px">
                            <Virtualia:VIcone ID="Cmd04" runat="server" IconeBackColor="#0E5F5C" IconeURL="~/Images/General/VirImag_BleuGris.jpg"
                                EtiText="Analyse des flux" IconeTooltip="Analyse des flux"
                                NavigateURL="~/Fenetres/Module/FrmETPMasseSal.aspx" ParametreURL="VUE" ParametreFRM="Flux" VIndex="4" />
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell Width="310px" Height="150px">
                            <Virtualia:VIcone ID="Cmd05" runat="server" IconeBackColor="#0E5F5C" IconeURL="~/Images/General/VirImag_Violet.jpg"
                                EtiText="Approche comptable par les bulletins de paie" IconeTooltip=""
                                NavigateURL="~/Fenetres/Module/FrmDepenses.aspx" ParametreURL="FRM" ParametreFRM="" VIndex="5" />
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell Width="310px" Height="150px">
                            <Virtualia:VIcone ID="Cmd06" runat="server" IconeBackColor="#0E5F5C" IconeURL="~/Images/General/VirImag_VioletGris.jpg"
                                SiVisible="False" EtiText="Préparation de la Paie" IconeTooltip=""
                                NavigateURL="~/Fenetres/Module/FrmPrepaGapaie.aspx" ParametreURL="FRM" ParametreFRM="" VIndex="6" />
                        </asp:TableCell>
                    </asp:TableRow>
                </asp:Table>
            </asp:TableCell>
            <asp:TableCell>
                <asp:Table ID="CadreTuile03" runat="server" HorizontalAlign="Left">
                    <asp:TableRow>
                        <asp:TableCell Width="310px" Height="150px">
                            <Virtualia:VIcone ID="Cmd07" runat="server" IconeBackColor="#137A76" IconeURL="~/Images/General/VirImag_Mauve.jpg"
                                SiVisible="True" EtiText="Quitter" IconeTooltip=""
                                NavigateURL="" ParametreURL="ID" ParametreFRM="" VIndex="0" />
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell Width="310px" Height="150px">
                            <Virtualia:VIcone ID="Cmd08" runat="server" IconeBackColor="#137A76" IconeURL="~/Images/General/VirImag_MauveGris.jpg"
                                SiVisible="False" EtiText="" IconeTooltip=""
                                NavigateURL="" ParametreURL="ID" ParametreFRM="" VIndex="8" />
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell Width="310px" Height="150px">
                            <Virtualia:VIcone ID="Cmd09" runat="server" IconeBackColor="#137A76" IconeURL="~/Images/General/VirImag_Rose.jpg"
                                SiVisible="False" EtiText="Quitter" IconeTooltip=""
                                NavigateURL="" ParametreURL="ID" ParametreFRM="" VIndex="0" />
                        </asp:TableCell>
                    </asp:TableRow>
                </asp:Table>
            </asp:TableCell>
            <asp:TableCell>
                <asp:Table ID="CadreTuile04" runat="server" HorizontalAlign="Left">
                    <asp:TableRow>
                        <asp:TableCell Width="310px" Height="150px">
                            <Virtualia:VIcone ID="Cmd10" runat="server" IconeBackColor="#19968D" IconeURL="~/Images/General/VirImag_RoseGris.jpg"
                                SiVisible="False" EtiText="" IconeTooltip=""
                                NavigateURL="" ParametreURL="ID" ParametreFRM="" VIndex="10" />
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell Width="310px" Height="150px">
                            <Virtualia:VIcone ID="Cmd11" runat="server" IconeBackColor="#19968D" IconeURL="~/Images/General/VirImag_Rouge.jpg"
                                SiVisible="False" EtiText="" IconeTooltip=""
                                NavigateURL="" ParametreURL="ID" ParametreFRM="" VIndex="11" />
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell Width="310px" Height="150px">
                            <Virtualia:VIcone ID="Cmd12" runat="server" IconeBackColor="#19968D" IconeURL="~/Images/General/VirImag_RougeGris.jpg"
                                SiVisible="False" EtiText="" IconeTooltip=""
                                NavigateURL="" ParametreURL="ID" ParametreFRM="" VIndex="12" />
                        </asp:TableCell>
                    </asp:TableRow>
                </asp:Table>
            </asp:TableCell>
            <asp:TableCell>
                <asp:Table ID="CadreTuile05" runat="server" HorizontalAlign="Left">
                    <asp:TableRow>
                        <asp:TableCell Width="310px" Height="150px">
                            <Virtualia:VIcone ID="Cmd13" runat="server" IconeURL="~/Images/General/VirImag_Violet.jpg"
                                SiVisible="False" EtiText="" IconeTooltip=""
                                NavigateURL="" ParametreURL="ID" ParametreFRM="" VIndex="13" />
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell Width="310px" Height="150px">
                            <Virtualia:VIcone ID="Cmd14" runat="server" IconeURL="~/Images/General/VirImag_Orange.jpg"
                                SiVisible="False" EtiText="" IconeTooltip=""
                                NavigateURL="" ParametreURL="ID" ParametreFRM="" VIndex="14" />
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell Width="310px" Height="150px">
                            <Virtualia:VIcone ID="Cmd15" runat="server" IconeURL="~/Images/General/VirImag_Bronze.jpg"
                                SiVisible="False" EtiText="" IconeTooltip=""
                                NavigateURL="" ParametreURL="ID" ParametreFRM="" VIndex="15" />
                        </asp:TableCell>
                    </asp:TableRow>
                </asp:Table>
            </asp:TableCell>
            <asp:TableCell>
                <asp:Table ID="CadreTuile06" runat="server" HorizontalAlign="Left">
                    <asp:TableRow>
                        <asp:TableCell Width="310px" Height="150px">
                            <Virtualia:VIcone ID="Cmd16" runat="server" IconeURL="~/Images/General/VirImag_VioletGris.jpg"
                                SiVisible="False" EtiText="" IconeTooltip=""
                                NavigateURL="" ParametreURL="ID" ParametreFRM="" VIndex="16" />
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell Width="310px" Height="150px">
                            <Virtualia:VIcone ID="Cmd17" runat="server" IconeURL="~/Images/General/VirImag_OrangeGris.jpg"
                                SiVisible="False" EtiText="" IconeTooltip=""
                                NavigateURL="" ParametreURL="ID" ParametreFRM="" VIndex="17" />
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell Width="310px" Height="150px">
                            <Virtualia:VIcone ID="Cmd18" runat="server" IconeURL="~/Images/General/VirImag_BronzeGris.jpg"
                                SiVisible="False" EtiText="" IconeTooltip=""
                                NavigateURL="" ParametreURL="ID" ParametreFRM="" VIndex="18" />
                        </asp:TableCell>
                    </asp:TableRow>
                </asp:Table>
            </asp:TableCell>
            <asp:TableCell>
                <asp:Table ID="CadreTuile07" runat="server" HorizontalAlign="Left">
                    <asp:TableRow>
                        <asp:TableCell Width="310px" Height="150px">
                            <Virtualia:VIcone ID="Cmd19" runat="server" IconeURL="~/Images/General/VirImag_Mauve.jpg"
                                SiVisible="False" EtiText="" IconeTooltip=""
                                NavigateURL="" ParametreURL="ID" ParametreFRM="" VIndex="19" />
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell Width="310px" Height="150px">
                            <Virtualia:VIcone ID="Cmd20" runat="server" IconeURL="~/Images/General/VirImag_Marron.jpg"
                                SiVisible="False" EtiText="" IconeTooltip=""
                                NavigateURL="" ParametreURL="ID" ParametreFRM="" VIndex="20" />
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell Width="310px" Height="150px">
                            <Virtualia:VIcone ID="Cmd21" runat="server" IconeURL="~/Images/General/VirImag_Vert.jpg"
                                SiVisible="False" EtiText="" IconeTooltip=""
                                NavigateURL="" ParametreURL="ID" ParametreFRM="" VIndex="21" />
                        </asp:TableCell>
                    </asp:TableRow>
                </asp:Table>
            </asp:TableCell>
            <asp:TableCell>
                <asp:Table ID="CadreTuile08" runat="server" HorizontalAlign="Left">
                    <asp:TableRow>
                        <asp:TableCell Width="310px" Height="150px">
                            <Virtualia:VIcone ID="Cmd22" runat="server" IconeURL="~/Images/General/VirImag_MauveGris.jpg"
                                SiVisible="False" EtiText="" IconeTooltip=""
                                NavigateURL="" ParametreURL="ID" ParametreFRM="" VIndex="22" />
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell Width="310px" Height="150px">
                            <Virtualia:VIcone ID="Cmd23" runat="server" IconeURL="~/Images/General/VirImag_MarronGris.jpg"
                                SiVisible="False" EtiText="" IconeTooltip=""
                                NavigateURL="" ParametreURL="ID" ParametreFRM="" VIndex="23" />
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell Width="310px" Height="150px">
                            <Virtualia:VIcone ID="Cmd24" runat="server" IconeURL="~/Images/General/VirImag_VertGris.jpg"
                                SiVisible="False" EtiText="" IconeTooltip=""
                                NavigateURL="" ParametreURL="ID" ParametreFRM="" VIndex="24" />
                        </asp:TableCell>
                    </asp:TableRow>
                </asp:Table>
            </asp:TableCell>
        </asp:TableRow>
    </asp:Table>
</asp:Panel>
