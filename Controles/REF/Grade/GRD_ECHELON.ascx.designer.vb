﻿'------------------------------------------------------------------------------
' <généré automatiquement>
'     Ce code a été généré par un outil.
'
'     Les modifications apportées à ce fichier peuvent provoquer un comportement incorrect et seront perdues si
'     le code est régénéré.
' </généré automatiquement>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On


Partial Public Class GRD_ECHELON
    
    '''<summary>
    '''Contrôle LigneEchelon.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LigneEchelon As Global.System.Web.UI.WebControls.Table
    
    '''<summary>
    '''Contrôle CommandeSelect.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CommandeSelect As Global.System.Web.UI.WebControls.ImageButton
    
    '''<summary>
    '''Contrôle DonEchelon.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents DonEchelon As Global.System.Web.UI.WebControls.TextBox
    
    '''<summary>
    '''Contrôle DonChevron.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents DonChevron As Global.System.Web.UI.WebControls.TextBox
    
    '''<summary>
    '''Contrôle DonIndiceBrut.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents DonIndiceBrut As Global.System.Web.UI.WebControls.TextBox
    
    '''<summary>
    '''Contrôle DonIndiceMajore.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents DonIndiceMajore As Global.System.Web.UI.WebControls.TextBox
    
    '''<summary>
    '''Contrôle DonDureeMini.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents DonDureeMini As Global.System.Web.UI.WebControls.TextBox
    
    '''<summary>
    '''Contrôle DonDureeMaxi.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents DonDureeMaxi As Global.System.Web.UI.WebControls.TextBox
    
    '''<summary>
    '''Contrôle DonDureeMoyenne.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents DonDureeMoyenne As Global.System.Web.UI.WebControls.TextBox
    
    '''<summary>
    '''Contrôle DonGroupe.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents DonGroupe As Global.System.Web.UI.WebControls.TextBox
End Class
