﻿Option Strict On
Option Explicit On
Option Compare Text
Imports Virtualia.Systeme.Evenements
Imports VI = Virtualia.Systeme.Constantes
Public Class PerAdministratifPublic
    Inherits System.Web.UI.UserControl
    Private WebFct As Virtualia.Net.Controles.WebFonctions
    Private WsDossierPer As Virtualia.Net.Individuel.DossierIndividu

    Private ReadOnly Property V_WebFonction() As Virtualia.Net.Controles.WebFonctions
        Get
            If WebFct Is Nothing Then
                WebFct = New Virtualia.Net.Controles.WebFonctions(Me, 0)
            End If
            Return WebFct
        End Get
    End Property

    Private ReadOnly Property V_Contexte As Virtualia.Net.Individuel.LocalNavigation
        Get
            Return CType(V_WebFonction.PointeurGlobal, Virtualia.Net.Session.ObjetGlobal).ContexteSession(Session.SessionID)
        End Get
    End Property

    Private Sub PerAdministratifPublic_PreRender(sender As Object, e As EventArgs) Handles Me.PreRender
        If (Not Page.ClientScript.IsStartupScriptRegistered("AjaxFileUpload_change_text")) Then
            Page.ClientScript.RegisterStartupScript(Me.GetType(), "AjaxFileTraduction", "AjaxFileUpload_change_text();", True)
        End If
        If HPopupRef.Value = "1" Then
            CellReference.Visible = True
            PopupReferentiel.Show()
        ElseIf HPopupRefGrade.Value = "1" Then
            CellRefGrade.Visible = True
            PopupRefGrade.Show()
        Else
            CellReference.Visible = False
            Call LireLaFiche()
        End If
    End Sub

    Private Sub LireLaFiche()
        Dim NumObjet As Integer
        Dim NumInfo As Integer
        Dim Ctl As Control
        Dim IndiceI As Integer = 0
        Dim SiLectureOnly As Boolean = False

        Dim VirControle As Controles_VCoupleEtiDonnee
        Do
            Ctl = V_WebFonction.VirWebControle(Me.CadreInfosAdm, "InfoH", IndiceI)
            If Ctl Is Nothing Then
                Exit Do
            End If
            NumInfo = CInt(Strings.Right(Ctl.ID, 2))
            VirControle = CType(Ctl, Controles_VCoupleEtiDonnee)
            NumObjet = VirControle.V_Objet
            If VirControle.V_SiDonneeDico = True Then
                VirControle.DonText = ValeurLue(NumObjet, NumInfo)
            Else
                VirControle.DonText = ""
            End If
            VirControle.V_SiEnLectureSeule = SiLectureOnly
            VirControle.V_SiAutoPostBack = False
            IndiceI += 1
        Loop

        Dim VirDonneeTable As Controles_VDuoEtiquetteCommande
        IndiceI = 0
        Do
            Ctl = V_WebFonction.VirWebControle(Me.CadreInfosAdm, "Dontab", IndiceI)
            If Ctl Is Nothing Then
                Exit Do
            End If
            NumInfo = CInt(Strings.Right(Ctl.ID, 2))
            VirDonneeTable = CType(Ctl, Controles_VDuoEtiquetteCommande)
            NumObjet = VirDonneeTable.V_Objet
            VirDonneeTable.DonText = ValeurLue(NumObjet, NumInfo)
            VirDonneeTable.V_SiEnLectureSeule = SiLectureOnly
            IndiceI += 1
        Loop

        Dim VirDonneeDate As VCoupleEtiDate
        IndiceI = 0
        Do
            Ctl = V_WebFonction.VirWebControle(Me.CadreInfosAdm, "InfoD", IndiceI)
            If Ctl Is Nothing Then
                Exit Do
            End If
            NumInfo = CInt(Strings.Right(Ctl.ID, 2))
            VirDonneeDate = CType(Ctl, VCoupleEtiDate)
            NumObjet = VirDonneeDate.V_Objet
            VirDonneeDate.DonText = ValeurLue(NumObjet, NumInfo)
            VirDonneeDate.V_SiEnLectureSeule = SiLectureOnly
            VirDonneeDate.V_SiAutoPostBack = False
            IndiceI += 1
        Loop

    End Sub

    Private Sub DonneeDate_ValeurChange(sender As Object, e As DonneeChangeEventArgs) Handles InfoDS00.ValeurChange, InfoDS03.ValeurChange,
            InfoDP00.ValeurChange, InfoDP03.ValeurChange, InfoDP07.ValeurChange, InfoDP09.ValeurChange, InfoDP10.ValeurChange,
            InfoDG00.ValeurChange, InfoDG09.ValeurChange, InfoDG21.ValeurChange

        WsDossierPer = V_Contexte.DossierPER
        If WsDossierPer Is Nothing Then
            Exit Sub
        End If
        Dim NumInfo As Integer = CType(sender, VCoupleEtiDate).V_Information
        Dim NumObjet As Integer = CType(sender, VCoupleEtiDate).V_Objet
        If WsDossierPer.DonneeLue(NumObjet, NumInfo) <> e.Valeur Then
            CType(sender, VCoupleEtiDate).DonBackColor = V_WebFonction.CouleurMaj
            WsDossierPer.TableauMaj(NumObjet, NumInfo) = e.Valeur
        End If
    End Sub

    Private Sub InfoH_ValeurChange(sender As Object, e As DonneeChangeEventArgs) Handles InfoHS04.ValeurChange, InfoHP02.ValeurChange,
            InfoHP04.ValeurChange, InfoHP12.ValeurChange, InfoHG03.ValeurChange, InfoHG04.ValeurChange, InfoHG10.ValeurChange,
            InfoHG22.ValeurChange

        WsDossierPer = V_Contexte.DossierPER
        If WsDossierPer Is Nothing Then
            Exit Sub
        End If
        Dim NumInfo As Integer = CInt(Strings.Right(CType(sender, Controles_VCoupleEtiDonnee).ID, 2))
        Dim NumObjet As Integer = CType(sender, Controles_VCoupleEtiDonnee).V_Objet
        If WsDossierPer.DonneeLue(NumObjet, NumInfo) <> e.Valeur Then
            CType(sender, Controles_VCoupleEtiDonnee).DonBackColor = V_WebFonction.CouleurMaj
            WsDossierPer.TableauMaj(NumObjet, NumInfo) = e.Valeur
        End If
    End Sub

    Private Sub Dontab_AppelTable(sender As Object, e As AppelTableEventArgs) Handles DontabS01.AppelTable, DontabS02.AppelTable,
         DontabP01.AppelTable, DontabP08.AppelTable, DontabP11.AppelTable, DontabG01.AppelTable, DontabG02.AppelTable, DontabG05.AppelTable,
         DontabG06.AppelTable, DontabG11.AppelTable, DontabG12.AppelTable, DontabG13.AppelTable

        RefVirtualia.V_PointdeVue = e.PointdeVueInverse
        RefVirtualia.V_NomTable = e.NomdelaTable
        Select Case e.ObjetAppelant
            Case VI.ObjetPer.ObaGrade
                V_WebFonction.PointeurContexte.TsTampon_StringList = Nothing
                RefVirtualia.V_Appelant(e.ObjetAppelant, e.DatedeValeur) = e.ControleAppelant
                If e.ControleAppelant = "DontabG01" Then
                    RefGradeGrilles.V_PointdeVue = e.PointdeVueInverse
                    RefGradeGrilles.V_NomTable = ""
                    RefGradeGrilles.V_Valeur_Selectionnee = e.NomdelaTable
                    RefGradeGrilles.V_Appelant(2, e.DatedeValeur) = e.ControleAppelant
                    HPopupRefGrade.Value = "1"
                    CellRefGrade.Visible = True
                    PopupRefGrade.Show()
                    Exit Sub
                End If
        End Select
        RefVirtualia.V_Appelant(e.ObjetAppelant) = e.ControleAppelant
        HPopupRef.Value = "1"
        CellReference.Visible = True
        PopupReferentiel.Show()
    End Sub

    Public WriteOnly Property Dontab_RetourAppelTable(ByVal IDAppelant As String) As String
        Set(ByVal value As String)
            WsDossierPer = V_Contexte.DossierPER
            If WsDossierPer Is Nothing Then
                Exit Property
            End If
            Dim Ctl As Control
            Dim NumInfo As Integer
            Dim NumObjet As Integer
            Dim VirDonneeTable As Controles_VDuoEtiquetteCommande
            Ctl = V_WebFonction.VirWebControle(Me.CadreInfosAdm, "Dontab" & Strings.Right(IDAppelant, 3), 0)
            If Ctl Is Nothing Then
                Exit Property
            End If
            VirDonneeTable = CType(Ctl, Controles_VDuoEtiquetteCommande)
            NumInfo = CInt(Strings.Right(VirDonneeTable.ID, 2))
            NumObjet = VirDonneeTable.V_Objet
            If VirDonneeTable.ID <> "DontabG01" Then
                VirDonneeTable.DonText = value
                If WsDossierPer.DonneeLue(NumObjet, NumInfo) <> value Then
                    VirDonneeTable.DonBackColor = V_WebFonction.CouleurMaj
                    WsDossierPer.TableauMaj(NumObjet, NumInfo) = value
                End If
                Exit Property
            End If
            '*** Grade ***
            Dim TableauData(0) As String
            Dim FicheREF As Virtualia.TablesObjet.ShemaREF.GRD_GRADE
            Dim Collgrades As Virtualia.Ressources.Datas.ObjetGradeGrille
            TableauData = Strings.Split(value, VI.PointVirgule, -1)
            If IsNumeric(TableauData(0)) = False Then
                Exit Property
            End If
            Collgrades = V_WebFonction.PointeurReferentiel.PointeurGradeGrilles
            If Collgrades Is Nothing Then
                Exit Property
            End If
            FicheREF = Collgrades.Fiche_Grade(CInt(TableauData(0)))
            If FicheREF Is Nothing Then
                Exit Property
            End If
            If WsDossierPer.DonneeLue(NumObjet, 1) <> FicheREF.Intitule Then
                VirDonneeTable.DonBackColor = V_WebFonction.CouleurMaj
                WsDossierPer.TableauMaj(NumObjet, 1) = FicheREF.Intitule
            End If
            WsDossierPer.TableauMaj(NumObjet, 5) = FicheREF.Categorie
            WsDossierPer.TableauMaj(NumObjet, 12) = FicheREF.Filiere
            WsDossierPer.TableauMaj(NumObjet, 6) = FicheREF.Corps
            WsDossierPer.TableauMaj(NumObjet, 7) = FicheREF.Echelle_de_remuneration
            If TableauData.Count >= 5 Then
                WsDossierPer.TableauMaj(NumObjet, 2) = TableauData(1) 'Echelon
                If TableauData(2) <> "" Then
                    WsDossierPer.TableauMaj(NumObjet, 7) = TableauData(2) 'Chevron
                End If
                WsDossierPer.TableauMaj(NumObjet, 3) = TableauData(3) 'Indice Brut
                WsDossierPer.TableauMaj(NumObjet, 4) = TableauData(4) 'Indice Majoré
            End If
        End Set
    End Property

    Protected Sub Referentiel_RetourEventHandler(ByVal sender As Object, ByVal e As System.EventArgs) Handles RefVirtualia.RetourEventHandler, RefGradeGrilles.RetourEventHandler
        HPopupRef.Value = "0"
        CellReference.Visible = False
        HPopupRefGrade.Value = "0"
        CellRefGrade.Visible = False
    End Sub

    Protected Sub Referentiel_ValeurSelectionnee(ByVal sender As Object, ByVal e As Virtualia.Systeme.Evenements.ValeurSelectionneeEventArgs) Handles RefVirtualia.ValeurSelectionnee, RefGradeGrilles.ValeurSelectionnee
        HPopupRef.Value = "0"
        CellReference.Visible = False
        HPopupRefGrade.Value = "0"
        CellRefGrade.Visible = False
        If e.ControleAppelant = "DontabG01" Then
            Dontab_RetourAppelTable(e.ControleAppelant) = e.Identifiant & VI.PointVirgule & e.Valeur
        Else
            Dontab_RetourAppelTable(e.ControleAppelant) = e.Valeur
        End If
    End Sub

    Private ReadOnly Property ValeurLue(ByVal NoObjet As Integer, ByVal NoInfo As Integer) As String
        Get
            If WsDossierPer Is Nothing Then
                Try
                    WsDossierPer = V_Contexte.DossierPER
                Catch ex As Exception
                    Return ""
                End Try
                If WsDossierPer Is Nothing Then
                    Return ""
                End If
            End If
            Return WsDossierPer.DonneeLue(NoObjet, NoInfo)
        End Get
    End Property
End Class