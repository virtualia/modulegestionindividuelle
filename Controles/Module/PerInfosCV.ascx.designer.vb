﻿'------------------------------------------------------------------------------
' <généré automatiquement>
'     Ce code a été généré par un outil.
'
'     Les modifications apportées à ce fichier peuvent provoquer un comportement incorrect et seront perdues si
'     le code est régénéré.
' </généré automatiquement>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On


Partial Public Class PerInfosCV
    
    '''<summary>
    '''Contrôle CadreGeneralCV.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CadreGeneralCV As Global.System.Web.UI.WebControls.Table
    
    '''<summary>
    '''Contrôle CadreTitreGeneral.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CadreTitreGeneral As Global.System.Web.UI.WebControls.Table
    
    '''<summary>
    '''Contrôle EtiTitre.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents EtiTitre As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle CadreInfosCV.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CadreInfosCV As Global.System.Web.UI.WebControls.Table
    
    '''<summary>
    '''Contrôle EtiPerDiplome.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents EtiPerDiplome As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle CadreDiplome.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CadreDiplome As Global.Virtualia.Net.PerDiplomes
    
    '''<summary>
    '''Contrôle EtiUploadDiplome.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents EtiUploadDiplome As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle AjaxUploadDiplome.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents AjaxUploadDiplome As Global.AjaxControlToolkit.AjaxFileUpload
    
    '''<summary>
    '''Contrôle EtiPerLangue.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents EtiPerLangue As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle CadreLangue.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CadreLangue As Global.Virtualia.Net.PerLangues
    
    '''<summary>
    '''Contrôle EtiUploadLangue.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents EtiUploadLangue As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle AjaxFileUploadLangue.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents AjaxFileUploadLangue As Global.AjaxControlToolkit.AjaxFileUpload
    
    '''<summary>
    '''Contrôle EtiPerQualification.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents EtiPerQualification As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle CadreBrevetPro.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CadreBrevetPro As Global.Virtualia.Net.PerBrevetsPro
    
    '''<summary>
    '''Contrôle EtiUploadBrevet.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents EtiUploadBrevet As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle AjaxFileUploadBrevet.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents AjaxFileUploadBrevet As Global.AjaxControlToolkit.AjaxFileUpload
End Class
