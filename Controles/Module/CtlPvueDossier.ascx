﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="CtlPvueDossier.ascx.vb" Inherits="Virtualia.Net.CtlPvueDossier" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register Src="~/Controles/Standards/VReferentiel.ascx" TagName="VReference" TagPrefix="Virtualia" %>
<%@ Register Src="~/Controles/Standards/VMessage.ascx" TagName="VMessage" TagPrefix="Virtualia" %>
<%@ Register Src="~/Controles/Module/PerInfosGenerales.ascx" TagName="VInfoGen" TagPrefix="Virtualia" %>
<%@ Register Src="~/Controles/Module/PerInfosCV.ascx" TagName="VInfoCV" TagPrefix="Virtualia" %>
<%@ Register Src="~/Controles/Module/PerAdministratifPublic.ascx" TagName="VAdministratif" TagPrefix="Virtualia" %>
<%@ Register Src="~/Controles/Module/PerAffectationPublic.ascx" TagName="VAffectation" TagPrefix="Virtualia" %>
<%@ Register Src="~/Controles/Module/PerFormation.ascx" TagName="VFormation" TagPrefix="Virtualia" %>
<%@ Register Src="~/Controles/Standards/VArmoirePER.ascx" TagName="VArmoire" TagPrefix="Virtualia" %>
<%@ Register src="~/Controles/PER/CtlPvuePER.ascx" tagname="VPER" tagprefix="Virtualia" %>


<asp:Table ID="CadreGlobal" runat="server" HorizontalAlign="Center" Width="1140px">
        <asp:TableRow>
            <asp:TableCell HorizontalAlign="Center" VerticalAlign="Top">
                        <asp:Table ID="TableOnglets" runat="server" Height="35px" Width="950px" HorizontalAlign="Center" Style="margin-top: 0px">
                            <asp:TableRow>
                                <asp:TableCell>
                                    <asp:Table ID="CadreOnglets" runat="server" Height="35px" Width="1100px" HorizontalAlign="Left" CellPadding="0" CellSpacing="0" BackColor="Transparent" Style="margin-top: 2px">
                                        <asp:TableRow>
                                            <asp:TableCell HorizontalAlign="Left">
                                                <asp:Menu ID="MenuChoix" runat="server" Width="1000px" DynamicHorizontalOffset="10" Font-Names="Trebuchet MS" Font-Size="Small" BackColor="Transparent" Font-Italic="false" ForeColor="#124545" Height="32px" Orientation="Horizontal" StaticSubMenuIndent="20px" Font-Bold="True" BorderWidth="2px" Style="margin-left: 1px;" StaticEnableDefaultPopOutImage="False" BorderColor="#124545" BorderStyle="None">
                                                    <StaticSelectedStyle BackColor="#124545" BorderStyle="Solid" BorderColor="#B0E0D7" Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="Small" ForeColor="#B0E0D7" />
                                                    <StaticMenuItemStyle VerticalPadding="2px" HorizontalPadding="10px" Height="24px" BackColor="#B0E0D7" BorderColor="#124545" BorderStyle="Solid" Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small" ForeColor="#124545" />
                                                    <StaticHoverStyle BackColor="#124545" ForeColor="White" BorderColor="#124545" BorderStyle="Solid" />
                                                    <Items>
                                                        <asp:MenuItem Text="DONNEES PERSONNELLES" Value="DONPER" ToolTip="" />
                                                        <asp:MenuItem Text="DIPLOMES ET QUALIFICATIONS" Value="DIPLOME" ToolTip="" />
                                                        <asp:MenuItem Text="SITUATION ADMINISTRATIVE" Value="SITADM" ToolTip="" />
                                                        <asp:MenuItem Text="AFFECTATIONS FONCTIONNELLES" Value="AFFECTATION" ToolTip="" />
                                                        <asp:MenuItem Text="PARCOURS DE FORMATION" Value="FORMATION" ToolTip="" />
                                                    </Items>
                                                </asp:Menu>
                                            </asp:TableCell>
                                        </asp:TableRow>
                                    </asp:Table>
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>

                        <asp:Table ID="CadreVues" runat="server" Height="800px" Width="1150px" HorizontalAlign="Center" BackImageUrl="~/Images/Fonds/Fond_Saisie.jpg" CellSpacing="2" Style="margin-top: 1px">
                            <asp:TableRow>
                                <asp:TableCell Height="30px">
                                    <asp:Label ID="EtiIdentite" runat="server" Text="" Height="25px" Width="400px"
                                        BackColor="#6C9690" BorderColor="#B0E0D7" BorderStyle="Outset" BorderWidth="2px" ForeColor="White"
                                        Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                        Style="margin-top: 2px; margin-left: 0px; margin-bottom: 0px; font-style: oblique; text-indent: 5px; text-align: center;">
                                    </asp:Label>
                                </asp:TableCell>

                                <asp:TableCell HorizontalAlign="Right">
                                    <asp:Table ID="CadreCmdOK" runat="server" Height="22px" CellPadding="0"
                                        CellSpacing="0" BackImageUrl="~/Images/Boutons/OK_Std.bmp" Visible="true"
                                        BorderWidth="2px" BorderStyle="Outset" BorderColor="#FFEBC8" ForeColor="#FFF2DB"
                                        Width="80px" HorizontalAlign="Center" Style="margin-top: 3px; margin-right: 3px">
                                        <asp:TableRow>
                                            <asp:TableCell VerticalAlign="Bottom">
                                                <asp:Button ID="CommandeOK" runat="server" Text="Enregistrer" Width="75px" Height="20px"
                                                    BackColor="Transparent" BorderColor="#B0E0D7" ForeColor="#D7FAF3"
                                                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small" Font-Italic="true"
                                                    BorderStyle="None" Style="margin-left: 6px; text-align: right;"></asp:Button>
                                            </asp:TableCell>
                                        </asp:TableRow>
                                    </asp:Table>
                                </asp:TableCell>

                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell ID="ConteneurVues" Width="1150px" VerticalAlign="Top" HorizontalAlign="Center">
                                    <asp:MultiView ID="MultiOnglets" runat="server" ActiveViewIndex="0">
                                        <asp:View ID="VueGenerale" runat="server">
                                            <Virtualia:VInfoGen ID="Per_InfoGen" runat="server" CadreStyle="margin-bottom: 20px" />
                                        </asp:View>
                                        <asp:View ID="VueDiplome" runat="server">
                                            <Virtualia:VInfoCV ID="Per_InfoCV" runat="server" CadreStyle="margin-bottom: 20px" />
                                        </asp:View>
                                        <asp:View ID="VueSitAdm" runat="server">
                                            <Virtualia:VAdministratif ID="Per_InfoAdm" runat="server" CadreStyle="margin-bottom: 20px" />
                                        </asp:View>
                                        <asp:View ID="VueAffectation" runat="server">
                                            <Virtualia:VAffectation ID="Per_Affectation" runat="server" CadreStyle="margin-bottom: 20px" />
                                        </asp:View>
                                        <asp:View ID="VueFormation" runat="server">
                                            <Virtualia:VFormation ID="Per_Formation" runat="server" V_SiEnLectureSeule="true" CadreStyle="margin-bottom: 20px" />
                                        </asp:View>
                                    </asp:MultiView>
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell ID="CellMessage" Visible="false">
                                    <ajaxToolkit:ModalPopupExtender ID="PopupMsg" runat="server" TargetControlID="HPopupMsg" PopupControlID="PanelMsgPopup" BackgroundCssClass="modalBackground" />
                                    <asp:Panel ID="PanelMsgPopup" runat="server">
                                        <Virtualia:VMessage ID="MsgVirtualia" runat="server" />
                                    </asp:Panel>
                                    <asp:HiddenField ID="HPopupMsg" runat="server" Value="0" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell ID="CellReference" Visible="false">
                                    <ajaxToolkit:ModalPopupExtender ID="PopupReferentiel" runat="server" TargetControlID="HPopupRef" PopupControlID="PanelRefPopup" BackgroundCssClass="modalBackground" />
                                    <asp:Panel ID="PanelRefPopup" runat="server">
                                        <Virtualia:VReference ID="RefVirtualia" runat="server" />
                                    </asp:Panel>
                                    <asp:HiddenField ID="HPopupRef" runat="server" Value="0" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell>
                                    <asp:HiddenField ID="HSelIde" runat="server" Value="0" />
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>
            </asp:TableCell>
        </asp:TableRow>
</asp:Table>
   
