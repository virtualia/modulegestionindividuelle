﻿'------------------------------------------------------------------------------
' <généré automatiquement>
'     Ce code a été généré par un outil.
'
'     Les modifications apportées à ce fichier peuvent provoquer un comportement incorrect et seront perdues si
'     le code est régénéré.
' </généré automatiquement>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On


Partial Public Class PerEnfants
    
    '''<summary>
    '''Contrôle CadrePerEnfant.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CadrePerEnfant As Global.System.Web.UI.WebControls.Table
    
    '''<summary>
    '''Contrôle CadreTitre.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CadreTitre As Global.System.Web.UI.WebControls.Table
    
    '''<summary>
    '''Contrôle EtiDate.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents EtiDate As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle EtiPrenom.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents EtiPrenom As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle EtiNom.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents EtiNom As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle EtiSexe.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents EtiSexe As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle Ligne_01.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Ligne_01 As Global.System.Web.UI.WebControls.Table
    
    '''<summary>
    '''Contrôle Date_L1.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Date_L1 As Global.Virtualia.Net.VCoupleEtiDate
    
    '''<summary>
    '''Contrôle Prenom_L1.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Prenom_L1 As Global.System.Web.UI.WebControls.TextBox
    
    '''<summary>
    '''Contrôle Nom_L1.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Nom_L1 As Global.System.Web.UI.WebControls.TextBox
    
    '''<summary>
    '''Contrôle Radio_L1.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Radio_L1 As Global.Virtualia.Net.Controles_VTrioHorizontalRadio
    
    '''<summary>
    '''Contrôle Ligne_02.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Ligne_02 As Global.System.Web.UI.WebControls.Table
    
    '''<summary>
    '''Contrôle Date_L2.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Date_L2 As Global.Virtualia.Net.VCoupleEtiDate
    
    '''<summary>
    '''Contrôle Prenom_L2.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Prenom_L2 As Global.System.Web.UI.WebControls.TextBox
    
    '''<summary>
    '''Contrôle Nom_L2.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Nom_L2 As Global.System.Web.UI.WebControls.TextBox
    
    '''<summary>
    '''Contrôle Radio_L2.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Radio_L2 As Global.Virtualia.Net.Controles_VTrioHorizontalRadio
    
    '''<summary>
    '''Contrôle Ligne_03.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Ligne_03 As Global.System.Web.UI.WebControls.Table
    
    '''<summary>
    '''Contrôle Date_L3.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Date_L3 As Global.Virtualia.Net.VCoupleEtiDate
    
    '''<summary>
    '''Contrôle Prenom_L3.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Prenom_L3 As Global.System.Web.UI.WebControls.TextBox
    
    '''<summary>
    '''Contrôle Nom_L3.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Nom_L3 As Global.System.Web.UI.WebControls.TextBox
    
    '''<summary>
    '''Contrôle Radio_L3.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Radio_L3 As Global.Virtualia.Net.Controles_VTrioHorizontalRadio
    
    '''<summary>
    '''Contrôle Ligne_04.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Ligne_04 As Global.System.Web.UI.WebControls.Table
    
    '''<summary>
    '''Contrôle Date_L4.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Date_L4 As Global.Virtualia.Net.VCoupleEtiDate
    
    '''<summary>
    '''Contrôle Prenom_L4.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Prenom_L4 As Global.System.Web.UI.WebControls.TextBox
    
    '''<summary>
    '''Contrôle Nom_L4.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Nom_L4 As Global.System.Web.UI.WebControls.TextBox
    
    '''<summary>
    '''Contrôle Radio_L4.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Radio_L4 As Global.Virtualia.Net.Controles_VTrioHorizontalRadio
    
    '''<summary>
    '''Contrôle Ligne_05.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Ligne_05 As Global.System.Web.UI.WebControls.Table
    
    '''<summary>
    '''Contrôle Date_L5.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Date_L5 As Global.Virtualia.Net.VCoupleEtiDate
    
    '''<summary>
    '''Contrôle Prenom_L5.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Prenom_L5 As Global.System.Web.UI.WebControls.TextBox
    
    '''<summary>
    '''Contrôle Nom_L5.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Nom_L5 As Global.System.Web.UI.WebControls.TextBox
    
    '''<summary>
    '''Contrôle Radio_L5.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Radio_L5 As Global.Virtualia.Net.Controles_VTrioHorizontalRadio
    
    '''<summary>
    '''Contrôle Ligne_06.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Ligne_06 As Global.System.Web.UI.WebControls.Table
    
    '''<summary>
    '''Contrôle Date_L6.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Date_L6 As Global.Virtualia.Net.VCoupleEtiDate
    
    '''<summary>
    '''Contrôle Prenom_L6.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Prenom_L6 As Global.System.Web.UI.WebControls.TextBox
    
    '''<summary>
    '''Contrôle Nom_L6.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Nom_L6 As Global.System.Web.UI.WebControls.TextBox
    
    '''<summary>
    '''Contrôle Radio_L6.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Radio_L6 As Global.Virtualia.Net.Controles_VTrioHorizontalRadio
    
    '''<summary>
    '''Contrôle Ligne_07.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Ligne_07 As Global.System.Web.UI.WebControls.Table
    
    '''<summary>
    '''Contrôle Date_L7.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Date_L7 As Global.Virtualia.Net.VCoupleEtiDate
    
    '''<summary>
    '''Contrôle Prenom_L7.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Prenom_L7 As Global.System.Web.UI.WebControls.TextBox
    
    '''<summary>
    '''Contrôle Nom_L7.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Nom_L7 As Global.System.Web.UI.WebControls.TextBox
    
    '''<summary>
    '''Contrôle Radio_L7.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Radio_L7 As Global.Virtualia.Net.Controles_VTrioHorizontalRadio
End Class
