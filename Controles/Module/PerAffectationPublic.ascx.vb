﻿Option Strict On
Option Explicit On
Option Compare Text
Imports Virtualia.Systeme.Evenements
Imports VI = Virtualia.Systeme.Constantes
Public Class PerAffectationPublic
    Inherits System.Web.UI.UserControl
    Private WebFct As Virtualia.Net.Controles.WebFonctions
    Private WsDossierPer As Virtualia.Net.Individuel.DossierIndividu

    Private ReadOnly Property V_WebFonction() As Virtualia.Net.Controles.WebFonctions
        Get
            If WebFct Is Nothing Then
                WebFct = New Virtualia.Net.Controles.WebFonctions(Me, 0)
            End If
            Return WebFct
        End Get
    End Property

    Private ReadOnly Property V_Contexte As Virtualia.Net.Individuel.LocalNavigation
        Get
            Return CType(V_WebFonction.PointeurGlobal, Virtualia.Net.Session.ObjetGlobal).ContexteSession(Session.SessionID)
        End Get
    End Property

    Private Sub PerAffectationPublic_PreRender(sender As Object, e As EventArgs) Handles Me.PreRender
        If HPopupRef.Value = "1" Then
            CellReference.Visible = True
            PopupReferentiel.Show()
        Else
            CellReference.Visible = False
            Call LireLaFiche()
        End If
    End Sub

    Private Sub LireLaFiche()
        Dim NumObjet As Integer
        Dim NumInfo As Integer
        Dim Ctl As Control
        Dim IndiceI As Integer = 0
        Dim SiLectureOnly As Boolean = False

        Dim VirControle As Controles_VCoupleEtiDonnee
        Do
            Ctl = V_WebFonction.VirWebControle(Me.CadreInfosAff, "InfoH", IndiceI)
            If Ctl Is Nothing Then
                Exit Do
            End If
            NumInfo = CInt(Strings.Right(Ctl.ID, 2))
            VirControle = CType(Ctl, Controles_VCoupleEtiDonnee)
            NumObjet = VirControle.V_Objet
            If VirControle.V_SiDonneeDico = True Then
                VirControle.DonText = ValeurLue(NumObjet, NumInfo)
            Else
                VirControle.DonText = ""
            End If
            VirControle.V_SiEnLectureSeule = SiLectureOnly
            VirControle.V_SiAutoPostBack = False
            IndiceI += 1
        Loop

        Dim VirDonneeTable As Controles_VDuoEtiquetteCommande
        IndiceI = 0
        Do
            Ctl = V_WebFonction.VirWebControle(Me.CadreInfosAff, "Dontab", IndiceI)
            If Ctl Is Nothing Then
                Exit Do
            End If
            NumInfo = CInt(Strings.Right(Ctl.ID, 2))
            VirDonneeTable = CType(Ctl, Controles_VDuoEtiquetteCommande)
            NumObjet = VirDonneeTable.V_Objet
            VirDonneeTable.DonText = ValeurLue(NumObjet, NumInfo)
            VirDonneeTable.V_SiEnLectureSeule = SiLectureOnly
            IndiceI += 1
        Loop

        Dim VirDonneeDate As VCoupleEtiDate
        IndiceI = 0
        Do
            Ctl = V_WebFonction.VirWebControle(Me.CadreInfosAff, "InfoD", IndiceI)
            If Ctl Is Nothing Then
                Exit Do
            End If
            NumInfo = CInt(Strings.Right(Ctl.ID, 2))
            VirDonneeDate = CType(Ctl, VCoupleEtiDate)
            NumObjet = VirDonneeDate.V_Objet
            VirDonneeDate.DonText = ValeurLue(NumObjet, NumInfo)
            VirDonneeDate.V_SiEnLectureSeule = SiLectureOnly
            VirDonneeDate.V_SiAutoPostBack = False
            IndiceI += 1
        Loop

        Dim VirVertical As Controles_VCoupleVerticalEtiDonnee
        IndiceI = 0
        Do
            Ctl = V_WebFonction.VirWebControle(Me.CadreInfosAff, "InfoV", IndiceI)
            If Ctl Is Nothing Then
                Exit Do
            End If
            NumInfo = CInt(Strings.Right(Ctl.ID, 2))
            VirVertical = CType(Ctl, Controles_VCoupleVerticalEtiDonnee)
            NumObjet = VirVertical.V_Objet
            VirVertical.DonText = ValeurLue(NumObjet, NumInfo)
            VirVertical.V_SiEnLectureSeule = SiLectureOnly
            VirVertical.V_SiAutoPostBack = False
            IndiceI += 1
        Loop

    End Sub

    Private Sub DonneeDate_ValeurChange(sender As Object, e As DonneeChangeEventArgs) Handles InfoDF00.ValeurChange, InfoDF07.ValeurChange,
            InfoDT00.ValeurChange, InfoDT12.ValeurChange

        WsDossierPer = V_Contexte.DossierPER
        If WsDossierPer Is Nothing Then
            Exit Sub
        End If
        Dim NumInfo As Integer = CType(sender, VCoupleEtiDate).V_Information
        Dim NumObjet As Integer = CType(sender, VCoupleEtiDate).V_Objet
        If WsDossierPer.DonneeLue(NumObjet, NumInfo) <> e.Valeur Then
            CType(sender, VCoupleEtiDate).DonBackColor = V_WebFonction.CouleurMaj
            WsDossierPer.TableauMaj(NumObjet, NumInfo) = e.Valeur
        End If
    End Sub

    Private Sub InfoH_ValeurChange(sender As Object, e As DonneeChangeEventArgs) Handles InfoHP01.ValeurChange, InfoHP02.ValeurChange,
            InfoHP04.ValeurChange, InfoHP07.ValeurChange, InfoHP08.ValeurChange, InfoHP10.ValeurChange, InfoHP11.ValeurChange,
            InfoHP12.ValeurChange, InfoHP13.ValeurChange

        WsDossierPer = V_Contexte.DossierPER
        If WsDossierPer Is Nothing Then
            Exit Sub
        End If
        Dim NumInfo As Integer = CInt(Strings.Right(CType(sender, Controles_VCoupleEtiDonnee).ID, 2))
        Dim NumObjet As Integer = CType(sender, Controles_VCoupleEtiDonnee).V_Objet
        If WsDossierPer.DonneeLue(NumObjet, NumInfo) <> e.Valeur Then
            CType(sender, Controles_VCoupleEtiDonnee).DonBackColor = V_WebFonction.CouleurMaj
            WsDossierPer.TableauMaj(NumObjet, NumInfo) = e.Valeur
        End If
    End Sub

    Private Sub InfoV_ValeurChange(sender As Object, e As DonneeChangeEventArgs) Handles InfoVT13.ValeurChange

        WsDossierPer = V_Contexte.DossierPER
        If WsDossierPer Is Nothing Then
            Exit Sub
        End If
        Dim NumInfo As Integer = CInt(Strings.Right(CType(sender, Controles_VCoupleVerticalEtiDonnee).ID, 2))
        Dim NumObjet As Integer = CType(sender, Controles_VCoupleVerticalEtiDonnee).V_Objet
        If WsDossierPer.DonneeLue(NumObjet, NumInfo) <> e.Valeur Then
            CType(sender, Controles_VCoupleVerticalEtiDonnee).DonBackColor = V_WebFonction.CouleurMaj
            WsDossierPer.TableauMaj(NumObjet, NumInfo) = e.Valeur
        End If
    End Sub

    Private Sub Dontab_AppelTable(sender As Object, e As AppelTableEventArgs) Handles DontabF01.AppelTable, DontabF02.AppelTable,
         DontabF03.AppelTable, DontabF04.AppelTable, DontabF05.AppelTable, DontabF14.AppelTable, DontabF15.AppelTable, DontabP09.AppelTable,
         DontabP14.AppelTable, DontabT01.AppelTable, DontabT10.AppelTable, DontabT11.AppelTable

        RefVirtualia.V_PointdeVue = e.PointdeVueInverse
        RefVirtualia.V_NomTable = e.NomdelaTable
        Select Case e.ObjetAppelant
            Case VI.ObjetPer.ObaGrade
                V_WebFonction.PointeurContexte.TsTampon_StringList = Nothing
                RefVirtualia.V_Appelant(e.ObjetAppelant, e.DatedeValeur) = e.ControleAppelant
            Case Else
                RefVirtualia.V_Appelant(e.ObjetAppelant) = e.ControleAppelant
        End Select
        HPopupRef.Value = "1"
        CellReference.Visible = True
        PopupReferentiel.Show()
    End Sub

    Public WriteOnly Property Dontab_RetourAppelTable(ByVal IDAppelant As String) As String
        Set(ByVal value As String)
            WsDossierPer = V_Contexte.DossierPER
            If WsDossierPer Is Nothing Then
                Exit Property
            End If
            Dim Ctl As Control
            Dim NumInfo As Integer
            Dim NumObjet As Integer
            Dim VirDonneeTable As Controles_VDuoEtiquetteCommande
            Ctl = V_WebFonction.VirWebControle(Me.CadreInfosAff, "Dontab" & Strings.Right(IDAppelant, 3), 0)
            If Ctl Is Nothing Then
                Exit Property
            End If
            VirDonneeTable = CType(Ctl, Controles_VDuoEtiquetteCommande)
            NumInfo = CInt(Strings.Right(VirDonneeTable.ID, 2))
            NumObjet = VirDonneeTable.V_Objet
            VirDonneeTable.DonText = value
            If WsDossierPer.DonneeLue(NumObjet, NumInfo) <> value Then
                VirDonneeTable.DonBackColor = V_WebFonction.CouleurMaj
                WsDossierPer.TableauMaj(NumObjet, NumInfo) = value
            End If
        End Set
    End Property

    Protected Sub Referentiel_RetourEventHandler(ByVal sender As Object, ByVal e As System.EventArgs) Handles RefVirtualia.RetourEventHandler
        HPopupRef.Value = "0"
        CellReference.Visible = False
    End Sub

    Protected Sub Referentiel_ValeurSelectionnee(ByVal sender As Object, ByVal e As Virtualia.Systeme.Evenements.ValeurSelectionneeEventArgs) Handles RefVirtualia.ValeurSelectionnee
        HPopupRef.Value = "0"
        CellReference.Visible = False
        Dontab_RetourAppelTable(e.ControleAppelant) = e.Valeur
    End Sub

    Private ReadOnly Property ValeurLue(ByVal NoObjet As Integer, ByVal NoInfo As Integer) As String
        Get
            If WsDossierPer Is Nothing Then
                Try
                    WsDossierPer = V_Contexte.DossierPER
                Catch ex As Exception
                    Return ""
                End Try
                If WsDossierPer Is Nothing Then
                    Return ""
                End If
            End If
            Return WsDossierPer.DonneeLue(NoObjet, NoInfo)
        End Get
    End Property
End Class