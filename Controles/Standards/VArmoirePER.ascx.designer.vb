﻿'------------------------------------------------------------------------------
' <généré automatiquement>
'     Ce code a été généré par un outil.
'
'     Les modifications apportées à ce fichier peuvent provoquer un comportement incorrect et seront perdues si
'     le code est régénéré.
' </généré automatiquement>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On


Partial Public Class VArmoirePER

    '''<summary>
    '''Contrôle PanelArmoire.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents PanelArmoire As Global.System.Web.UI.WebControls.Panel

    '''<summary>
    '''Contrôle CadreArmoire.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CadreArmoire As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle TableOnglets.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents TableOnglets As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle MenuChoix.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents MenuChoix As Global.System.Web.UI.WebControls.Menu

    '''<summary>
    '''Contrôle CadreListe.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CadreListe As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle TableTypeArmoire.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents TableTypeArmoire As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle EtiTypeArmoire.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents EtiTypeArmoire As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle DropDownArmoire.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents DropDownArmoire As Global.System.Web.UI.WebControls.DropDownList

    '''<summary>
    '''Contrôle CelluleLettre.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CelluleLettre As Global.System.Web.UI.WebControls.TableCell

    '''<summary>
    '''Contrôle CadreLettre.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CadreLettre As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle ButtonA.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents ButtonA As Global.System.Web.UI.WebControls.ImageButton

    '''<summary>
    '''Contrôle ButtonB.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents ButtonB As Global.System.Web.UI.WebControls.ImageButton

    '''<summary>
    '''Contrôle ButtonC.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents ButtonC As Global.System.Web.UI.WebControls.ImageButton

    '''<summary>
    '''Contrôle ButtonD.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents ButtonD As Global.System.Web.UI.WebControls.ImageButton

    '''<summary>
    '''Contrôle ButtonE.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents ButtonE As Global.System.Web.UI.WebControls.ImageButton

    '''<summary>
    '''Contrôle ButtonF.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents ButtonF As Global.System.Web.UI.WebControls.ImageButton

    '''<summary>
    '''Contrôle ButtonG.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents ButtonG As Global.System.Web.UI.WebControls.ImageButton

    '''<summary>
    '''Contrôle ButtonH.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents ButtonH As Global.System.Web.UI.WebControls.ImageButton

    '''<summary>
    '''Contrôle ButtonI.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents ButtonI As Global.System.Web.UI.WebControls.ImageButton

    '''<summary>
    '''Contrôle ButtonJ.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents ButtonJ As Global.System.Web.UI.WebControls.ImageButton

    '''<summary>
    '''Contrôle ButtonK.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents ButtonK As Global.System.Web.UI.WebControls.ImageButton

    '''<summary>
    '''Contrôle ButtonL.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents ButtonL As Global.System.Web.UI.WebControls.ImageButton

    '''<summary>
    '''Contrôle ButtonM.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents ButtonM As Global.System.Web.UI.WebControls.ImageButton

    '''<summary>
    '''Contrôle ButtonN.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents ButtonN As Global.System.Web.UI.WebControls.ImageButton

    '''<summary>
    '''Contrôle ButtonO.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents ButtonO As Global.System.Web.UI.WebControls.ImageButton

    '''<summary>
    '''Contrôle ButtonP.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents ButtonP As Global.System.Web.UI.WebControls.ImageButton

    '''<summary>
    '''Contrôle ButtonQ.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents ButtonQ As Global.System.Web.UI.WebControls.ImageButton

    '''<summary>
    '''Contrôle ButtonR.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents ButtonR As Global.System.Web.UI.WebControls.ImageButton

    '''<summary>
    '''Contrôle ButtonS.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents ButtonS As Global.System.Web.UI.WebControls.ImageButton

    '''<summary>
    '''Contrôle ButtonT.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents ButtonT As Global.System.Web.UI.WebControls.ImageButton

    '''<summary>
    '''Contrôle ButtonU.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents ButtonU As Global.System.Web.UI.WebControls.ImageButton

    '''<summary>
    '''Contrôle ButtonV.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents ButtonV As Global.System.Web.UI.WebControls.ImageButton

    '''<summary>
    '''Contrôle ButtonW.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents ButtonW As Global.System.Web.UI.WebControls.ImageButton

    '''<summary>
    '''Contrôle ButtonX.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents ButtonX As Global.System.Web.UI.WebControls.ImageButton

    '''<summary>
    '''Contrôle ButtonY.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents ButtonY As Global.System.Web.UI.WebControls.ImageButton

    '''<summary>
    '''Contrôle ButtonZ.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents ButtonZ As Global.System.Web.UI.WebControls.ImageButton

    '''<summary>
    '''Contrôle ButtonAll.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents ButtonAll As Global.System.Web.UI.WebControls.ImageButton

    '''<summary>
    '''Contrôle EtiRecherche.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents EtiRecherche As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle DonRecherche.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents DonRecherche As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''Contrôle PanelTree.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents PanelTree As Global.System.Web.UI.WebControls.Panel

    '''<summary>
    '''Contrôle TreeListeDossier.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents TreeListeDossier As Global.System.Web.UI.WebControls.TreeView

    '''<summary>
    '''Contrôle EtiStatus1.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents EtiStatus1 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle CadrePER.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CadrePER As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle EtiSelection.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents EtiSelection As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle VListePer.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VListePer As Global.System.Web.UI.WebControls.ListBox

    '''<summary>
    '''Contrôle EtiStatus2.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents EtiStatus2 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle CadreBas.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CadreBas As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle PanelCmdBas.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents PanelCmdBas As Global.System.Web.UI.WebControls.Panel

    '''<summary>
    '''Contrôle CadreCmdCsv.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CadreCmdCsv As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle CommandeCsv.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CommandeCsv As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''Contrôle HSelLettre.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents HSelLettre As Global.System.Web.UI.WebControls.HiddenField
End Class
