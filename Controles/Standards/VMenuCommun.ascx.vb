﻿Option Strict On
Option Explicit On
Option Compare Text
Imports VI = Virtualia.Systeme.Constantes
Public Class VMenuCommun
    Inherits System.Web.UI.UserControl
    Private WebFct As Virtualia.Net.Controles.WebFonctions
    Public Delegate Sub Dossier_ClickEventHandler(ByVal sender As Object, ByVal e As Virtualia.Systeme.Evenements.DonneeChangeEventArgs)
    Public Event Menu_Click As Dossier_ClickEventHandler
    Private WsNomState As String = "TreeMenu"
    Private WsListeMenu As List(Of Virtualia.Net.Controles.ItemMenuCommun)
    Private WsAppelant As String

    Protected Overridable Sub VMenu_Click(ByVal e As Virtualia.Systeme.Evenements.DonneeChangeEventArgs)
        RaiseEvent Menu_Click(Me, e)
    End Sub

    Public Property V_Appelant As String
        Get
            Return WsAppelant
        End Get
        Set(ByVal value As String)
            WsAppelant = value
        End Set
    End Property

    Private Property V_DataPathItemSel As String
        Get
            Dim CacheArmoire As ArrayList

            CacheArmoire = CType(Me.ViewState(WsNomState), ArrayList)
            If CacheArmoire Is Nothing Then
                Return ""
            End If
            Return CacheArmoire(0).ToString
        End Get
        Set(ByVal value As String)
            Dim CacheArmoire As ArrayList
            If Me.ViewState(WsNomState) IsNot Nothing Then
                Me.ViewState.Remove(WsNomState)
            End If
            CacheArmoire = New ArrayList
            CacheArmoire.Add(value)
            Me.ViewState.Add(WsNomState, CacheArmoire)
        End Set
    End Property

    Private Sub FaireListeOrganisee()
        Dim IndiceA As Integer
        Dim RuptureN1 As String = ""
        Dim UrlImageN1 = "~/Images/Armoire/BleuFermer16.bmp"
        Dim UrlImagePer = "~/Images/Armoire/FicheBleue.bmp"
        Dim NewNoeudN1 As TreeNode = Nothing
        Dim NewNoeudN2 As TreeNode = Nothing

        TreeListeMenu.Nodes.Clear()
        TreeListeMenu.LevelStyles.Clear()

        Call ChargerMenu()

        For IndiceA = 0 To WsListeMenu.Count - 1
            Select Case WsListeMenu.Item(IndiceA).Groupe
                Case Is <> RuptureN1
                    RuptureN1 = WsListeMenu.Item(IndiceA).Groupe
                    NewNoeudN2 = Nothing

                    NewNoeudN1 = New TreeNode(RuptureN1, "N1" & VI.Tild & IndiceA.ToString)
                    NewNoeudN1.ImageUrl = UrlImageN1
                    NewNoeudN1.PopulateOnDemand = False
                    NewNoeudN1.SelectAction = TreeNodeSelectAction.SelectExpand
                    TreeListeMenu.Nodes.Add(NewNoeudN1)
            End Select

            If NewNoeudN1 IsNot Nothing Then
                NewNoeudN2 = New TreeNode(WsListeMenu.Item(IndiceA).Intitule, CStr(WsListeMenu.Item(IndiceA).Index_Vue))
                NewNoeudN2.ImageUrl = UrlImagePer
                NewNoeudN2.PopulateOnDemand = False
                NewNoeudN2.SelectAction = TreeNodeSelectAction.SelectExpand
                NewNoeudN1.ChildNodes.Add(NewNoeudN2)
            End If

        Next IndiceA

        Call StylerlArmoire(2)

        RuptureN1 = V_DataPathItemSel
        If RuptureN1 <> "" Then
            Try
                NewNoeudN1 = TreeListeMenu.FindNode(RuptureN1)
                NewNoeudN1.Selected = True
            Catch ex As Exception
                NewNoeudN1 = Nothing
            End Try
        End If
        TreeListeMenu.ExpandAll()

    End Sub

    Private Sub StylerlArmoire(ByVal Profondeur As Integer)
        Dim Vstyle As System.Web.UI.WebControls.TreeNodeStyle
        TreeListeMenu.LevelStyles.Clear()

        If Profondeur = 1 Then

            Vstyle = New System.Web.UI.WebControls.TreeNodeStyle
            Vstyle.Font.Name = "Trebuchet MS"
            Vstyle.Font.Size = System.Web.UI.WebControls.FontUnit.Small
            Vstyle.Font.Italic = True
            Vstyle.Font.Bold = False
            Vstyle.BorderWidth = New Unit(0)
            Vstyle.BorderStyle = System.Web.UI.WebControls.BorderStyle.None
            Vstyle.BackColor = Drawing.Color.White

            TreeListeMenu.LevelStyles.Add(Vstyle)
            Exit Sub
        End If

        Vstyle = New System.Web.UI.WebControls.TreeNodeStyle
        Vstyle.ChildNodesPadding = New Unit(10)
        Vstyle.HorizontalPadding = New Unit(20)
        Vstyle.NodeSpacing = New Unit(1)
        Vstyle.Font.Name = "Trebuchet MS"
        Vstyle.Font.Italic = True
        Vstyle.Font.Size = System.Web.UI.WebControls.FontUnit.Small
        Vstyle.Font.Bold = False
        Vstyle.BorderWidth = New Unit(1)
        Vstyle.BorderStyle = System.Web.UI.WebControls.BorderStyle.Ridge
        Vstyle.BackColor = WebFct.ConvertCouleur("#142425")
        Vstyle.ForeColor = WebFct.ConvertCouleur("#B0E0D7")

        TreeListeMenu.LevelStyles.Add(Vstyle)

        If Profondeur = 2 Then

            Vstyle = New System.Web.UI.WebControls.TreeNodeStyle
            Vstyle.Font.Name = "Trebuchet MS"
            Vstyle.Font.Size = System.Web.UI.WebControls.FontUnit.Small
            Vstyle.Font.Italic = True
            Vstyle.Font.Bold = False
            Vstyle.BorderWidth = New Unit(0)
            Vstyle.BorderStyle = System.Web.UI.WebControls.BorderStyle.None
            Vstyle.BackColor = Drawing.Color.White

            TreeListeMenu.LevelStyles.Add(Vstyle)
            Exit Sub
        End If

        Vstyle = New System.Web.UI.WebControls.TreeNodeStyle
        Vstyle.ChildNodesPadding = New Unit(10)
        Vstyle.HorizontalPadding = New Unit(20)
        Vstyle.NodeSpacing = New Unit(1)
        Vstyle.Font.Name = "Trebuchet MS"
        Vstyle.Font.Size = System.Web.UI.WebControls.FontUnit.Small
        Vstyle.Font.Italic = True
        Vstyle.Font.Bold = False
        Vstyle.BorderWidth = New Unit(0)
        Vstyle.BorderStyle = System.Web.UI.WebControls.BorderStyle.Ridge
        Vstyle.BackColor = WebFct.ConvertCouleur("#1C5151")
        Vstyle.ForeColor = Drawing.Color.White

        TreeListeMenu.LevelStyles.Add(Vstyle)

        If Profondeur = 3 Then
            Vstyle = New System.Web.UI.WebControls.TreeNodeStyle
            Vstyle.Font.Name = "Trebuchet MS"
            Vstyle.Font.Size = System.Web.UI.WebControls.FontUnit.Small
            Vstyle.Font.Italic = True
            Vstyle.Font.Bold = False
            Vstyle.BorderWidth = New Unit(0)
            Vstyle.BorderStyle = System.Web.UI.WebControls.BorderStyle.None
            Vstyle.BackColor = Drawing.Color.White

            TreeListeMenu.LevelStyles.Add(Vstyle)
            Exit Sub
        End If

        Vstyle = New System.Web.UI.WebControls.TreeNodeStyle
        Vstyle.ChildNodesPadding = New Unit(10)
        Vstyle.HorizontalPadding = New Unit(20)
        Vstyle.NodeSpacing = New Unit(1)
        Vstyle.Font.Name = "Trebuchet MS"
        Vstyle.Font.Size = System.Web.UI.WebControls.FontUnit.Small
        Vstyle.Font.Italic = True
        Vstyle.Font.Bold = False
        Vstyle.BorderWidth = New Unit(0)
        Vstyle.BorderStyle = System.Web.UI.WebControls.BorderStyle.Ridge
        Vstyle.BackColor = WebFct.ConvertCouleur("#137A76")
        Vstyle.ForeColor = Drawing.Color.White

        TreeListeMenu.LevelStyles.Add(Vstyle)

        If Profondeur = 4 Then
            Vstyle = New System.Web.UI.WebControls.TreeNodeStyle
            Vstyle.Font.Name = "Trebuchet MS"
            Vstyle.Font.Size = System.Web.UI.WebControls.FontUnit.Small
            Vstyle.Font.Italic = True
            Vstyle.Font.Bold = False
            Vstyle.BorderWidth = New Unit(0)
            Vstyle.BorderStyle = System.Web.UI.WebControls.BorderStyle.None
            Vstyle.BackColor = Drawing.Color.White

            TreeListeMenu.LevelStyles.Add(Vstyle)
        End If

    End Sub

    Private Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        WebFct = New Virtualia.Net.Controles.WebFonctions(Me, 0)
    End Sub

    Private Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Call FaireListeOrganisee()
    End Sub

    Private Sub ChargerMenu()
        Dim Chaine As String
        Dim Menu As Virtualia.Net.Controles.ItemMenuCommun
        WsListeMenu = New List(Of Virtualia.Net.Controles.ItemMenuCommun)

        Select Case WsAppelant
            Case "PER"
                Chaine = "ETAT CIVIL ET DOMICILIATIONS"
                Menu = New Virtualia.Net.Controles.ItemMenuCommun
                Menu.Groupe = Chaine
                Menu.Intitule = "Etat civil et situation de famille"
                Menu.Point_de_Vue = VI.PointdeVue.PVueApplicatif
                Menu.Numero_Objet = VI.ObjetPer.ObaCivil
                Menu.Index_Vue = 0
                Menu.ID_Vue = "VueEtatCivil"
                WsListeMenu.Add(Menu)

                Menu = New Virtualia.Net.Controles.ItemMenuCommun
                Menu.Groupe = Chaine
                Menu.Intitule = "Adresse personnelle"
                Menu.Point_de_Vue = VI.PointdeVue.PVueApplicatif
                Menu.Numero_Objet = VI.ObjetPer.ObaAdresse
                Menu.Index_Vue = 1
                Menu.ID_Vue = "VueAdresse"
                WsListeMenu.Add(Menu)

                Menu = New Virtualia.Net.Controles.ItemMenuCommun
                Menu.Groupe = Chaine
                Menu.Intitule = "Fiche Banque RIB et IBAN"
                Menu.Point_de_Vue = VI.PointdeVue.PVueApplicatif
                Menu.Numero_Objet = VI.ObjetPer.ObaBanque
                Menu.Index_Vue = 2
                Menu.ID_Vue = "VueBanque"
                WsListeMenu.Add(Menu)

                Chaine = "ENFANTS"
                Menu = New Virtualia.Net.Controles.ItemMenuCommun
                Menu.Groupe = Chaine
                Menu.Intitule = "Liste des enfants"
                Menu.Point_de_Vue = VI.PointdeVue.PVueApplicatif
                Menu.Numero_Objet = VI.ObjetPer.ObaEnfant
                Menu.Index_Vue = 3
                Menu.ID_Vue = "VueEnfant"
                WsListeMenu.Add(Menu)

                Chaine = "SITUATION ADMINISTRATIVE"
                Menu = New Virtualia.Net.Controles.ItemMenuCommun
                Menu.Groupe = Chaine
                Menu.Intitule = "Administration / Etablissement"
                Menu.Point_de_Vue = VI.PointdeVue.PVueApplicatif
                Menu.Numero_Objet = VI.ObjetPer.ObaSociete
                Menu.Index_Vue = 4
                Menu.ID_Vue = "VueAdministration"
                WsListeMenu.Add(Menu)

                Menu = New Virtualia.Net.Controles.ItemMenuCommun
                Menu.Groupe = Chaine
                Menu.Intitule = "Services antèrieurs"
                Menu.Point_de_Vue = VI.PointdeVue.PVueApplicatif
                Menu.Numero_Objet = VI.ObjetPer.ObaOrigine
                Menu.Index_Vue = 5
                Menu.ID_Vue = "VueAdmOrigine"
                WsListeMenu.Add(Menu)

                Menu = New Virtualia.Net.Controles.ItemMenuCommun
                Menu.Groupe = Chaine
                If System.Configuration.ConfigurationManager.AppSettings("TypeBudget") = "SIFAC" Then
                    Menu.Intitule = "Ventilation budgétaire"
                    Menu.Point_de_Vue = VI.PointdeVue.PVueApplicatif
                    Menu.Numero_Objet = 180
                    Menu.Index_Vue = 6
                    Menu.ID_Vue = "VueLOLF"
                Else
                    Menu.Intitule = "Affectation LOLF"
                    Menu.Point_de_Vue = VI.PointdeVue.PVueApplicatif
                    Menu.Numero_Objet = VI.ObjetPer.ObaLolf
                    Menu.Index_Vue = 6
                    Menu.ID_Vue = "VueLOLF"
                End If
                WsListeMenu.Add(Menu)

                Menu = New Virtualia.Net.Controles.ItemMenuCommun
                Menu.Groupe = Chaine
                Menu.Intitule = "Affectation fonctionnelle"
                Menu.Point_de_Vue = VI.PointdeVue.PVueApplicatif
                Menu.Numero_Objet = VI.ObjetPer.ObaOrganigramme
                Menu.Index_Vue = 7
                Menu.ID_Vue = "VueAffectation"
                WsListeMenu.Add(Menu)

                Menu = New Virtualia.Net.Controles.ItemMenuCommun
                Menu.Groupe = Chaine
                Menu.Intitule = "Adresse professionnelle"
                Menu.Point_de_Vue = VI.PointdeVue.PVueApplicatif
                Menu.Numero_Objet = VI.ObjetPer.ObaAdrPro
                Menu.Index_Vue = 8
                Menu.ID_Vue = "VueAdressePro"
                WsListeMenu.Add(Menu)

                Menu = New Virtualia.Net.Controles.ItemMenuCommun
                Menu.Groupe = Chaine
                Menu.Intitule = "Affiliations sociales"
                Menu.Point_de_Vue = VI.PointdeVue.PVueApplicatif
                Menu.Numero_Objet = VI.ObjetPer.ObaCaisse
                Menu.Index_Vue = 9
                Menu.ID_Vue = "VueCaisse"
                WsListeMenu.Add(Menu)

                Menu = New Virtualia.Net.Controles.ItemMenuCommun
                Menu.Groupe = Chaine
                Menu.Intitule = "Mutuelle"
                Menu.Point_de_Vue = VI.PointdeVue.PVueApplicatif
                Menu.Numero_Objet = VI.ObjetPer.ObaMutuelle
                Menu.Index_Vue = 10
                Menu.ID_Vue = "VueMutuelle"
                WsListeMenu.Add(Menu)

                Menu = New Virtualia.Net.Controles.ItemMenuCommun
                Menu.Groupe = Chaine
                Menu.Intitule = "Absences et congés maladie"
                Menu.Point_de_Vue = VI.PointdeVue.PVueApplicatif
                Menu.Numero_Objet = VI.ObjetPer.ObaAbsence
                Menu.Index_Vue = 11
                Menu.ID_Vue = "VueAbsence"
                WsListeMenu.Add(Menu)

                Chaine = "CARRIERE"
                Menu = New Virtualia.Net.Controles.ItemMenuCommun
                Menu.Groupe = Chaine
                Menu.Intitule = "Situation statutaire"
                Menu.Point_de_Vue = VI.PointdeVue.PVueApplicatif
                Menu.Numero_Objet = VI.ObjetPer.ObaStatut
                Menu.Index_Vue = 12
                Menu.ID_Vue = "VueStatut"
                WsListeMenu.Add(Menu)

                Menu = New Virtualia.Net.Controles.ItemMenuCommun
                Menu.Groupe = Chaine
                Menu.Intitule = "Position et modalité de service"
                Menu.Point_de_Vue = VI.PointdeVue.PVueApplicatif
                Menu.Numero_Objet = VI.ObjetPer.ObaActivite
                Menu.Index_Vue = 13
                Menu.ID_Vue = "VuePosition"
                WsListeMenu.Add(Menu)

                Menu = New Virtualia.Net.Controles.ItemMenuCommun
                Menu.Groupe = Chaine
                Menu.Intitule = "Corps, Grade et échelon"
                Menu.Point_de_Vue = VI.PointdeVue.PVueApplicatif
                Menu.Numero_Objet = VI.ObjetPer.ObaGrade
                Menu.Index_Vue = 14
                Menu.ID_Vue = "VueGrade"
                WsListeMenu.Add(Menu)

                Chaine = "DOUBLE CARRIERE - DETACHEMENT INTERNE"
                Menu = New Virtualia.Net.Controles.ItemMenuCommun
                Menu.Groupe = Chaine
                Menu.Intitule = "Situation statutaire"
                Menu.Point_de_Vue = VI.PointdeVue.PVueApplicatif
                Menu.Numero_Objet = VI.ObjetPer.ObaStatutDetache
                Menu.Index_Vue = 15
                Menu.ID_Vue = "VueStatutDetache"
                WsListeMenu.Add(Menu)

                Menu = New Virtualia.Net.Controles.ItemMenuCommun
                Menu.Groupe = Chaine
                Menu.Intitule = "Position et modalité de service"
                Menu.Point_de_Vue = VI.PointdeVue.PVueApplicatif
                Menu.Numero_Objet = VI.ObjetPer.ObaPositionDetache
                Menu.Index_Vue = 16
                Menu.ID_Vue = "VuePositionDetache"
                WsListeMenu.Add(Menu)

                Menu = New Virtualia.Net.Controles.ItemMenuCommun
                Menu.Groupe = Chaine
                Menu.Intitule = "Corps, Grade et échelon"
                Menu.Point_de_Vue = VI.PointdeVue.PVueApplicatif
                Menu.Numero_Objet = VI.ObjetPer.ObaGradeDetache
                Menu.Index_Vue = 17
                Menu.ID_Vue = "VueGradeDetache"
                WsListeMenu.Add(Menu)

                Chaine = "INDEMNITES ET HEURES SUPPLEMENTAIRES"
                Menu = New Virtualia.Net.Controles.ItemMenuCommun
                Menu.Groupe = Chaine
                Menu.Intitule = "Primes et indemnités permanentes"
                Menu.Point_de_Vue = VI.PointdeVue.PVueApplicatif
                Menu.Numero_Objet = VI.ObjetPer.ObaIndemnite
                Menu.Index_Vue = 18
                Menu.ID_Vue = "VuePrime"
                WsListeMenu.Add(Menu)

                Menu = New Virtualia.Net.Controles.ItemMenuCommun
                Menu.Groupe = Chaine
                Menu.Intitule = "Variables de paie et allocations"
                Menu.Point_de_Vue = VI.PointdeVue.PVueApplicatif
                Menu.Numero_Objet = VI.ObjetPer.ObaVariablePaie
                Menu.Index_Vue = 19
                Menu.ID_Vue = "VueVariablePaie"
                WsListeMenu.Add(Menu)

                Menu = New Virtualia.Net.Controles.ItemMenuCommun
                Menu.Groupe = Chaine
                Menu.Intitule = "Heures supplémentaires"
                Menu.Point_de_Vue = VI.PointdeVue.PVueApplicatif
                Menu.Numero_Objet = VI.ObjetPer.ObaHeuresSup
                Menu.Index_Vue = 20
                Menu.ID_Vue = "VueHeureSup"
                WsListeMenu.Add(Menu)
            Case "REF"

                Chaine = "REGLES GENERALES ET VALEURS PAR DEFAUT"
                Menu = New Virtualia.Net.Controles.ItemMenuCommun
                Menu.Groupe = Chaine
                Menu.Intitule = "Absences"
                Menu.Point_de_Vue = VI.PointdeVue.PVueAbsences
                Menu.Numero_Objet = 1
                Menu.Index_Vue = 0
                Menu.ID_Vue = "VueAbsences"
                WsListeMenu.Add(Menu)

                Menu = New Virtualia.Net.Controles.ItemMenuCommun
                Menu.Groupe = Chaine
                Menu.Intitule = "Présences"
                Menu.Point_de_Vue = VI.PointdeVue.PVuePresences
                Menu.Numero_Objet = 1
                Menu.Index_Vue = 0
                Menu.ID_Vue = "VuePresences"
                WsListeMenu.Add(Menu)

                Menu = New Virtualia.Net.Controles.ItemMenuCommun
                Menu.Groupe = Chaine
                Menu.Intitule = "Couleurs du planning"
                Menu.Point_de_Vue = VI.PointdeVue.PVuePaie
                Menu.Numero_Objet = 1
                Menu.Index_Vue = 0
                Menu.ID_Vue = "VueCouleurs"
                WsListeMenu.Add(Menu)

                Menu = New Virtualia.Net.Controles.ItemMenuCommun
                Menu.Groupe = Chaine
                Menu.Intitule = "Jour ouvrable"
                Menu.Point_de_Vue = VI.PointdeVue.PVuePaie
                Menu.Numero_Objet = 1
                Menu.Index_Vue = 0
                Menu.ID_Vue = "VueJourOuvrable"
                WsListeMenu.Add(Menu)

                Menu = New Virtualia.Net.Controles.ItemMenuCommun
                Menu.Groupe = Chaine
                Menu.Intitule = "Jours fériés mobiles"
                Menu.Point_de_Vue = VI.PointdeVue.PVuePaie
                Menu.Numero_Objet = 1
                Menu.Index_Vue = 0
                Menu.ID_Vue = "VueJoursFeries"
                WsListeMenu.Add(Menu)

                Menu = New Virtualia.Net.Controles.ItemMenuCommun
                Menu.Groupe = Chaine
                Menu.Intitule = "Droits à congés annuels"
                Menu.Point_de_Vue = VI.PointdeVue.PVuePaie
                Menu.Numero_Objet = 1
                Menu.Index_Vue = 0
                Menu.ID_Vue = "VueDroitsConges"
                WsListeMenu.Add(Menu)

                Menu = New Virtualia.Net.Controles.ItemMenuCommun
                Menu.Groupe = Chaine
                Menu.Intitule = "Droits à congés maladies"
                Menu.Point_de_Vue = VI.PointdeVue.PVuePaie
                Menu.Numero_Objet = 1
                Menu.Index_Vue = 0
                Menu.ID_Vue = "VueMaladies"
                WsListeMenu.Add(Menu)

                Menu = New Virtualia.Net.Controles.ItemMenuCommun
                Menu.Groupe = Chaine
                Menu.Intitule = "Temps de travail légal"
                Menu.Point_de_Vue = VI.PointdeVue.PVuePaie
                Menu.Numero_Objet = 1
                Menu.Index_Vue = 0
                Menu.ID_Vue = "VueTravailLegal"
                WsListeMenu.Add(Menu)

                Chaine = "REGLES LOCALES"
                Menu = New Virtualia.Net.Controles.ItemMenuCommun
                Menu.Groupe = Chaine
                Menu.Intitule = "Etablissements"
                Menu.Point_de_Vue = VI.PointdeVue.PVueEtablissement
                Menu.Numero_Objet = 1
                Menu.Index_Vue = 0
                Menu.ID_Vue = "VueEtablissements"
                WsListeMenu.Add(Menu)

                Menu = New Virtualia.Net.Controles.ItemMenuCommun
                Menu.Groupe = Chaine
                Menu.Intitule = "Pays"
                Menu.Point_de_Vue = VI.PointdeVue.PVuePays
                Menu.Numero_Objet = 1
                Menu.Index_Vue = 0
                Menu.ID_Vue = "VuePays"
                WsListeMenu.Add(Menu)

                Chaine = "CYCLES DE TRAVAIL"
                Menu = New Virtualia.Net.Controles.ItemMenuCommun
                Menu.Groupe = Chaine
                Menu.Intitule = "Cycles de travail"
                Menu.Point_de_Vue = VI.PointdeVue.PVueCycle
                Menu.Numero_Objet = 1
                Menu.Index_Vue = 0
                Menu.ID_Vue = "VueCyclesTravail"
                WsListeMenu.Add(Menu)

                Menu = New Virtualia.Net.Controles.ItemMenuCommun
                Menu.Groupe = Chaine
                Menu.Intitule = "Unités de cycle ou cycles de base"
                Menu.Point_de_Vue = VI.PointdeVue.PVueBaseHebdo
                Menu.Numero_Objet = 1
                Menu.Index_Vue = 0
                Menu.ID_Vue = "VueCyclesUnite"
                WsListeMenu.Add(Menu)
                
                Chaine = "REGLES DE VALORISATION"
                Menu = New Virtualia.Net.Controles.ItemMenuCommun
                Menu.Groupe = Chaine
                Menu.Intitule = "Règles de valorisation des heures travaillées"
                Menu.Point_de_Vue = VI.PointdeVue.PVueValTemps
                Menu.Numero_Objet = 1
                Menu.Index_Vue = 0
                Menu.ID_Vue = "VueValTemps"
                WsListeMenu.Add(Menu)

        End Select

    End Sub

    Private Sub TreeListeMenu_SelectedNodeChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles TreeListeMenu.SelectedNodeChanged
        V_DataPathItemSel = CType(sender, System.Web.UI.WebControls.TreeView).SelectedNode.ValuePath

        Dim Evenement As Virtualia.Systeme.Evenements.DonneeChangeEventArgs = Nothing
        Evenement = New Virtualia.Systeme.Evenements.DonneeChangeEventArgs(CType(sender, System.Web.UI.WebControls.TreeView).SelectedNode.Value, _
                                                            CType(sender, System.Web.UI.WebControls.TreeView).SelectedNode.Text)
        VMenu_Click(Evenement)
    End Sub

End Class