﻿Option Strict On
Option Explicit On
Option Compare Text
Imports VI = Virtualia.Systeme.Constantes
Public Class VGrillePersonnalisee
    Inherits System.Web.UI.UserControl
    Public Delegate Sub Valeur_ChangeEventHandler(ByVal sender As Object, ByVal e As Virtualia.Systeme.Evenements.DonneeChangeEventArgs)
    Public Event ValeurChange As Valeur_ChangeEventHandler
    Private WebFct As Virtualia.Net.Controles.WebFonctions
    Private WsNbLignes As Integer = 20
    Private WsNbColonnes As Integer = 7
    Private WsSiColonneCommande As Boolean = False
    Private WsStyleDonnee(6) As String
    Private WsSiStyleAlterne As Boolean = False
    Private WsBackColorSelected As System.Drawing.Color = Drawing.Color.Empty
    Private WsForeColorSelected As System.Drawing.Color = Drawing.Color.Empty

    Private WsNomStateCache As String = "VPersonnalise"
    Private WsCtl_Cache As Virtualia.Net.VCaches.CacheControleListe
    '
    Protected Overridable Sub Saisie_Change(ByVal e As Virtualia.Systeme.Evenements.DonneeChangeEventArgs)
        RaiseEvent ValeurChange(Me, e)
    End Sub

    Private Property CacheVirControle As Virtualia.Net.VCaches.CacheControleListe
        Get
            If Me.ViewState(WsNomStateCache) IsNot Nothing Then
                Return CType(Me.ViewState(WsNomStateCache), Virtualia.Net.VCaches.CacheControleListe)
            End If
            Dim NewCache As Virtualia.Net.VCaches.CacheControleListe
            NewCache = New Virtualia.Net.VCaches.CacheControleListe
            Return NewCache
        End Get
        Set(value As Virtualia.Net.VCaches.CacheControleListe)
            If value Is Nothing Then
                Exit Property
            End If
            If Me.ViewState(WsNomStateCache) IsNot Nothing Then
                Me.ViewState.Remove(WsNomStateCache)
            End If
            Me.ViewState.Add(WsNomStateCache, value)
        End Set
    End Property

    Public Property V_Liste() As List(Of String)
        Get
            WsCtl_Cache = CacheVirControle
            Return WsCtl_Cache.Liste_Valeurs
        End Get
        Set(ByVal value As List(Of String))
            WsCtl_Cache = CacheVirControle
            WsCtl_Cache.Liste_Valeurs = value
            CacheVirControle = WsCtl_Cache
        End Set
    End Property

    Public Property V_LibelColonne() As List(Of String)
        Get
            WsCtl_Cache = CacheVirControle
            Return WsCtl_Cache.Libelles_Colonne
        End Get
        Set(ByVal value As List(Of String))
            WsCtl_Cache = CacheVirControle
            WsCtl_Cache.Libelles_Colonne = value
            CacheVirControle = WsCtl_Cache
        End Set
    End Property

    Public Property V_LibelCaption() As List(Of String)
        Get
            WsCtl_Cache = CacheVirControle
            Return WsCtl_Cache.Libelles_Caption
        End Get
        Set(ByVal value As List(Of String))
            WsCtl_Cache = CacheVirControle
            WsCtl_Cache.Libelles_Caption = value
            CacheVirControle = WsCtl_Cache
        End Set
    End Property

    Public Property V_Clef_ItemSelectionne() As String
        Get
            WsCtl_Cache = CacheVirControle
            Return WsCtl_Cache.Clef_ItemSelectionne
        End Get
        Set(ByVal value As String)
            WsCtl_Cache = CacheVirControle
            WsCtl_Cache.Clef_ItemSelectionne = value
            CacheVirControle = WsCtl_Cache
        End Set
    End Property

    Public Property V_ItemSelectionne() As String
        Get
            WsCtl_Cache = CacheVirControle
            Return WsCtl_Cache.Valeur_ItemSelectionne
        End Get
        Set(ByVal value As String)
            WsCtl_Cache = CacheVirControle
            WsCtl_Cache.Valeur_ItemSelectionne = value
            CacheVirControle = WsCtl_Cache
        End Set
    End Property

    Public Property V_NumColonne_Filtre As Integer
        Get
            WsCtl_Cache = CacheVirControle
            Return WsCtl_Cache.Colonne_Filtre
        End Get
        Set(value As Integer)
            WsCtl_Cache = CacheVirControle
            WsCtl_Cache.Colonne_Filtre = value
            CacheVirControle = WsCtl_Cache
        End Set
    End Property

    Public WriteOnly Property V_Filtres(ByVal Libelles As List(Of String)) As List(Of String)
        Set(ByVal value As List(Of String))
            LstFiltres.V_Libel = Libelles
            LstFiltres.V_Liste = value
        End Set
    End Property

    Public Property Nombre_Lignes_Page As Integer
        Get
            Return WsNbLignes
        End Get
        Set(value As Integer)
            WsNbLignes = value
        End Set
    End Property

    Public Property Nombre_Colonnes As Integer
        Get
            Return WsNbColonnes
        End Get
        Set(value As Integer)
            Select Case value
                Case 1 To 7
                    WsNbColonnes = value
                Case Else
                    WsNbColonnes = 7
            End Select
        End Set
    End Property

    Public Property LargeurCadre As System.Web.UI.WebControls.Unit
        Get
            Return PanelGrille.Width
        End Get
        Set(value As System.Web.UI.WebControls.Unit)
            PanelGrille.Width = value
        End Set
    End Property

    Public Property HauteurCadre As System.Web.UI.WebControls.Unit
        Get
            Return PanelGrille.Height
        End Get
        Set(value As System.Web.UI.WebControls.Unit)
            PanelGrille.Height = value
        End Set
    End Property

    Public Property BackColorCadre As System.Drawing.Color
        Get
            Return PanelGrille.BackColor
        End Get
        Set(ByVal value As System.Drawing.Color)
            PanelGrille.BackColor = value
        End Set
    End Property

    Public Property SiColonneCommande As Boolean
        Get
            Return WsSiColonneCommande
        End Get
        Set(value As Boolean)
            WsSiColonneCommande = value
        End Set
    End Property

    Public Property UrlImageCommande As String
        Get
            Return CmdCol_L01.ImageUrl
        End Get
        Set(value As String)
            CmdCol_L01.ImageUrl = value
        End Set
    End Property

    Public Property ToolTipImageCommande As String
        Get
            Return CmdCol_L01.ToolTip
        End Get
        Set(value As String)
            CmdCol_L01.ToolTip = value
        End Set
    End Property

    Public Property SiCmdVisible(ByVal NoLigne As Integer) As Boolean
        Get
            Dim Ctl As Control
            Ctl = V_WebFonction.VirWebControle(Me.CadreGrille, "CellCmd", NoLigne - 1)
            If Ctl Is Nothing Then
                Return True
            End If
            Return CType(Ctl, System.Web.UI.WebControls.TableCell).Visible
        End Get
        Set(value As Boolean)
            Dim Ctl As Control
            Ctl = V_WebFonction.VirWebControle(Me.CadreGrille, "CellCmd", NoLigne - 1)
            If Ctl Is Nothing Then
                Exit Property
            End If
            CType(Ctl, System.Web.UI.WebControls.TableCell).Visible = value
        End Set
    End Property

    Public Property SiFiltreVisible As Boolean
        Get
            Return CellFiltre.Visible
        End Get
        Set(value As Boolean)
            CellFiltre.Visible = value
        End Set
    End Property

    Public Property SiTitreVisible As Boolean
        Get
            Return CellTitre.Visible
        End Get
        Set(value As Boolean)
            CellTitre.Visible = value
        End Set
    End Property

    Public Property Titre As String
        Get
            Return EtiTitre.Text
        End Get
        Set(value As String)
            EtiTitre.Text = value
        End Set
    End Property

    Public Property LargeurTitre As System.Web.UI.WebControls.Unit
        Get
            Return EtiTitre.Width
        End Get
        Set(value As System.Web.UI.WebControls.Unit)
            EtiTitre.Width = value
            CellTitre.Width = New Unit(value.Value + 10)
        End Set
    End Property

    Public Property BackColorTitre As System.Drawing.Color
        Get
            Return EtiTitre.BackColor
        End Get
        Set(ByVal value As System.Drawing.Color)
            EtiTitre.BackColor = value
        End Set
    End Property

    Public Property ForeColorTitre As System.Drawing.Color
        Get
            Return EtiTitre.ForeColor
        End Get
        Set(ByVal value As System.Drawing.Color)
            EtiTitre.ForeColor = value
        End Set
    End Property

    Private Property LibelleColonne(ByVal Index As Integer) As String
        Get
            Dim Ctl As Control
            Ctl = V_WebFonction.VirWebControle(Me.CadreEtiquettes, "EtiCol", Index - 1)
            If Ctl Is Nothing Then
                Return ""
            End If
            Return CType(Ctl, System.Web.UI.WebControls.Label).Text
        End Get
        Set(value As String)
            Dim Ctl As Control
            Ctl = V_WebFonction.VirWebControle(Me.CadreEtiquettes, "EtiCol", Index - 1)
            If Ctl Is Nothing Then
                Exit Property
            End If
            CType(Ctl, System.Web.UI.WebControls.Label).Text = value
        End Set
    End Property

    Public Property SiReadOnly(ByVal Index As Integer) As Boolean
        Get
            Dim Ctl As Control
            Ctl = V_WebFonction.VirWebControle(Me.Ligne01, "DonCol", Index - 1)
            If Ctl Is Nothing Then
                Return True
            End If
            Return CType(Ctl, System.Web.UI.WebControls.TextBox).ReadOnly
        End Get
        Set(value As Boolean)
            Dim Ctl As Control
            Ctl = V_WebFonction.VirWebControle(Me.Ligne01, "DonCol", Index - 1)
            If Ctl Is Nothing Then
                Exit Property
            End If
            CType(Ctl, System.Web.UI.WebControls.TextBox).ReadOnly = value
        End Set
    End Property

    Public Property SiAutoPostBack(ByVal Index As Integer) As Boolean
        Get
            Dim Ctl As Control
            Ctl = V_WebFonction.VirWebControle(Me.Ligne01, "DonCol", Index - 1)
            If Ctl Is Nothing Then
                Return True
            End If
            Return CType(Ctl, System.Web.UI.WebControls.TextBox).AutoPostBack
        End Get
        Set(value As Boolean)
            Dim Ctl As Control
            Ctl = V_WebFonction.VirWebControle(Me.Ligne01, "DonCol", Index - 1)
            If Ctl Is Nothing Then
                Exit Property
            End If
            CType(Ctl, System.Web.UI.WebControls.TextBox).AutoPostBack = value
        End Set
    End Property

    Public Property BackColorColonnes As System.Drawing.Color
        Get
            Return CellEti1.BackColor
        End Get
        Set(ByVal value As System.Drawing.Color)
            Dim Ctl As Control
            Dim IndiceI As Integer
            IndiceI = 0
            Do
                Ctl = V_WebFonction.VirWebControle(Me.CadreEtiquettes, "CellEti", IndiceI)
                If Ctl Is Nothing Then
                    Exit Property
                End If
                CType(Ctl, System.Web.UI.WebControls.TableCell).BackColor = value
                IndiceI += 1
            Loop
        End Set
    End Property

    Public Property ForeColorColonnes As System.Drawing.Color
        Get
            Return EtiCol1.ForeColor
        End Get
        Set(ByVal value As System.Drawing.Color)
            Dim Ctl As Control
            Dim IndiceI As Integer
            IndiceI = 0
            Do
                Ctl = V_WebFonction.VirWebControle(Me.CadreEtiquettes, "EtiCol", IndiceI)
                If Ctl Is Nothing Then
                    Exit Property
                End If
                CType(Ctl, System.Web.UI.WebControls.Label).ForeColor = value
                IndiceI += 1
            Loop
        End Set
    End Property

    Public WriteOnly Property Style_Colonne_1 As String
        Set(value As String)
            WsStyleDonnee(0) = value
            Call Styler_Donnee(value, DonCol_L01_1)
        End Set
    End Property

    Public WriteOnly Property Style_Colonne_2 As String
        Set(value As String)
            WsStyleDonnee(1) = value
            Call Styler_Donnee(value, DonCol_L01_2)
        End Set
    End Property

    Public WriteOnly Property Style_Colonne_3 As String
        Set(value As String)
            WsStyleDonnee(2) = value
            Call Styler_Donnee(value, DonCol_L01_3)
        End Set
    End Property

    Public WriteOnly Property Style_Colonne_4 As String
        Set(value As String)
            WsStyleDonnee(3) = value
            Call Styler_Donnee(value, DonCol_L01_4)
        End Set
    End Property

    Public WriteOnly Property Style_Colonne_5 As String
        Set(value As String)
            WsStyleDonnee(4) = value
            Call Styler_Donnee(value, DonCol_L01_5)
        End Set
    End Property

    Public WriteOnly Property Style_Colonne_6 As String
        Set(value As String)
            WsStyleDonnee(5) = value
            Call Styler_Donnee(value, DonCol_L01_6)
        End Set
    End Property

    Public WriteOnly Property Style_Colonne_7 As String
        Set(value As String)
            WsStyleDonnee(6) = value
            Call Styler_Donnee(value, DonCol_L01_7)
        End Set
    End Property

    Public Property Taille_Colonne(ByVal Index As Integer) As System.Web.UI.WebControls.Unit
        Get
            Dim Ctl As Control
            Ctl = V_WebFonction.VirWebControle(Me.Ligne01, "CellDon", Index - 1)
            If Ctl Is Nothing Then
                Return 0
            End If
            Return CType(Ctl, System.Web.UI.WebControls.TableCell).Width
        End Get
        Set(value As System.Web.UI.WebControls.Unit)
            Dim Ctl As Control
            Ctl = V_WebFonction.VirWebControle(Me.CadreEtiquettes, "CellEti", Index - 1)
            If Ctl Is Nothing Then
                Exit Property
            End If
            CType(Ctl, System.Web.UI.WebControls.TableCell).Width = value
            Ctl = V_WebFonction.VirWebControle(Me.CadreEtiquettes, "EtiCol", Index - 1)
            If Ctl Is Nothing Then
                Exit Property
            End If
            CType(Ctl, System.Web.UI.WebControls.Label).Width = New Unit(value.Value - 10)
            Ctl = V_WebFonction.VirWebControle(Me.Ligne01, "CellDon", Index - 1)
            If Ctl Is Nothing Then
                Exit Property
            End If
            CType(Ctl, System.Web.UI.WebControls.TableCell).Width = value
            Ctl = V_WebFonction.VirWebControle(Me.Ligne01, "DonCol", Index - 1)
            If Ctl Is Nothing Then
                Exit Property
            End If
            CType(Ctl, System.Web.UI.WebControls.TextBox).Width = New Unit(value.Value - 10)
        End Set
    End Property

    Public Property Taille_Colonne_1 As System.Web.UI.WebControls.Unit
        Get
            Return CellDon_L01_1.Width
        End Get
        Set(value As System.Web.UI.WebControls.Unit)
            CellEti1.Width = value
            EtiCol1.Width = New Unit(value.Value - 10)
            CellDon_L01_1.Width = value
            DonCol_L01_1.Width = New Unit(value.Value - 10)
        End Set
    End Property

    Public Property Taille_Colonne_2 As System.Web.UI.WebControls.Unit
        Get
            Return CellDon_L01_2.Width
        End Get
        Set(value As System.Web.UI.WebControls.Unit)
            CellEti2.Width = value
            EtiCol2.Width = New Unit(value.Value - 10)
            CellDon_L01_2.Width = value
            DonCol_L01_2.Width = New Unit(value.Value - 10)
        End Set
    End Property

    Public Property Taille_Colonne_3 As System.Web.UI.WebControls.Unit
        Get
            Return CellDon_L01_3.Width
        End Get
        Set(value As System.Web.UI.WebControls.Unit)
            CellEti3.Width = value
            EtiCol3.Width = New Unit(value.Value - 10)
            CellDon_L01_3.Width = value
            DonCol_L01_3.Width = New Unit(value.Value - 10)
        End Set
    End Property

    Public Property Taille_Colonne_4 As System.Web.UI.WebControls.Unit
        Get
            Return CellDon_L01_4.Width
        End Get
        Set(value As System.Web.UI.WebControls.Unit)
            CellEti4.Width = value
            EtiCol4.Width = New Unit(value.Value - 10)
            CellDon_L01_4.Width = value
            DonCol_L01_4.Width = New Unit(value.Value - 10)
        End Set
    End Property

    Public Property Taille_Colonne_5 As System.Web.UI.WebControls.Unit
        Get
            Return CellDon_L01_5.Width
        End Get
        Set(value As System.Web.UI.WebControls.Unit)
            CellEti5.Width = value
            EtiCol5.Width = New Unit(value.Value - 10)
            CellDon_L01_5.Width = value
            DonCol_L01_5.Width = New Unit(value.Value - 10)
        End Set
    End Property

    Public Property Taille_Colonne_6 As System.Web.UI.WebControls.Unit
        Get
            Return CellDon_L01_6.Width
        End Get
        Set(value As System.Web.UI.WebControls.Unit)
            CellEti6.Width = value
            EtiCol6.Width = New Unit(value.Value - 10)
            CellDon_L01_6.Width = value
            DonCol_L01_6.Width = New Unit(value.Value - 10)
        End Set
    End Property

    Public Property Taille_Colonne_7 As System.Web.UI.WebControls.Unit
        Get
            Return CellDon_L01_7.Width
        End Get
        Set(value As System.Web.UI.WebControls.Unit)
            CellEti7.Width = value
            EtiCol7.Width = New Unit(value.Value - 10)
            CellDon_L01_7.Width = value
            DonCol_L01_7.Width = New Unit(value.Value - 10)
        End Set
    End Property

    Public Property SiStyleAlterne As Boolean
        Get
            Return WsSiStyleAlterne
        End Get
        Set(ByVal value As Boolean)
            WsSiStyleAlterne = value
        End Set
    End Property

    Public Property BackColorSelected As System.Drawing.Color
        Get
            If WsBackColorSelected = Drawing.Color.Empty Then
                WsBackColorSelected = V_WebFonction.ConvertCouleur("#D1DDF1")
            End If
            Return WsBackColorSelected
        End Get
        Set(ByVal value As System.Drawing.Color)
            WsBackColorSelected = value
        End Set
    End Property

    Public Property ForeColorSelected As System.Drawing.Color
        Get
            If WsForeColorSelected = Drawing.Color.Empty Then
                WsForeColorSelected = V_WebFonction.ConvertCouleur("#333333")
            End If
            Return WsForeColorSelected
        End Get
        Set(ByVal value As System.Drawing.Color)
            WsForeColorSelected = value
        End Set
    End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Call Initialiser()
    End Sub

    Private Sub Page_PreRender(sender As Object, e As System.EventArgs) Handles Me.PreRender
        Call FaireListe()
    End Sub

    Private Sub LstFiltres_ValeurChange(sender As Object, e As Systeme.Evenements.DonneeChangeEventArgs) Handles LstFiltres.ValeurChange
        WsCtl_Cache = CacheVirControle
        WsCtl_Cache.Valeur_Filtre = e.Valeur
        CacheVirControle = WsCtl_Cache
    End Sub

    Private Sub FDateDebut_ValeurChange(sender As Object, e As Systeme.Evenements.DonneeChangeEventArgs) Handles DDateDebut.ValeurChange
        WsCtl_Cache = CacheVirControle
        WsCtl_Cache.Date_Debut_Filtre = e.Valeur
        CacheVirControle = WsCtl_Cache
    End Sub

    Private Sub FDateFin_ValeurChange(sender As Object, e As Systeme.Evenements.DonneeChangeEventArgs) Handles DDateFin.ValeurChange
        WsCtl_Cache = CacheVirControle
        WsCtl_Cache.Date_Fin_Filtre = e.Valeur
        CacheVirControle = WsCtl_Cache
    End Sub

    Private Sub CmdPrecedent_Click(sender As Object, e As ImageClickEventArgs) Handles CmdPrecedent.Click
        WsCtl_Cache = CacheVirControle
        WsCtl_Cache.Numero_Page -= 1
        CacheVirControle = WsCtl_Cache
    End Sub

    Private Sub CmdSuivant_Click(sender As Object, e As ImageClickEventArgs) Handles CmdSuivant.Click
        WsCtl_Cache = CacheVirControle
        WsCtl_Cache.Numero_Page += 1
        CacheVirControle = WsCtl_Cache
    End Sub

    Private Sub Initialiser()
        Dim NumInfo As Integer
        Dim Ctl As Control
        Dim IndiceI As Integer = 1
        Dim Ligne As System.Web.UI.WebControls.TableRow
        Dim Cellule As System.Web.UI.WebControls.TableCell

        Do
            Ctl = V_WebFonction.VirWebControle(Me.CadreEtiquettes, "CellEti", IndiceI)
            If Ctl Is Nothing Then
                Exit Do
            End If
            NumInfo = CInt(Strings.Right(Ctl.ID, 1))
            If NumInfo > Nombre_Colonnes Then
                CType(Ctl, System.Web.UI.WebControls.TableCell).Visible = False
            End If
            IndiceI += 1
        Loop
        IndiceI = 1
        Do
            Ctl = V_WebFonction.VirWebControle(Me.Ligne01, "CellDon", IndiceI)
            If Ctl Is Nothing Then
                Exit Do
            End If
            NumInfo = CInt(Strings.Right(Ctl.ID, 1))
            If NumInfo > Nombre_Colonnes Then
                CType(Ctl, System.Web.UI.WebControls.TableCell).Visible = False
            End If
            IndiceI += 1
        Loop

        CellCadrageFiltre.Visible = SiColonneCommande
        CellNeutre.Visible = SiColonneCommande
        CellCmd_L01.Visible = SiColonneCommande

        If Me.CadreGrille.Rows.Count = Nombre_Lignes_Page Then
            Exit Sub
        End If
        For IndiceI = 2 To Nombre_Lignes_Page
            Ligne = New System.Web.UI.WebControls.TableRow
            Cellule = New System.Web.UI.WebControls.TableCell
            Cellule.Controls.Add(CreerUneLigneDynamique(IndiceI))
            Ligne.Controls.Add(Cellule)
            CadreGrille.Controls.Add(Ligne)
        Next IndiceI

    End Sub

    Private Sub PreparerLignes()
        Dim IndiceI As Integer
        Dim Ctl As Control

        IndiceI = 0
        Do
            Ctl = V_WebFonction.VirWebControle(Me.CadreGrille, "Ligne", IndiceI)
            If Ctl Is Nothing Then
                Exit Do
            End If
            CType(Ctl, System.Web.UI.WebControls.Table).Visible = True
            IndiceI += 1
            SiCmdVisible(IndiceI) = SiColonneCommande
        Loop

        IndiceI = 0
        Do
            Ctl = V_WebFonction.VirWebControle(Me.CadreGrille, "CellDon", IndiceI)
            If Ctl Is Nothing Then
                Exit Do
            End If
            CType(Ctl, System.Web.UI.WebControls.TableCell).BackColor = Drawing.Color.White
            IndiceI += 1
        Loop

        IndiceI = 0
        Do
            Ctl = V_WebFonction.VirWebControle(Me.CadreGrille, "DonCol", IndiceI)
            If Ctl Is Nothing Then
                Exit Do
            End If
            CType(Ctl, System.Web.UI.WebControls.TextBox).Text = ""
            CType(Ctl, System.Web.UI.WebControls.TextBox).ForeColor = Drawing.Color.Black
            CType(Ctl, System.Web.UI.WebControls.TextBox).Font.Bold = False
            IndiceI += 1
        Loop

    End Sub

    Private Function CreerUneLigneDynamique(ByVal Index As Integer) As System.Web.UI.WebControls.Table
        Dim IndiceC As Integer
        Dim CadreDyna As System.Web.UI.WebControls.Table
        Dim Rangee As System.Web.UI.WebControls.TableRow
        Dim Colonne As System.Web.UI.WebControls.TableCell
        Dim DonneDyna As System.Web.UI.WebControls.TextBox
        Dim CmdDyna As System.Web.UI.WebControls.ImageButton

        CadreDyna = New System.Web.UI.WebControls.Table
        CadreDyna.ID = "Ligne" & Strings.Format(Index, "00")
        CadreDyna.HorizontalAlign = Ligne01.HorizontalAlign
        CadreDyna.CellSpacing = Ligne01.CellSpacing
        CadreDyna.CellPadding = Ligne01.CellPadding

        Rangee = New System.Web.UI.WebControls.TableRow
        Rangee.VerticalAlign = VerticalAlign.Top
        For IndiceC = 1 To Nombre_Colonnes
            Colonne = New System.Web.UI.WebControls.TableCell
            Colonne.ID = "CellDon_" & "L" & Strings.Format(Index, "00") & "_" & IndiceC
            Colonne.Width = Taille_Colonne(IndiceC)
            Colonne.CssClass = CellDon_L01_1.CssClass
            Colonne.Visible = True

            DonneDyna = New System.Web.UI.WebControls.TextBox
            DonneDyna.ID = "DonCol_" & "L" & Strings.Format(Index, "00") & "_" & IndiceC
            DonneDyna.Width = New Unit(Colonne.Width.Value - 10)
            DonneDyna.CssClass = DonCol_L01_1.CssClass
            DonneDyna.ReadOnly = SiReadOnly(IndiceC)
            DonneDyna.AutoPostBack = SiAutoPostBack(IndiceC)
            DonneDyna.TextMode = TextBoxMode.SingleLine
            If WsStyleDonnee(IndiceC - 1) IsNot Nothing Then
                Call Styler_Donnee(WsStyleDonnee(IndiceC - 1), DonneDyna)
            End If
            DonneDyna.Text = ""
            DonneDyna.Visible = True
            Colonne.Controls.Add(DonneDyna)
            Rangee.Controls.Add(Colonne)
        Next IndiceC

        If SiColonneCommande = True Then
            Colonne = New System.Web.UI.WebControls.TableCell
            Colonne.ID = "CellCmd_" & "L" & Strings.Format(Index, "00")
            Colonne.CssClass = CellCmd_L01.CssClass
            Colonne.Width = CellCmd_L01.Width
            Colonne.Visible = True

            CmdDyna = New System.Web.UI.WebControls.ImageButton
            CmdDyna.ID = "CmdCol_" & "L" & Strings.Format(Index, "00")
            CmdDyna.Height = CmdCol_L01.Height
            CmdDyna.Width = CmdCol_L01.Width
            CmdDyna.ImageUrl = CmdCol_L01.ImageUrl
            CmdDyna.ToolTip = CmdCol_L01.ToolTip
            AddHandler CmdDyna.Click, Sub(s, ev)
                                          CmdCol_Click(s, CType(ev, ImageClickEventArgs))
                                      End Sub
            CmdDyna.Visible = True
            Colonne.Controls.Add(CmdDyna)
            Rangee.Controls.Add(Colonne)
        End If

        CadreDyna.Controls.Add(Rangee)
        CadreDyna.Visible = True
        Return CadreDyna
    End Function

    Private Sub Styler_Donnee(ByVal Valeurs As String, ByVal DonCol As System.Web.UI.WebControls.TextBox)
        Dim TableauData(0) As String
        Dim TableauW(0) As String
        Dim IndiceI As Integer
        TableauData = Strings.Split(Valeurs, ";")
        For IndiceI = 0 To TableauData.Count - 1
            If TableauData(IndiceI) = "" Then
                Exit For
            End If
            TableauW = Strings.Split(TableauData(IndiceI), ":")
            DonCol.Style.Remove(Strings.Trim(TableauW(0)))
            DonCol.Style.Add(Strings.Trim(TableauW(0)), Strings.Trim(TableauW(1)))
        Next IndiceI
    End Sub

    Private ReadOnly Property V_WebFonction As Virtualia.Net.Controles.WebFonctions
        Get
            If WebFct Is Nothing Then
                WebFct = New Virtualia.Net.Controles.WebFonctions(Me, 0)
            End If
            Return WebFct
        End Get
    End Property

    Private Sub FaireListe()
        Dim LstValeurs As List(Of String)
        Dim LstLibels As List(Of String)
        Dim LstColonnes As List(Of String)
        Dim IndiceI As Integer
        Dim IndiceA As Integer
        Dim TableauCol(0) As String
        Dim Ligne As System.Web.UI.WebControls.Table
        Dim Ctl As Control
        Dim Cpt As Integer = 0
        Dim Total As Integer = 0
        Dim SiAfaire As Boolean
        Dim IndiceDebut As Integer
        Dim Clef As String

        Call PreparerLignes()
        WsCtl_Cache = CacheVirControle

        If SiFiltreVisible = True Then
            If WsCtl_Cache.Valeur_Filtre = "" Then
                WsCtl_Cache.Valeur_Filtre = "Toutes les valeurs"
                If WsCtl_Cache.Date_Debut_Filtre = "" Then
                    WsCtl_Cache.Date_Debut_Filtre = "01/01/" & Strings.Right(WebFct.ViRhDates.DateduJour, 4)
                    DDateDebut.DonText = WsCtl_Cache.Date_Debut_Filtre
                End If
                If WsCtl_Cache.Date_Fin_Filtre = "" Then
                    WsCtl_Cache.Date_Fin_Filtre = "31/12/" & Strings.Right(WebFct.ViRhDates.DateduJour, 4)
                    DDateFin.DonText = WsCtl_Cache.Date_Fin_Filtre
                End If
            End If
            If WsCtl_Cache.Date_Debut_Filtre = "" Then
                WsCtl_Cache.Date_Debut_Filtre = "01/01/1980"
            End If
            If WsCtl_Cache.Date_Fin_Filtre = "" Then
                WsCtl_Cache.Date_Fin_Filtre = "31/12/2050"
            End If
        End If

        LstColonnes = WsCtl_Cache.Libelles_Colonne
        If LstColonnes IsNot Nothing Then
            For IndiceI = 0 To LstColonnes.Count - 1
                If LstColonnes.Item(IndiceI) <> "Clef" Then
                    LibelleColonne(IndiceI + 1) = LstColonnes.Item(IndiceI)
                End If
            Next IndiceI
        End If

        IndiceDebut = (WsCtl_Cache.Numero_Page - 1) * Nombre_Lignes_Page

        LstValeurs = WsCtl_Cache.Liste_Valeurs
        If LstValeurs IsNot Nothing Then
            For IndiceI = 0 To LstValeurs.Count - 1
                TableauCol = Strings.Split(LstValeurs.Item(IndiceI), VI.Tild, -1)
                SiAfaire = False
                If SiFiltreVisible = False Then
                    Select Case IndiceI
                        Case Is >= IndiceDebut
                            SiAfaire = True
                    End Select
                Else
                    If WsCtl_Cache.Valeur_Filtre = "Toutes les valeurs" Then
                        SiAfaire = True
                    ElseIf TableauCol(WsCtl_Cache.Colonne_Filtre) = WsCtl_Cache.Valeur_Filtre Then
                        SiAfaire = True
                    End If
                    If SiAfaire = True Then
                        Select Case WebFct.ViRhDates.ComparerDates(TableauCol(0), WsCtl_Cache.Date_Debut_Filtre)
                            Case VI.ComparaisonDates.Egalite, VI.ComparaisonDates.PlusGrand
                                Select Case WebFct.ViRhDates.ComparerDates(TableauCol(0), WsCtl_Cache.Date_Fin_Filtre)
                                    Case VI.ComparaisonDates.Egalite, VI.ComparaisonDates.PlusPetit
                                        SiAfaire = True
                                    Case Else
                                        SiAfaire = False
                                End Select
                            Case Else
                                SiAfaire = False
                        End Select
                    End If
                    If SiAfaire = True Then
                        Select Case Total
                            Case Is < IndiceDebut
                                SiAfaire = False
                        End Select
                        Total += 1
                    End If
                End If
                If (SiAfaire = True) And (Cpt < Nombre_Lignes_Page) Then
                    Ctl = V_WebFonction.VirWebControle(Me.CadreGrille, "Ligne", Cpt)
                    If Ctl Is Nothing Then
                        Exit For
                    End If
                    Cpt += 1
                    Ligne = CType(Ctl, System.Web.UI.WebControls.Table)
                    Clef = TableauCol(TableauCol.Count - 1)
                    For IndiceA = 0 To TableauCol.Count - 2
                        Ctl = V_WebFonction.VirWebControle(Ligne, "CellDon", IndiceA)
                        If Ctl IsNot Nothing Then
                            If SiStyleAlterne = True Then
                                Select Case Cpt Mod 2
                                    Case 1
                                        CType(Ctl, System.Web.UI.WebControls.TableCell).BackColor = V_WebFonction.ConvertCouleur("#D7FAF3")
                                End Select
                            End If
                            If Clef = WsCtl_Cache.Clef_ItemSelectionne Then
                                CType(Ctl, System.Web.UI.WebControls.TableCell).BackColor = BackColorSelected
                            End If
                        End If
                        Ctl = V_WebFonction.VirWebControle(Ligne, "DonCol", IndiceA)
                        If Ctl IsNot Nothing Then
                            CType(Ctl, System.Web.UI.WebControls.TextBox).Text = TableauCol(IndiceA)
                            If Clef = WsCtl_Cache.Clef_ItemSelectionne Then
                                CType(Ctl, System.Web.UI.WebControls.TextBox).ForeColor = ForeColorSelected
                                CType(Ctl, System.Web.UI.WebControls.TextBox).Font.Bold = True
                            End If
                        End If
                    Next IndiceA
                End If
            Next IndiceI
        End If

        CellPagination.Visible = False
        If SiFiltreVisible = False Then
            Total = LstValeurs.Count
        End If
        If Total > Nombre_Lignes_Page Then
            CellPagination.Visible = True
            IndiceI = (Total \ Nombre_Lignes_Page) + 1
            If IndiceI <> WsCtl_Cache.Total_Pages Then
                WsCtl_Cache.Total_Pages = IndiceI
                CacheVirControle = WsCtl_Cache
            End If
        End If

        If Cpt < Nombre_Lignes_Page Then
            IndiceI = Cpt
            Do
                Ctl = V_WebFonction.VirWebControle(Me.CadreGrille, "Ligne", IndiceI)
                If Ctl Is Nothing Then
                    Exit Do
                End If
                CType(Ctl, System.Web.UI.WebControls.Table).Visible = False
                IndiceI += 1
            Loop
        End If
        LabelPage.Text = WsCtl_Cache.Numero_Page & " / " & WsCtl_Cache.Total_Pages

        LstLibels = WsCtl_Cache.Libelles_Caption
        Select Case Cpt
            Case 0
                EtiNbAffiches.Text = LstLibels.Item(0)
            Case 1
                EtiNbAffiches.Text = LstLibels.Item(1) & " sur " & Total
            Case Else
                EtiNbAffiches.Text = Cpt & Space(1) & LstLibels.Item(2) & " sur " & Total
        End Select
        DDateFin.DateDebut = WsCtl_Cache.Date_Debut_Filtre
    End Sub

    Private Sub CmdCol_Click(sender As Object, e As ImageClickEventArgs) Handles CmdCol_L01.Click
        Dim NumLigne As Integer

        NumLigne = CInt(Strings.Right(CType(sender, System.Web.UI.WebControls.ImageButton).ID, 2))
        V_Clef_ItemSelectionne = ClefSelectionnee(NumLigne)

        Dim Evenement As Virtualia.Systeme.Evenements.DonneeChangeEventArgs
        Evenement = New Virtualia.Systeme.Evenements.DonneeChangeEventArgs(V_Clef_ItemSelectionne)
        Saisie_Change(Evenement)
    End Sub

    Private ReadOnly Property ClefSelectionnee(ByVal NumLigne As Integer) As String
        Get
            Dim LstValeurs As List(Of String)
            Dim TableauCol(0) As String
            Dim SiAfaire As Boolean
            Dim IndexSel As Integer
            Dim Total As Integer

            WsCtl_Cache = CacheVirControle
            LstValeurs = WsCtl_Cache.Liste_Valeurs
            If LstValeurs Is Nothing Then
                Return ""
            End If
            IndexSel = ((WsCtl_Cache.Numero_Page - 1) * Nombre_Lignes_Page) + NumLigne
            If SiFiltreVisible = False Then
                TableauCol = Strings.Split(LstValeurs.Item(IndexSel), VI.Tild, -1)
                Return TableauCol(TableauCol.Count - 1)
            End If

            For IndiceI = 0 To LstValeurs.Count - 1
                TableauCol = Strings.Split(LstValeurs.Item(IndiceI), VI.Tild, -1)
                SiAfaire = False
                If WsCtl_Cache.Valeur_Filtre = "Toutes les valeurs" Then
                    SiAfaire = True
                ElseIf TableauCol(WsCtl_Cache.Colonne_Filtre) = WsCtl_Cache.Valeur_Filtre Then
                    SiAfaire = True
                End If
                If SiAfaire = True Then
                    Select Case WebFct.ViRhDates.ComparerDates(TableauCol(0), WsCtl_Cache.Date_Debut_Filtre)
                        Case VI.ComparaisonDates.Egalite, VI.ComparaisonDates.PlusGrand
                            Select Case WebFct.ViRhDates.ComparerDates(TableauCol(0), WsCtl_Cache.Date_Fin_Filtre)
                                Case VI.ComparaisonDates.Egalite, VI.ComparaisonDates.PlusPetit
                                    SiAfaire = True
                                Case Else
                                    SiAfaire = False
                            End Select
                        Case Else
                            SiAfaire = False
                    End Select
                End If
                If SiAfaire = True Then
                    Total += 1
                    If Total = IndexSel Then
                        Return TableauCol(TableauCol.Count - 1)
                    End If
                End If
            Next IndiceI
            Return ""
        End Get
    End Property
End Class