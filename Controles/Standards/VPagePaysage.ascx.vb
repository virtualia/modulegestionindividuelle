﻿Option Strict On
Option Explicit On
Option Compare Text
Public Class VPagePaysage
    Inherits System.Web.UI.UserControl
    Private WsNomState As String = "PagePdf"
    Private WebFct As Virtualia.Net.Controles.WebFonctions
    Private WsNoPage As Integer = 0
    Private WsNbPages As Integer = 0

    Public Property NumerodePage As Integer
        Get
            Return WsNoPage
        End Get
        Set(value As Integer)
            WsNoPage = value
            If value = 0 Then
                EtiPagination.Text = ""
                Exit Property
            End If
            EtiPagination.Text = " Page : " & value.ToString
        End Set
    End Property

    Public Property NombredePages As Integer
        Get
            Return WsNbPages
        End Get
        Set(value As Integer)
            WsNbPages = value
            If value = 0 Then
                EtiNbPages.Text = ""
                Exit Property
            End If
            EtiNbPages.Text = " / " & value.ToString
        End Set
    End Property

    Public Property SiVisible(ByVal Index As Integer) As Boolean
        Get
            Dim Ctl As Control
            Dim VirControle As System.Web.UI.WebControls.Label
            Dim NumInfo As Integer
            Dim IndiceI As Integer = 0

            Do
                Ctl = WebFct.VirWebControle(Me.PagePaysage, "Ligne", IndiceI)
                If Ctl Is Nothing Then
                    Exit Do
                End If
                NumInfo = CInt(Strings.Right(Ctl.ID, 2))
                If NumInfo = Index Then
                    VirControle = CType(Ctl, System.Web.UI.WebControls.Label)
                    Return VirControle.Visible
                End If
                IndiceI += 1
            Loop
            Return False
        End Get
        Set(ByVal value As Boolean)
            Dim Ctl As Control
            Dim VirControle As System.Web.UI.WebControls.Label
            Dim NumInfo As Integer
            Dim IndiceI As Integer = 0

            Do
                Ctl = WebFct.VirWebControle(Me.PagePaysage, "Ligne", IndiceI)
                If Ctl Is Nothing Then
                    Exit Do
                End If
                NumInfo = CInt(Strings.Right(Ctl.ID, 2))
                If NumInfo = Index Then
                    VirControle = CType(Ctl, System.Web.UI.WebControls.Label)
                    VirControle.Visible = value
                    Exit Do
                End If
                IndiceI += 1
            Loop
        End Set
    End Property

    Public Property SiGras(ByVal Index As Integer) As Boolean
        Get
            Dim Ctl As Control
            Dim VirControle As System.Web.UI.WebControls.Label
            Dim NumInfo As Integer
            Dim IndiceI As Integer = 0

            Do
                Ctl = WebFct.VirWebControle(Me.PagePaysage, "Ligne", IndiceI)
                If Ctl Is Nothing Then
                    Exit Do
                End If
                NumInfo = CInt(Strings.Right(Ctl.ID, 2))
                If NumInfo = Index Then
                    VirControle = CType(Ctl, System.Web.UI.WebControls.Label)
                    Return VirControle.Font.Bold
                End If
                IndiceI += 1
            Loop
            Return False
        End Get
        Set(ByVal value As Boolean)
            Dim Ctl As Control
            Dim VirControle As System.Web.UI.WebControls.Label
            Dim NumInfo As Integer
            Dim IndiceI As Integer = 0

            Do
                Ctl = WebFct.VirWebControle(Me.PagePaysage, "Ligne", IndiceI)
                If Ctl Is Nothing Then
                    Exit Do
                End If
                NumInfo = CInt(Strings.Right(Ctl.ID, 2))
                If NumInfo = Index Then
                    VirControle = CType(Ctl, System.Web.UI.WebControls.Label)
                    VirControle.Font.Bold = value
                    Exit Do
                End If
                IndiceI += 1
            Loop
        End Set
    End Property

    Public WriteOnly Property LigneStyle(ByVal Index As Integer) As String
        Set(ByVal value As String)
            Dim TableauData(0) As String
            Dim TableauW(0) As String
            Dim IndiceI As Integer = 0
            Dim IndiceK As Integer = 0
            Dim Ctl As Control
            Dim VirControle As System.Web.UI.WebControls.Label
            Dim NumInfo As Integer

            TableauData = Strings.Split(value, ";")
            Do
                Ctl = WebFct.VirWebControle(Me.PagePaysage, "Ligne", IndiceI)
                If Ctl Is Nothing Then
                    Exit Do
                End If
                NumInfo = CInt(Strings.Right(Ctl.ID, 2))
                If NumInfo = Index Then
                    VirControle = CType(Ctl, System.Web.UI.WebControls.Label)
                    For IndiceK = 0 To TableauData.Count - 1
                        If TableauData(IndiceK) = "" Then
                            Exit For
                        End If
                        TableauW = Strings.Split(TableauData(IndiceK), ":")
                        VirControle.Style.Remove(Strings.Trim(TableauW(0)))
                        VirControle.Style.Add(Strings.Trim(TableauW(0)), Strings.Trim(TableauW(1)))
                    Next IndiceK
                    Exit Do
                End If
                IndiceI += 1
            Loop
        End Set
    End Property

    Public Property Textes As List(Of String)
        Get
            If Me.ViewState(WsNomState) Is Nothing Then
                Return Nothing
            End If
            Dim CacheListe As ArrayList = CType(Me.ViewState(WsNomState), ArrayList)
            Dim IndiceI As Integer
            Dim LstPage As New List(Of String)
            For IndiceI = 0 To CacheListe.Count - 1
                LstPage.Add(CacheListe.Item(IndiceI).ToString)
            Next IndiceI
            Return LstPage
        End Get
        Set(value As List(Of String))
            If value Is Nothing Then
                Exit Property
            End If
            Dim CacheListe As ArrayList
            Dim IndiceI As Integer
            If Me.ViewState(WsNomState) IsNot Nothing Then
                Me.ViewState.Remove(WsNomState)
            End If
            CacheListe = New ArrayList
            For IndiceI = 0 To value.Count - 1
                CacheListe.Add(value.Item(IndiceI))
            Next IndiceI
            Me.ViewState.Add(WsNomState, CacheListe)
        End Set
    End Property

    Public Property Texte(ByVal Index As Integer) As String
        Get
            Dim Ctl As Control
            Dim VirControle As System.Web.UI.WebControls.Label
            Dim NumInfo As Integer
            Dim IndiceI As Integer = 0

            Do
                Ctl = WebFct.VirWebControle(Me.PagePaysage, "Ligne", IndiceI)
                If Ctl Is Nothing Then
                    Exit Do
                End If
                NumInfo = CInt(Strings.Right(Ctl.ID, 2))
                If NumInfo = Index Then
                    VirControle = CType(Ctl, System.Web.UI.WebControls.Label)
                    Return VirControle.Text
                End If
                IndiceI += 1
            Loop
            Return ""
        End Get
        Set(ByVal value As String)
            Dim Ctl As Control
            Dim VirControle As System.Web.UI.WebControls.Label
            Dim NumInfo As Integer
            Dim IndiceI As Integer = 0

            Do
                Ctl = WebFct.VirWebControle(Me.PagePaysage, "Ligne", IndiceI)
                If Ctl Is Nothing Then
                    Exit Do
                End If
                NumInfo = CInt(Strings.Right(Ctl.ID, 2))
                If NumInfo = Index Then
                    VirControle = CType(Ctl, System.Web.UI.WebControls.Label)
                    VirControle.Text = value
                    Exit Do
                End If
                IndiceI += 1
            Loop
        End Set
    End Property

    Private Sub Page_Init(sender As Object, e As System.EventArgs) Handles Me.Init
        WebFct = New Virtualia.Net.Controles.WebFonctions(Me, 0)
    End Sub

    Private Sub Page_PreRender(sender As Object, e As System.EventArgs) Handles Me.PreRender
        Dim IndiceI As Integer
        Dim LstDatas As List(Of String) = Textes
        If LstDatas IsNot Nothing Then
            For IndiceI = 0 To LstDatas.Count - 1
                Texte(IndiceI + 1) = LstDatas(IndiceI)
            Next IndiceI
        End If
        EtiDateJour.Text = "Le " & WebFct.ViRhDates.ClairDate(WebFct.ViRhDates.DateduJour, True) & Strings.Space(2) & Format(TimeOfDay, "t")
    End Sub
End Class