﻿Option Strict On
Option Explicit On
Option Compare Text
Imports VI = Virtualia.Systeme.Constantes
Public Class VEtatPaysage
    Inherits System.Web.UI.UserControl
    Private WebFct As Virtualia.Net.Controles.WebFonctions

    Public Property SiVisible(ByVal Index As Integer) As Boolean
        Get
            Dim Ctl As Control
            Dim VirControle As Virtualia.Net.VPagePaysage
            Dim NumInfo As Integer
            Dim IndiceI As Integer = 0

            Do
                Ctl = WebFct.VirWebControle(Me.EtatPaysage, "Page", IndiceI)
                If Ctl Is Nothing Then
                    Exit Do
                End If
                If Ctl.ID.Length = 7 Then
                    NumInfo = CInt(Strings.Right(Ctl.ID, 3))
                Else
                    NumInfo = CInt(Strings.Right(Ctl.ID, 2))
                End If
                If NumInfo = Index Then
                    VirControle = CType(Ctl, Virtualia.Net.VPagePaysage)
                    Return VirControle.Visible
                End If
                IndiceI += 1
            Loop
            Return False
        End Get
        Set(ByVal value As Boolean)
            Dim Ctl As Control
            Dim VirControle As Virtualia.Net.VPagePaysage
            Dim NumInfo As Integer
            Dim IndiceI As Integer = 0

            Do
                Ctl = WebFct.VirWebControle(Me.EtatPaysage, "Page", IndiceI)
                If Ctl Is Nothing Then
                    Exit Do
                End If
                If Ctl.ID.Length = 7 Then
                    NumInfo = CInt(Strings.Right(Ctl.ID, 3))
                Else
                    NumInfo = CInt(Strings.Right(Ctl.ID, 2))
                End If
                If NumInfo = Index Then
                    VirControle = CType(Ctl, Virtualia.Net.VPagePaysage)
                    VirControle.Visible = value
                    Exit Do
                End If
                IndiceI += 1
            Loop
        End Set
    End Property

    Public Property SiVisible(ByVal NoPage As Integer, ByVal NoLigne As Integer) As Boolean
        Get
            Dim Ctl As Control
            Dim VirControle As Virtualia.Net.VPagePaysage
            Dim NumInfo As Integer
            Dim IndiceI As Integer = 0

            Do
                Ctl = WebFct.VirWebControle(Me.EtatPaysage, "Page", IndiceI)
                If Ctl Is Nothing Then
                    Exit Do
                End If
                If Ctl.ID.Length = 7 Then
                    NumInfo = CInt(Strings.Right(Ctl.ID, 3))
                Else
                    NumInfo = CInt(Strings.Right(Ctl.ID, 2))
                End If
                If NumInfo = NoPage Then
                    VirControle = CType(Ctl, Virtualia.Net.VPagePaysage)
                    Return VirControle.SiVisible(NoLigne)
                End If
                IndiceI += 1
            Loop
            Return False
        End Get
        Set(ByVal value As Boolean)
            Dim Ctl As Control
            Dim VirControle As Virtualia.Net.VPagePaysage
            Dim NumInfo As Integer
            Dim IndiceI As Integer = 0

            Do
                Ctl = WebFct.VirWebControle(Me.EtatPaysage, "Page", IndiceI)
                If Ctl Is Nothing Then
                    Exit Do
                End If
                If Ctl.ID.Length = 7 Then
                    NumInfo = CInt(Strings.Right(Ctl.ID, 3))
                Else
                    NumInfo = CInt(Strings.Right(Ctl.ID, 2))
                End If
                If NumInfo = NoPage Then
                    VirControle = CType(Ctl, Virtualia.Net.VPagePaysage)
                    VirControle.SiVisible(NoLigne) = value
                    Exit Do
                End If
                IndiceI += 1
            Loop
        End Set
    End Property

    Public Property SiGras(ByVal NoPage As Integer, ByVal NoLigne As Integer) As Boolean
        Get
            Dim Ctl As Control
            Dim VirControle As Virtualia.Net.VPagePaysage
            Dim NumInfo As Integer
            Dim IndiceI As Integer = 0

            Do
                Ctl = WebFct.VirWebControle(Me.EtatPaysage, "Page", IndiceI)
                If Ctl Is Nothing Then
                    Exit Do
                End If
                If Ctl.ID.Length = 7 Then
                    NumInfo = CInt(Strings.Right(Ctl.ID, 3))
                Else
                    NumInfo = CInt(Strings.Right(Ctl.ID, 2))
                End If
                If NumInfo = NoPage Then
                    VirControle = CType(Ctl, Virtualia.Net.VPagePaysage)
                    Return VirControle.SiGras(NoLigne)
                End If
                IndiceI += 1
            Loop
            Return False
        End Get
        Set(ByVal value As Boolean)
            Dim Ctl As Control
            Dim VirControle As Virtualia.Net.VPagePaysage
            Dim NumInfo As Integer
            Dim IndiceI As Integer = 0

            Do
                Ctl = WebFct.VirWebControle(Me.EtatPaysage, "Page", IndiceI)
                If Ctl Is Nothing Then
                    Exit Do
                End If
                If Ctl.ID.Length = 7 Then
                    NumInfo = CInt(Strings.Right(Ctl.ID, 3))
                Else
                    NumInfo = CInt(Strings.Right(Ctl.ID, 2))
                End If
                If NumInfo = NoPage Then
                    VirControle = CType(Ctl, Virtualia.Net.VPagePaysage)
                    VirControle.SiGras(NoLigne) = value
                    Exit Do
                End If
                IndiceI += 1
            Loop
        End Set
    End Property

    Public WriteOnly Property StyledeLigne(ByVal NoPage As Integer, ByVal NoLigne As Integer) As String
        Set(ByVal value As String)
            Dim Ctl As Control
            Dim VirControle As Virtualia.Net.VPagePaysage
            Dim NumInfo As Integer
            Dim IndiceI As Integer = 0

            Do
                Ctl = WebFct.VirWebControle(Me.EtatPaysage, "Page", IndiceI)
                If Ctl Is Nothing Then
                    Exit Do
                End If
                If Ctl.ID.Length = 7 Then
                    NumInfo = CInt(Strings.Right(Ctl.ID, 3))
                Else
                    NumInfo = CInt(Strings.Right(Ctl.ID, 2))
                End If
                If NumInfo = NoPage Then
                    VirControle = CType(Ctl, Virtualia.Net.VPagePaysage)
                    VirControle.LigneStyle(NoLigne) = value
                    Exit Do
                End If
                IndiceI += 1
            Loop
        End Set
    End Property

    Public Property NombreMaxiDePages(ByVal NoPage As Integer) As Integer
        Get
            Dim Ctl As Control
            Dim VirControle As Virtualia.Net.VPagePaysage
            Dim NumInfo As Integer
            Dim IndiceI As Integer = 0

            Do
                Ctl = WebFct.VirWebControle(Me.EtatPaysage, "Page", IndiceI)
                If Ctl Is Nothing Then
                    Exit Do
                End If
                If Ctl.ID.Length = 7 Then
                    NumInfo = CInt(Strings.Right(Ctl.ID, 3))
                Else
                    NumInfo = CInt(Strings.Right(Ctl.ID, 2))
                End If
                If NumInfo = NoPage Then
                    VirControle = CType(Ctl, Virtualia.Net.VPagePaysage)
                    Return VirControle.NombredePages
                End If
                IndiceI += 1
            Loop
            Return 0
        End Get
        Set(ByVal value As Integer)
            Dim Ctl As Control
            Dim VirControle As Virtualia.Net.VPagePaysage
            Dim NumInfo As Integer
            Dim IndiceI As Integer = 0

            Do
                Ctl = WebFct.VirWebControle(Me.EtatPaysage, "Page", IndiceI)
                If Ctl Is Nothing Then
                    Exit Do
                End If
                If Ctl.ID.Length = 7 Then
                    NumInfo = CInt(Strings.Right(Ctl.ID, 3))
                Else
                    NumInfo = CInt(Strings.Right(Ctl.ID, 2))
                End If
                If NumInfo = NoPage Then
                    VirControle = CType(Ctl, Virtualia.Net.VPagePaysage)
                    VirControle.NombredePages = value
                    Exit Do
                End If
                IndiceI += 1
            Loop
        End Set
    End Property

    Public Property Contenu(ByVal NoPage As Integer) As List(Of String)
        Get
            Dim Ctl As Control
            Dim VirControle As Virtualia.Net.VPagePaysage
            Dim NumInfo As Integer
            Dim IndiceI As Integer = 0

            Do
                Ctl = WebFct.VirWebControle(Me.EtatPaysage, "Page", IndiceI)
                If Ctl Is Nothing Then
                    Exit Do
                End If
                If Ctl.ID.Length = 7 Then
                    NumInfo = CInt(Strings.Right(Ctl.ID, 3))
                Else
                    NumInfo = CInt(Strings.Right(Ctl.ID, 2))
                End If
                If NumInfo = NoPage Then
                    VirControle = CType(Ctl, Virtualia.Net.VPagePaysage)
                    Return VirControle.Textes
                End If
                IndiceI += 1
            Loop
            Return Nothing
        End Get
        Set(value As List(Of String))
            If value Is Nothing Then
                Exit Property
            End If
            Dim Ctl As Control
            Dim VirControle As Virtualia.Net.VPagePaysage
            Dim NumInfo As Integer
            Dim IndiceI As Integer = 0

            Do
                Ctl = WebFct.VirWebControle(Me.EtatPaysage, "Page", IndiceI)
                If Ctl Is Nothing Then
                    Exit Do
                End If
                If Ctl.ID.Length = 7 Then
                    NumInfo = CInt(Strings.Right(Ctl.ID, 3))
                Else
                    NumInfo = CInt(Strings.Right(Ctl.ID, 2))
                End If
                If NumInfo = NoPage Then
                    VirControle = CType(Ctl, Virtualia.Net.VPagePaysage)
                    VirControle.Textes = value
                    Exit Do
                End If
                IndiceI += 1
            Loop
        End Set
    End Property

    Private Sub Page_Init(sender As Object, e As System.EventArgs) Handles Me.Init
        WebFct = New Virtualia.Net.Controles.WebFonctions(Me, 0)
    End Sub
End Class