﻿<%@ Control Language="VB" AutoEventWireup="false" Inherits="Virtualia.Net.Fenetre_PER_INDEMNITE" Codebehind="PER_INDEMNITE.ascx.vb" %>

<%@ Register src="~/Controles/Saisies/VCoupleEtiDate.ascx" tagname="VCoupleEtiDate" tagprefix="Virtualia" %>
<%@ Register src="~/Controles/Saisies/VCoupleEtiDonnee.ascx" tagname="VCoupleEtiDonnee" tagprefix="Virtualia" %>
<%@ Register src="~/Controles/Saisies/VDuoEtiquetteCommande.ascx" tagname="VDuoEtiquetteCommande" tagprefix="Virtualia" %>
<%@ Register src="~/Controles/Standards/VListeGrid.ascx" tagname="VListeGrid" tagprefix="Virtualia" %>
<%@ Register src="~/Controles/Saisies/VCocheSimple.ascx" tagname="VCocheSimple" tagprefix="Virtualia" %>
<%@ Register src="~/Controles/Saisies/VCoupleVerticalEtiDonnee.ascx" tagname="VCoupleVerticalEtiDonnee" tagprefix="Virtualia" %>

 <asp:Table ID="CadreControle" runat="server" Height="30px" Width="585px" CellPadding="0" CellSpacing="0" HorizontalAlign="Center">
   <asp:TableRow> 
     <asp:TableCell> 
       <Virtualia:VListeGrid ID="ListeGrille" runat="server" CadreWidth="585px" SiColonneSelect="true"
                                        SiCaseAcocher="false"/>
     </asp:TableCell>
   </asp:TableRow>
   <asp:TableRow>
     <asp:TableCell>
       <asp:Table ID="CadreInfo" runat="server" BorderStyle="Ridge" BorderWidth="2px"
        BorderColor="#B0E0D7" Height="520px" Width="615px" HorizontalAlign="Center" style="margin-top: 3px;">
        <asp:TableRow>
          <asp:TableCell>
            <asp:Table ID="CadreCommandes" runat="server" Height="22px" CellPadding="0" CellSpacing="0"
                Width="244px" HorizontalAlign="Right" style="margin-top: 3px;margin-right: 3px">
                <asp:TableRow>
                    <asp:TableCell VerticalAlign="Bottom">
                        <asp:Table ID="CadreCmdNewSupp" runat="server" BackImageUrl="~/Images/Boutons/NewSupp_Std.bmp" 
                            Width="160px" CellPadding="0" CellSpacing="0"
                            BorderWidth="2px" BorderStyle="Outset" BorderColor="#B0E0D7" ForeColor="#D7FAF3">
                            <asp:TableRow>
                                <asp:TableCell>
                                    <asp:Button ID="CommandeNew" runat="server" Text="Nouveau" Width="68px" Height="20px"
                                        BackColor="Transparent" BorderColor="#B0E0D7" ForeColor="#D7FAF3"
                                        Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small" Font-Italic="true"
                                        BorderStyle="None" style=" margin-left: 10px; text-align: right;"
                                        Tooltip="Nouvelle fiche">
                                    </asp:Button>
                                </asp:TableCell>
                                <asp:TableCell>
                                    <asp:Button ID="CommandeSupp" runat="server" Text="Supprimer" Width="68px" Height="20px"
                                        BackColor="Transparent" BorderColor="#B0E0D7" ForeColor="#D7FAF3"
                                        Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small" Font-Italic="true"
                                        BorderStyle="None" style=" margin-left: 10px; text-align: right;"
                                        Tooltip="Supprimer la fiche">
                                    </asp:Button>
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>    
                    </asp:TableCell>
                    <asp:TableCell VerticalAlign="Bottom">
                        <asp:Table ID="CadreCmdOK" runat="server" BackImageUrl="~/Images/Boutons/OK_Std.bmp" Width="70px"
                            BorderWidth="2px" BorderStyle="Outset" BorderColor="#B0E0D7" ForeColor="#D7FAF3"
                            CellPadding="0" CellSpacing="0" Visible="false">
                            <asp:TableRow>
                                <asp:TableCell>
                                    <asp:Button ID="CommandeOK" runat="server" Text="Valider" Width="65px" Height="20px"
                                        BackColor="Transparent" BorderColor="#B0E0D7" ForeColor="#D7FAF3" 
                                        Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small" Font-Italic="true"
                                        BorderStyle="None" style=" margin-left: 6px; text-align: right;">
                                        </asp:Button>
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>  
                    </asp:TableCell>
                </asp:TableRow>
              </asp:Table>
          </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow>
          <asp:TableCell>
              <asp:Table ID="CadreTitre" runat="server" Height="40px" CellPadding="0" Width="585px" 
                CellSpacing="0" HorizontalAlign="Center">
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="Etiquette" runat="server" Text="Régime indemnitaire" Height="20px" Width="300px"
                            BackColor="#2FA49B" BorderColor="#B0E0D7" BorderStyle="Groove"
                            BorderWidth="2px" ForeColor="#D7FAF3"
                            Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="Small"
                            style="margin-top: 5px; margin-left: 4px; margin-bottom: 20px;
                            font-style: oblique; text-indent: 5px; text-align: center;">
                        </asp:Label>          
                    </asp:TableCell>      
                </asp:TableRow>
              </asp:Table>
          </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow>
          <asp:TableCell>
              <asp:Table ID="CadreDates" runat="server" CellPadding="0" CellSpacing="0" Width="585px">
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <Virtualia:VCoupleEtiDate ID="InfoD00" runat="server" TypeCalendrier="Standard" EtiWidth="150px"
                                        V_SiDonneeDico="true" V_PointdeVue="1" V_Objet="21" V_Information="0" DonTabIndex="1"/>
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Left" >
                        <Virtualia:VCoupleEtiDate ID="InfoD06" runat="server" TypeCalendrier="Standard" EtiWidth="150px" SiDateFin="true"
                                        V_SiDonneeDico="true" V_PointdeVue="1" V_Objet="21" V_Information="6" DonTabIndex="2"/>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="20px" ColumnSpan="2"></asp:TableCell>
                </asp:TableRow>
              </asp:Table>
          </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow>
          <asp:TableCell>
              <asp:Table ID="CadreDon" runat="server" CellPadding="0" CellSpacing="0" Width="585px">
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <Virtualia:VDuoEtiquetteCommande ID="Dontab01" runat="server"
                           V_PointdeVue="1" V_Objet="21" V_Information="1" V_SiDonneeDico="true"
                           DonTabIndex="3" EtiWidth="100px" EtiText="Indemnité" DonWidth="468px"/>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="3px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Label ID="LabelModeCalcul" runat="server" Height="40px" Width="570px"
                           Text="Mode de calcul" Style="margin-left:5px; text-align:center;"
                           BackColor="#B0E0D7" BorderColor="#B0E0D7"  BorderStyle="Outset"
                           BorderWidth="2px" ForeColor="#142425" Font-Italic="true"
                           Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small">
                        </asp:Label>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="7px"></asp:TableCell>
                </asp:TableRow>
              </asp:Table>
          </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow>
          <asp:TableCell>
              <asp:Table ID="TableauMontant" runat="server" CellPadding="0" CellSpacing="0" Width="585px">
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <Virtualia:VCoupleEtiDonnee ID="InfoH02" runat="server"
                           V_PointdeVue="1" V_Objet="21" V_Information="2" V_SiDonneeDico="true"
                           EtiWidth="150px" DonWidth="80px" DonTabIndex="4"/>
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Left" Width="335px">  
                        <asp:Label ID="LabelUnite" runat="server" Text="€" Height="20px" Width="25px"
                           BackColor="Transparent" BorderStyle="none"  BorderWidth="0px"
                           ForeColor="#2FA49B" Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                           style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                           font-style: oblique; text-indent: 0px; text-align: left;">
                        </asp:Label> 
                    </asp:TableCell>
                </asp:TableRow>
              </asp:Table>
          </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow>
          <asp:TableCell>
              <asp:Table ID="TableauDonIndemnite" runat="server" CellPadding="0" CellSpacing="0" Width="585px">
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <Virtualia:VDuoEtiquetteCommande ID="Dontab05" runat="server"
                           V_PointdeVue="1" V_Objet="21" V_Information="5" V_SiDonneeDico="true"
                           DonTabIndex="5" EtiWidth="100px" DonWidth="150px"/>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="10px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <Virtualia:VDuoEtiquetteCommande ID="Dontab07" runat="server"
                           V_PointdeVue="1" V_Objet="21" V_Information="7" V_SiDonneeDico="true"
                           DonTabIndex="6" EtiWidth="100px" DonWidth="160px"/> 
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <Virtualia:VCocheSimple ID="Coche08" runat="server"
                           V_PointdeVue="1" V_Objet="21" V_Information="8" V_SiDonneeDico="true"
                           V_Width="200px" V_Style="margin-left:4px"/> 
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="15px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <Virtualia:VCoupleVerticalEtiDonnee ID="InfoV03" runat="server" DonTextMode="true"
                           V_PointdeVue="1" V_Objet="21" V_Information="3" V_SiDonneeDico="true"
                           EtiWidth="570px" DonWidth="568px" DonHeight="120px" DonTabIndex="8"
                           EtiStyle="text-align:center; margin-left: 5px;" Donstyle="margin-left: 5px;"/>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="12px"></asp:TableCell>
                </asp:TableRow>
             </asp:Table>
          </asp:TableCell>
        </asp:TableRow>     
      </asp:Table>
     </asp:TableCell>
   </asp:TableRow>
 </asp:Table>