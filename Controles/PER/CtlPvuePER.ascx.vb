﻿Option Strict On
Option Explicit On
Option Compare Text
Imports VI = Virtualia.Systeme.Constantes
Public Class CtlPvuePER
    Inherits System.Web.UI.UserControl
    Private WebFct As Virtualia.Net.Controles.WebFonctions
    Private WsNomStateIde As String = "MenuPERIde"

    Public Property V_Identifiant As Integer
        Get
            If Me.ViewState(WsNomStateIde) Is Nothing Then
                Return 0
            End If
            Return CType(Me.ViewState(WsNomStateIde), Integer)
        End Get
        Set(ByVal value As Integer)
            If value = 0 Then
                Exit Property
            End If
            If Me.ViewState(WsNomStateIde) IsNot Nothing Then
                If CType(Me.ViewState(WsNomStateIde), Integer) = value Then
                    Exit Property
                End If
                Me.ViewState.Remove(WsNomStateIde)
            End If
            Me.ViewState.Add(WsNomStateIde, value)
        End Set
    End Property

    Private Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        WebFct = New Virtualia.Net.Controles.WebFonctions(Me, 0)
    End Sub

    Private Sub Page_PreRender(sender As Object, e As EventArgs) Handles Me.PreRender
        If HPopupRef.Value = "1" Then
            CellReference.Visible = True
            PopupReferentiel.Show()
        ElseIf HPopupRefGrade.Value = "1" Then
            CellRefGrade.Visible = True
            PopupRefGrade.Show()
        Else
            CellReference.Visible = False
            Call AfficherFenetrePER()
        End If
    End Sub

    Private Sub AfficherFenetrePER()
        Dim Ide_Dossier As Integer = V_Identifiant
        If Ide_Dossier = 0 Then
            Exit Sub
        End If
        Select Case MultiPER.ActiveViewIndex
            Case 0
                PER_ETATCIVIL_1.Identifiant = Ide_Dossier
            Case 1
                PER_DOMICILE_4.Identifiant = Ide_Dossier
            Case 2
                PER_BANQUE_5.Identifiant = Ide_Dossier
            Case 3
                PER_ENFANT_3.Identifiant = Ide_Dossier
            Case 4
                PER_COLLECTIVITE_26.Identifiant = Ide_Dossier
            Case 5
                PER_ADMORIGINE_25.Identifiant = Ide_Dossier
            Case 6
                PER_LOLF_92.Identifiant = Ide_Dossier
            Case 7
                PER_AFFECTATION_17.Identifiant = Ide_Dossier
            Case 8
                PER_ADRESSEPRO_24.Identifiant = Ide_Dossier
            Case 9
                PER_AFFILIATION_27.Identifiant = Ide_Dossier
            Case 10
                PER_MUTUELLE_59.Identifiant = Ide_Dossier
            Case 11
                PER_ABSENCE_15.Identifiant = Ide_Dossier
            Case 12
                PER_STATUT_12.Identifiant = Ide_Dossier
            Case 13
                PER_POSITION_13.Identifiant = Ide_Dossier
            Case 14
                PER_GRADE_14.Identifiant = Ide_Dossier
            Case 15
                PER_STATUT_54.Identifiant = Ide_Dossier
            Case 16
                PER_POSITION_55.Identifiant = Ide_Dossier
            Case 17
                PER_GRADE_56.Identifiant = Ide_Dossier
            Case 18
                PER_INDEMNITE_21.Identifiant = Ide_Dossier
            Case 19
                PER_VARIABLE_PAIE_67.Identifiant = Ide_Dossier
            Case 20
                PER_HEURESUP_61.Identifiant = Ide_Dossier
            Case 21
                PER_SIFAC_180.Identifiant = Ide_Dossier
                MultiPER.SetActiveView(VueSifac)
        End Select
    End Sub

    Private Sub PER_MENU_Menu_Click(ByVal sender As Object, ByVal e As Systeme.Evenements.DonneeChangeEventArgs) Handles PER_MENU.Menu_Click
        If IsNumeric(e.Parametre) Then
            MultiPER.ActiveViewIndex = CInt(e.Parametre)
        End If
    End Sub

    Protected Sub Referentiel_RetourEventHandler(ByVal sender As Object, ByVal e As System.EventArgs) Handles RefVirtualia.RetourEventHandler, RefGradeGrilles.RetourEventHandler
        HPopupRef.Value = "0"
        CellReference.Visible = False
        Select Case WebFct.PointeurContexte.SysRef_IDVueRetour
            Case "VueEtatcivil"
                MultiPER.SetActiveView(VueEtatCivil)
            Case "VueAdresse"
                MultiPER.SetActiveView(VueAdresse)
            Case "VueBanque"
                MultiPER.SetActiveView(VueBanque)
            Case "VueAdministration"
                MultiPER.SetActiveView(VueAdministration)
            Case "VueAdmOrigine"
                MultiPER.SetActiveView(VueAdmOrigine)
            Case "VueLOLF"
                MultiPER.SetActiveView(VueLOLF)
            Case "VueAffectation"
                MultiPER.SetActiveView(VueAffectation)
            Case "VueAdressePro"
                MultiPER.SetActiveView(VueAdressePro)
            Case "VueCaisse"
                MultiPER.SetActiveView(VueCaisse)
            Case "VueMutuelle"
                MultiPER.SetActiveView(VueMutuelle)
            Case "VueAbsence"
                MultiPER.SetActiveView(VueAbsence)
            Case "VueAbsence"
                MultiPER.SetActiveView(VueAbsence)
            Case "VueStatut"
                MultiPER.SetActiveView(VueStatut)
            Case "VueStatutDetache"
                MultiPER.SetActiveView(VueStatutDetache)
            Case "VuePosition"
                MultiPER.SetActiveView(VuePosition)
            Case "VuePositionDetache"
                MultiPER.SetActiveView(VuePositionDetache)
            Case "VueGrade"
                HPopupRefGrade.Value = "0"
                CellRefGrade.Visible = False
                MultiPER.SetActiveView(VueGrade)
            Case "VueGradeDetache"
                HPopupRefGrade.Value = "0"
                CellRefGrade.Visible = False
                MultiPER.SetActiveView(VueGradeDetache)
            Case "VuePrime"
                MultiPER.SetActiveView(VuePrime)
            Case "VueHeureSup"
                MultiPER.SetActiveView(VueHeureSup)
            Case "VueVariablePaie"
                MultiPER.SetActiveView(VueVariablePaie)
            Case "VueSifac"
                MultiPER.SetActiveView(VueSifac)
        End Select
        WebFct.PointeurContexte.Fenetre_VueActive(VI.CategorieRH.InfosPersonnelles) = MultiPER.ActiveViewIndex
    End Sub

    Protected Sub Referentiel_ValeurSelectionnee(ByVal sender As Object, ByVal e As Virtualia.Systeme.Evenements.ValeurSelectionneeEventArgs) _
        Handles RefVirtualia.ValeurSelectionnee, RefGradeGrilles.ValeurSelectionnee
        HPopupRef.Value = "0"
        CellReference.Visible = False
        Select Case WebFct.PointeurContexte.SysRef_IDVueRetour
            Case "VueEtatcivil"
                PER_ETATCIVIL_1.Dontab_RetourAppelTable(e.ControleAppelant) = e.Valeur
                MultiPER.SetActiveView(VueEtatCivil)
            Case "VueAdresse"
                PER_DOMICILE_4.Dontab_RetourAppelTable(e.ControleAppelant) = e.Valeur
                MultiPER.SetActiveView(VueAdresse)
            Case "VueBanque"
                PER_BANQUE_5.Dontab_RetourAppelTable(e.ControleAppelant) = e.Valeur
                MultiPER.SetActiveView(VueBanque)
            Case "VueAdministration"
                PER_COLLECTIVITE_26.Dontab_RetourAppelTable(e.ControleAppelant) = e.Valeur
                MultiPER.SetActiveView(VueAdministration)
            Case "VueAdmOrigine"
                PER_ADMORIGINE_25.Dontab_RetourAppelTable(e.ControleAppelant) = e.Valeur
                MultiPER.SetActiveView(VueAdmOrigine)
            Case "VueLOLF"
                PER_LOLF_92.Dontab_RetourAppelTable(e.ControleAppelant) = e.Valeur
                MultiPER.SetActiveView(VueLOLF)
            Case "VueAffectation"
                PER_AFFECTATION_17.Dontab_RetourAppelTable(e.ControleAppelant) = e.Valeur
                MultiPER.SetActiveView(VueAffectation)
            Case "VueAdressePro"
                PER_ADRESSEPRO_24.Dontab_RetourAppelTable(e.ControleAppelant) = e.Valeur
                MultiPER.SetActiveView(VueAdressePro)
            Case "VueCaisse"
                PER_AFFILIATION_27.Dontab_RetourAppelTable(e.ControleAppelant) = e.Valeur
                MultiPER.SetActiveView(VueCaisse)
            Case "VueMutuelle"
                PER_MUTUELLE_59.Dontab_RetourAppelTable(e.ControleAppelant) = e.Valeur
                MultiPER.SetActiveView(VueMutuelle)
            Case "VueAbsence"
                PER_ABSENCE_15.Dontab_RetourAppelTable(e.ControleAppelant) = e.Valeur
                MultiPER.SetActiveView(VueAbsence)
            Case "VueStatut"
                PER_STATUT_12.Dontab_RetourAppelTable(e.ControleAppelant) = e.Valeur
                MultiPER.SetActiveView(VueStatut)
            Case "VuePosition"
                PER_POSITION_13.Dontab_RetourAppelTable(e.ControleAppelant) = e.Valeur
                MultiPER.SetActiveView(VuePosition)
            Case "VueGrade"
                HPopupRefGrade.Value = "0"
                CellRefGrade.Visible = False
                If e.ControleAppelant = "Dontab01" Then
                    PER_GRADE_14.Dontab_RetourAppelTable(e.ControleAppelant) = e.Identifiant & VI.PointVirgule & e.Valeur
                Else
                    PER_GRADE_14.Dontab_RetourAppelTable(e.ControleAppelant) = e.Valeur
                End If
                MultiPER.SetActiveView(VueGrade)
            Case "VuePrime"
                PER_INDEMNITE_21.Dontab_RetourAppelTable(e.ControleAppelant) = e.Valeur
                MultiPER.SetActiveView(VuePrime)
            Case "VueStatutDetache"
                PER_STATUT_54.Dontab_RetourAppelTable(e.ControleAppelant) = e.Valeur
                MultiPER.SetActiveView(VueStatutDetache)
            Case "VuePositionDetache"
                PER_POSITION_55.Dontab_RetourAppelTable(e.ControleAppelant) = e.Valeur
                MultiPER.SetActiveView(VuePositionDetache)
            Case "VueGradeDetache"
                HPopupRefGrade.Value = "0"
                CellRefGrade.Visible = False
                If e.ControleAppelant = "Dontab01" Then
                    PER_GRADE_56.Dontab_RetourAppelTable(e.ControleAppelant) = e.Identifiant & VI.PointVirgule & e.Valeur
                Else
                    PER_GRADE_56.Dontab_RetourAppelTable(e.ControleAppelant) = e.Valeur
                End If
                MultiPER.SetActiveView(VueGradeDetache)
            Case "VueVariablePaie"
                PER_VARIABLE_PAIE_67.Dontab_RetourAppelTable(e.ControleAppelant) = e.Valeur
                MultiPER.SetActiveView(VueVariablePaie)
            Case "VueSifac"
                PER_SIFAC_180.Dontab_RetourAppelTable(e.ControleAppelant, e.Code) = e.Valeur
                MultiPER.SetActiveView(VueSifac)
        End Select
        WebFct.PointeurContexte.Fenetre_VueActive(VI.CategorieRH.InfosPersonnelles) = MultiPER.ActiveViewIndex
    End Sub

    Protected Sub PER_AppelTable(ByVal sender As Object, ByVal e As Virtualia.Systeme.Evenements.AppelTableEventArgs) _
    Handles PER_ETATCIVIL_1.AppelTable, PER_DOMICILE_4.AppelTable, PER_COLLECTIVITE_26.AppelTable, PER_LOLF_92.AppelTable,
        PER_AFFECTATION_17.AppelTable, PER_ADRESSEPRO_24.AppelTable, PER_AFFILIATION_27.AppelTable, PER_MUTUELLE_59.AppelTable, PER_ABSENCE_15.AppelTable,
        PER_STATUT_12.AppelTable, PER_POSITION_13.AppelTable, PER_GRADE_14.AppelTable, PER_INDEMNITE_21.AppelTable, PER_VARIABLE_PAIE_67.AppelTable,
        PER_BANQUE_5.AppelTable, PER_STATUT_54.AppelTable, PER_POSITION_55.AppelTable, PER_GRADE_56.AppelTable, PER_ADMORIGINE_25.AppelTable, PER_SIFAC_180.AppelTable

        Select Case e.ObjetAppelant
            Case VI.ObjetPer.ObaCivil
                WebFct.PointeurContexte.SysRef_IDVueRetour = VueEtatCivil.ID
            Case VI.ObjetPer.ObaAdresse
                WebFct.PointeurContexte.SysRef_IDVueRetour = VueAdresse.ID
            Case VI.ObjetPer.ObaBanque
                WebFct.PointeurContexte.SysRef_IDVueRetour = VueBanque.ID
            Case VI.ObjetPer.ObaSociete
                WebFct.PointeurContexte.SysRef_IDVueRetour = VueAdministration.ID
            Case VI.ObjetPer.ObaOrigine
                WebFct.PointeurContexte.SysRef_IDVueRetour = VueAdmOrigine.ID
            Case VI.ObjetPer.ObaLolf
                WebFct.PointeurContexte.SysRef_IDVueRetour = VueLOLF.ID
            Case VI.ObjetPer.ObaOrganigramme
                WebFct.PointeurContexte.SysRef_IDVueRetour = VueAffectation.ID
            Case VI.ObjetPer.ObaAdrPro
                WebFct.PointeurContexte.SysRef_IDVueRetour = VueAdressePro.ID
            Case VI.ObjetPer.ObaCaisse
                WebFct.PointeurContexte.SysRef_IDVueRetour = VueCaisse.ID
            Case VI.ObjetPer.ObaMutuelle
                WebFct.PointeurContexte.SysRef_IDVueRetour = VueMutuelle.ID
            Case VI.ObjetPer.ObaAbsence
                WebFct.PointeurContexte.SysRef_IDVueRetour = VueAbsence.ID
            Case VI.ObjetPer.ObaStatut
                WebFct.PointeurContexte.SysRef_IDVueRetour = VueStatut.ID
            Case VI.ObjetPer.ObaActivite
                WebFct.PointeurContexte.SysRef_IDVueRetour = VuePosition.ID
            Case VI.ObjetPer.ObaGrade
                WebFct.PointeurContexte.SysRef_IDVueRetour = VueGrade.ID
            Case VI.ObjetPer.ObaIndemnite
                WebFct.PointeurContexte.SysRef_IDVueRetour = VuePrime.ID
            Case VI.ObjetPer.ObaHeuresSup
                WebFct.PointeurContexte.SysRef_IDVueRetour = VueHeureSup.ID
            Case VI.ObjetPer.ObaStatutDetache
                WebFct.PointeurContexte.SysRef_IDVueRetour = VueStatutDetache.ID
            Case VI.ObjetPer.ObaPositionDetache
                WebFct.PointeurContexte.SysRef_IDVueRetour = VuePositionDetache.ID
            Case VI.ObjetPer.ObaGradeDetache
                WebFct.PointeurContexte.SysRef_IDVueRetour = VueGradeDetache.ID
            Case VI.ObjetPer.ObaVariablePaie
                WebFct.PointeurContexte.SysRef_IDVueRetour = VueVariablePaie.ID
            Case 180
                WebFct.PointeurContexte.SysRef_IDVueRetour = VueSifac.ID
            Case Else
                Exit Sub
        End Select
        Select Case e.ObjetAppelant
            Case VI.ObjetPer.ObaGrade, VI.ObjetPer.ObaGradeDetache
                If e.ControleAppelant = "Dontab01" Then
                    RefGradeGrilles.V_PointdeVue = e.PointdeVueInverse
                    RefGradeGrilles.V_NomTable = ""
                    RefGradeGrilles.V_Valeur_Selectionnee = e.NomdelaTable
                    RefGradeGrilles.V_Appelant(2, e.DatedeValeur) = e.ControleAppelant
                    HPopupRefGrade.Value = "1"
                    CellRefGrade.Visible = True
                    PopupRefGrade.Show()
                    Exit Sub
                End If
        End Select

        RefVirtualia.V_PointdeVue = e.PointdeVueInverse
        RefVirtualia.V_NomTable = e.NomdelaTable
        Select Case e.ObjetAppelant
            Case VI.ObjetPer.ObaCivil
                If e.ControleAppelant = "Dontab06" Then
                    RefVirtualia.V_DuoTable(e.PointdeVueInverse, e.NomdelaTable, e.PointdeVueInverse) = "Pays"
                End If
                RefVirtualia.V_Appelant(e.ObjetAppelant) = e.ControleAppelant
        End Select
        RefVirtualia.V_Appelant(e.ObjetAppelant) = e.ControleAppelant
        HPopupRef.Value = "1"
        CellReference.Visible = True
        PopupReferentiel.Show()
    End Sub

    Protected Sub MsgVirtualia_ValeurRetour(ByVal sender As Object, ByVal e As Virtualia.Systeme.Evenements.MessageRetourEventArgs) Handles MsgVirtualia.ValeurRetour
        HPopupMsg.Value = "0"
        CellMessage.Visible = False
        Select Case e.Emetteur
            Case Is = "SuppFiche"
                Select Case e.NumeroObjet
                    Case VI.ObjetPer.ObaEnfant
                        PER_ENFANT_3.RetourDialogueSupp(e.ReponseMsg) = e.ChaineDatas
                        MultiPER.SetActiveView(VueEnfant)
                    Case VI.ObjetPer.ObaStatut
                        PER_STATUT_12.RetourDialogueSupp(e.ReponseMsg) = e.ChaineDatas
                        MultiPER.SetActiveView(VueStatut)
                    Case VI.ObjetPer.ObaActivite
                        PER_POSITION_13.RetourDialogueSupp(e.ReponseMsg) = e.ChaineDatas
                        MultiPER.SetActiveView(VuePosition)
                    Case VI.ObjetPer.ObaGrade
                        PER_GRADE_14.RetourDialogueSupp(e.ReponseMsg) = e.ChaineDatas
                        MultiPER.SetActiveView(VueGrade)
                    Case VI.ObjetPer.ObaAbsence
                        PER_ABSENCE_15.RetourDialogueSupp(e.ReponseMsg) = e.ChaineDatas
                        MultiPER.SetActiveView(VueAbsence)
                    Case VI.ObjetPer.ObaOrganigramme
                        PER_AFFECTATION_17.RetourDialogueSupp(e.ReponseMsg) = e.ChaineDatas
                        MultiPER.SetActiveView(VueAffectation)
                    Case VI.ObjetPer.ObaIndemnite
                        PER_INDEMNITE_21.RetourDialogueSupp(e.ReponseMsg) = e.ChaineDatas
                        MultiPER.SetActiveView(VuePrime)
                    Case VI.ObjetPer.ObaSociete
                        PER_COLLECTIVITE_26.RetourDialogueSupp(e.ReponseMsg) = e.ChaineDatas
                        MultiPER.SetActiveView(VueAdministration)
                    Case VI.ObjetPer.ObaHeuresSup
                        PER_HEURESUP_61.RetourDialogueSupp(e.ReponseMsg) = e.ChaineDatas
                        MultiPER.SetActiveView(VueHeureSup)
                    Case VI.ObjetPer.ObaLolf
                        PER_LOLF_92.RetourDialogueSupp(e.ReponseMsg) = e.ChaineDatas
                        MultiPER.SetActiveView(VueLOLF)
                    Case VI.ObjetPer.ObaStatutDetache
                        PER_STATUT_54.RetourDialogueSupp(e.ReponseMsg) = e.ChaineDatas
                        MultiPER.SetActiveView(VueStatutDetache)
                    Case VI.ObjetPer.ObaPositionDetache
                        PER_POSITION_55.RetourDialogueSupp(e.ReponseMsg) = e.ChaineDatas
                        MultiPER.SetActiveView(VuePositionDetache)
                    Case VI.ObjetPer.ObaGradeDetache
                        PER_GRADE_56.RetourDialogueSupp(e.ReponseMsg) = e.ChaineDatas
                        MultiPER.SetActiveView(VueGradeDetache)
                    Case VI.ObjetPer.ObaVariablePaie
                        PER_VARIABLE_PAIE_67.RetourDialogueSupp(e.ReponseMsg) = e.ChaineDatas
                        MultiPER.SetActiveView(VueVariablePaie)
                    Case 180
                        PER_SIFAC_180.RetourDialogueSupp(e.ReponseMsg) = e.ChaineDatas
                        MultiPER.SetActiveView(VueSifac)
                End Select
            Case Is = "SuppDossier"
                Exit Sub
            Case Is = "MajDossier"
                Select Case e.NumeroObjet
                    Case VI.ObjetPer.ObaCivil
                        PER_ETATCIVIL_1.V_RetourDialogueMaj(e.ReponseMsg) = e.ChaineDatas
                        MultiPER.SetActiveView(VueEtatCivil)
                    Case VI.ObjetPer.ObaEnfant
                        PER_ENFANT_3.V_RetourDialogueMaj(e.ReponseMsg) = e.ChaineDatas
                        MultiPER.SetActiveView(VueEnfant)
                    Case VI.ObjetPer.ObaAdresse
                        PER_DOMICILE_4.V_RetourDialogueMaj(e.ReponseMsg) = e.ChaineDatas
                        MultiPER.SetActiveView(VueAdresse)
                    Case VI.ObjetPer.ObaBanque
                        PER_BANQUE_5.V_RetourDialogueMaj(e.ReponseMsg) = e.ChaineDatas
                        MultiPER.SetActiveView(VueBanque)
                    Case VI.ObjetPer.ObaStatut
                        PER_STATUT_12.V_RetourDialogueMaj(e.ReponseMsg) = e.ChaineDatas
                        MultiPER.SetActiveView(VueStatut)
                    Case VI.ObjetPer.ObaActivite
                        PER_POSITION_13.V_RetourDialogueMaj(e.ReponseMsg) = e.ChaineDatas
                        MultiPER.SetActiveView(VuePosition)
                    Case VI.ObjetPer.ObaGrade
                        PER_GRADE_14.V_RetourDialogueMaj(e.ReponseMsg) = e.ChaineDatas
                        MultiPER.SetActiveView(VueGrade)
                    Case VI.ObjetPer.ObaAbsence
                        PER_ABSENCE_15.V_RetourDialogueMaj(e.ReponseMsg) = e.ChaineDatas
                        MultiPER.SetActiveView(VueAbsence)
                    Case VI.ObjetPer.ObaOrganigramme
                        PER_AFFECTATION_17.V_RetourDialogueMaj(e.ReponseMsg) = e.ChaineDatas
                        MultiPER.SetActiveView(VueAffectation)
                    Case VI.ObjetPer.ObaAdrPro
                        PER_ADRESSEPRO_24.V_RetourDialogueMaj(e.ReponseMsg) = e.ChaineDatas
                        MultiPER.SetActiveView(VueAdressePro)
                    Case VI.ObjetPer.ObaIndemnite
                        PER_INDEMNITE_21.V_RetourDialogueMaj(e.ReponseMsg) = e.ChaineDatas
                        MultiPER.SetActiveView(VuePrime)
                    Case VI.ObjetPer.ObaSociete
                        PER_COLLECTIVITE_26.V_RetourDialogueMaj(e.ReponseMsg) = e.ChaineDatas
                        MultiPER.SetActiveView(VueAdministration)
                    Case VI.ObjetPer.ObaCaisse
                        PER_AFFILIATION_27.V_RetourDialogueMaj(e.ReponseMsg) = e.ChaineDatas
                        MultiPER.SetActiveView(VueCaisse)
                    Case VI.ObjetPer.ObaMutuelle
                        PER_MUTUELLE_59.V_RetourDialogueMaj(e.ReponseMsg) = e.ChaineDatas
                        MultiPER.SetActiveView(VueMutuelle)
                    Case VI.ObjetPer.ObaHeuresSup
                        PER_HEURESUP_61.V_RetourDialogueMaj(e.ReponseMsg) = e.ChaineDatas
                        MultiPER.SetActiveView(VueHeureSup)
                    Case VI.ObjetPer.ObaLolf
                        PER_LOLF_92.V_RetourDialogueMaj(e.ReponseMsg) = e.ChaineDatas
                        MultiPER.SetActiveView(VueLOLF)
                    Case VI.ObjetPer.ObaStatutDetache
                        PER_STATUT_54.V_RetourDialogueMaj(e.ReponseMsg) = e.ChaineDatas
                        MultiPER.SetActiveView(VueStatutDetache)
                    Case VI.ObjetPer.ObaPositionDetache
                        PER_POSITION_55.V_RetourDialogueMaj(e.ReponseMsg) = e.ChaineDatas
                        MultiPER.SetActiveView(VuePositionDetache)
                    Case VI.ObjetPer.ObaGradeDetache
                        PER_GRADE_56.V_RetourDialogueMaj(e.ReponseMsg) = e.ChaineDatas
                        MultiPER.SetActiveView(VueGradeDetache)
                    Case VI.ObjetPer.ObaOrigine
                        PER_ADMORIGINE_25.V_RetourDialogueMaj(e.ReponseMsg) = e.ChaineDatas
                        MultiPER.SetActiveView(VueAdmOrigine)
                    Case 180
                        PER_SIFAC_180.V_RetourDialogueMaj(e.ReponseMsg) = e.ChaineDatas
                        MultiPER.SetActiveView(VueSifac)
                End Select
        End Select
    End Sub

    Protected Sub MessageDialogue(ByVal sender As Object, ByVal e As Virtualia.Systeme.Evenements.MessageSaisieEventArgs) _
    Handles PER_ETATCIVIL_1.MessageDialogue, PER_ENFANT_3.MessageDialogue, PER_DOMICILE_4.MessageDialogue,
    PER_BANQUE_5.MessageDialogue, PER_STATUT_12.MessageDialogue, PER_POSITION_13.MessageDialogue,
    PER_GRADE_14.MessageDialogue, PER_ABSENCE_15.MessageDialogue, PER_AFFECTATION_17.MessageDialogue, PER_ADRESSEPRO_24.MessageDialogue,
    PER_INDEMNITE_21.MessageDialogue, PER_AFFILIATION_27.MessageDialogue, PER_COLLECTIVITE_26.MessageDialogue,
    PER_MUTUELLE_59.MessageDialogue, PER_HEURESUP_61.MessageDialogue, PER_LOLF_92.MessageDialogue,
    PER_STATUT_54.MessageDialogue, PER_POSITION_55.MessageDialogue, PER_GRADE_56.MessageDialogue, PER_ADMORIGINE_25.MessageDialogue,
    PER_VARIABLE_PAIE_67.MessageDialogue, PER_SIFAC_180.MessageDialogue
        HPopupMsg.Value = "1"
        CellMessage.Visible = True
        MsgVirtualia.AfficherMessage = e
        PopupMsg.Show()
    End Sub

    Protected Sub MessageSaisie(ByVal sender As Object, ByVal e As Virtualia.Systeme.Evenements.MessageSaisieEventArgs) _
    Handles PER_ETATCIVIL_1.MessageSaisie, PER_ENFANT_3.MessageSaisie, PER_DOMICILE_4.MessageSaisie,
    PER_BANQUE_5.MessageSaisie, PER_STATUT_12.MessageSaisie, PER_POSITION_13.MessageSaisie, PER_ADRESSEPRO_24.MessageSaisie,
    PER_GRADE_14.MessageSaisie, PER_ABSENCE_15.MessageSaisie, PER_AFFECTATION_17.MessageSaisie, PER_INDEMNITE_21.MessageSaisie,
    PER_AFFILIATION_27.MessageSaisie, PER_COLLECTIVITE_26.MessageSaisie, PER_MUTUELLE_59.MessageSaisie,
    PER_HEURESUP_61.MessageSaisie, PER_LOLF_92.MessageSaisie, PER_STATUT_54.MessageSaisie,
    PER_POSITION_55.MessageSaisie, PER_GRADE_56.MessageSaisie, PER_ADMORIGINE_25.MessageSaisie, PER_VARIABLE_PAIE_67.MessageSaisie, PER_SIFAC_180.MessageSaisie
        HPopupMsg.Value = "0"
        CellMessage.Visible = False
    End Sub

End Class