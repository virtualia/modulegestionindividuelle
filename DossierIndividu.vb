﻿Option Strict On
Option Explicit On
Option Compare Text
Imports VI = Virtualia.Systeme.Constantes
Imports Virtualia.Systeme.MetaModele
Imports Virtualia.TablesObjet.ShemaPER

Namespace Individuel
    Public Class DossierIndividu
        Inherits List(Of VIR_FICHE)
        Private WsParent As Individuel.EnsembleDossiers
        Private WsIdentifiant As Integer

        Public Property Identifiant() As Integer
            Get
                Return WsIdentifiant
            End Get
            Set(ByVal value As Integer)
                Dim Lst As List(Of VIR_FICHE)
                Me.Clear()
                WsIdentifiant = value
                Lst = WsParent.PointeurGlobal.VirServiceServeur.LectureDossier_ToFiches(WsParent.PointeurGlobal.VirNomUtilisateur, VI.PointdeVue.PVueApplicatif, WsIdentifiant, False, WsParent.ObjetsDllIndividu)
                If Lst IsNot Nothing Then
                    Me.AddRange(Lst)
                    Call Complementer_Fiches()
                End If
            End Set
        End Property

        Public ReadOnly Property FicheEtatCivil As PER_ETATCIVIL
            Get
                Dim LstFiches As List(Of VIR_FICHE)
                LstFiches = Liste_Fiches("PER_ETATCIVIL")
                If LstFiches Is Nothing Then
                    Return Nothing
                End If
                Return CType(LstFiches.Item(0), PER_ETATCIVIL)
            End Get
        End Property

        Public Property ListeEnfants As List(Of PER_ENFANT)
            Get
                Dim LstFiches As List(Of VIR_FICHE)
                Dim LstEnfants As List(Of PER_ENFANT)
                Dim FicheBase As PER_ENFANT
                LstFiches = Liste_Fiches("PER_ENFANT")
                If LstFiches Is Nothing Then
                    Return Nothing
                End If
                '** Spécifique - Traitement des jumeaux 
                LstEnfants = New List(Of PER_ENFANT)
                For Each Element In LstFiches
                    FicheBase = New PER_ENFANT
                    FicheBase.Ide_Dossier = Element.Ide_Dossier
                    FicheBase.Date_de_naissance = CType(Element, PER_ENFANT).Date_de_naissance
                    FicheBase.Nom = CType(Element, PER_ENFANT).Nom
                    FicheBase.Prenom = CType(Element, PER_ENFANT).Prenom
                    FicheBase.Sexe = CType(Element, PER_ENFANT).Sexe
                    LstEnfants.Add(FicheBase)
                    If CType(Element, PER_ENFANT).Jumeau <> "" Then
                        FicheBase = New PER_ENFANT
                        FicheBase.Ide_Dossier = Element.Ide_Dossier
                        FicheBase.Date_de_naissance = CType(Element, PER_ENFANT).Date_de_naissance
                        FicheBase.Nom = CType(Element, PER_ENFANT).Nom
                        FicheBase.Prenom = CType(Element, PER_ENFANT).Jumeau
                        FicheBase.Sexe = CType(Element, PER_ENFANT).Sexe_du_jumeau
                        LstEnfants.Add(FicheBase)
                    End If
                    If CType(Element, PER_ENFANT).Triple <> "" Then
                        FicheBase = New PER_ENFANT
                        FicheBase.Ide_Dossier = Element.Ide_Dossier
                        FicheBase.Date_de_naissance = CType(Element, PER_ENFANT).Date_de_naissance
                        FicheBase.Nom = CType(Element, PER_ENFANT).Nom
                        FicheBase.Prenom = CType(Element, PER_ENFANT).Triple
                        FicheBase.Sexe = CType(Element, PER_ENFANT).Sexe_du_Triple
                        LstEnfants.Add(FicheBase)
                    End If
                Next
                Return WsParent.PointeurGlobal.VirRhFonction.ConvertisseurListeFiches(Of PER_ENFANT)(LstFiches)
            End Get
            Set(value As List(Of PER_ENFANT))
                Dim LstEnfants As List(Of PER_ENFANT)
                Dim LstFiches As List(Of VIR_FICHE)
                Dim FichePER As PER_ENFANT = Nothing
                Dim IndiceI As Integer

                If value Is Nothing Then
                    Exit Property
                End If
                '** Spécifique - Traitement des jumeaux 
                LstEnfants = New List(Of PER_ENFANT)
                For Each Element In value
                    If FichePER Is Nothing OrElse Element.Date_de_naissance <> FichePER.Date_de_naissance Then
                        FichePER = New PER_ENFANT
                        FichePER.Ide_Dossier = Element.Ide_Dossier
                        FichePER.Date_de_naissance = Element.Date_de_naissance
                        FichePER.Nom = Element.Nom
                        FichePER.Prenom = Element.Prenom
                        FichePER.Sexe = Element.Sexe
                        LstEnfants.Add(FichePER)
                    ElseIf FichePER.Jumeau = "" Then
                        FichePER.Jumeau = Element.Prenom
                        FichePER.Sexe_du_jumeau = Element.Sexe
                    ElseIf FichePER.Triple = "" Then
                        FichePER.Triple = Element.Prenom
                        FichePER.Sexe_du_Triple = Element.Sexe
                    End If
                Next
                LstFiches = Liste_Fiches("PER_ENFANT")
                If LstFiches Is Nothing Then
                    For Each FichePER In LstEnfants
                        Me.Add(FichePER)
                    Next
                    Exit Property
                End If
                For IndiceI = 0 To LstEnfants.Count - 1
                    If Fiche_ParRang(VI.ObjetPer.ObaEnfant, IndiceI) Is Nothing Then
                        Me.Add(FichePER)
                    Else
                        FichePER = CType(Fiche_ParRang(VI.ObjetPer.ObaEnfant, IndiceI), PER_ENFANT)
                        FichePER.Date_de_naissance = LstEnfants.Item(IndiceI).Date_de_naissance
                        FichePER.Nom = LstEnfants.Item(IndiceI).Nom
                        FichePER.Prenom = LstEnfants.Item(IndiceI).Prenom
                        FichePER.Sexe = LstEnfants.Item(IndiceI).Sexe
                        FichePER.Jumeau = LstEnfants.Item(IndiceI).Prenom
                        FichePER.Sexe_du_jumeau = LstEnfants.Item(IndiceI).Sexe
                        FichePER.Triple = LstEnfants.Item(IndiceI).Prenom
                        FichePER.Sexe_du_Triple = LstEnfants.Item(IndiceI).Sexe
                    End If
                Next
            End Set
        End Property

        Public ReadOnly Property DonneeLue(ByVal NumObjet As Integer, ByVal NumInfo As Integer) As String
            Get
                Dim LstFiches As List(Of VIR_FICHE)
                LstFiches = Liste_Fiches(NumObjet)
                If LstFiches Is Nothing OrElse LstFiches.Count = 0 Then
                    Return ""
                End If
                Return LstFiches.Item(0).V_TableauData(NumInfo).ToString
            End Get
        End Property

        Public ReadOnly Property DonneeLue(ByVal NumObjet As Integer, ByVal NumInfo As Integer, ByVal NoRang As Integer) As String
            Get
                Dim LstFiches As List(Of VIR_FICHE)
                LstFiches = Liste_Fiches(NumObjet)
                If LstFiches Is Nothing OrElse LstFiches.Count = 0 Then
                    Return ""
                End If
                If NoRang > LstFiches.Count - 1 Then
                    Return ""
                End If
                Return LstFiches.Item(NoRang).V_TableauData(NumInfo).ToString
            End Get
        End Property

        Public WriteOnly Property TableauMaj(ByVal NumObjet As Integer, ByVal NumInfo As Integer) As String
            Set(value As String)
                Dim LstFiches As List(Of VIR_FICHE)
                Dim FicheMAJ As VIR_FICHE
                Dim TableauData As ArrayList
                Dim TableauMaj As ArrayList
                Dim IndiceI As Integer

                LstFiches = Liste_Fiches(NumObjet)
                If LstFiches Is Nothing OrElse LstFiches.Count = 0 Then
                    Dim ConstructeurFiche As VConstructeur = New VConstructeur(WsParent.PointeurGlobal.VirModele.InstanceProduit.ClefModele)
                    FicheMAJ = ConstructeurFiche.V_NouvelleFiche(NumObjet, WsIdentifiant)
                    Me.Add(FicheMAJ)
                Else
                    FicheMAJ = LstFiches.Item(0)
                End If
                TableauData = FicheMAJ.V_TableauData
                TableauData(NumInfo) = value
                Select Case WsParent.NatureObjet(VI.PointdeVue.PVueApplicatif, NumObjet, NumInfo)
                    Case VI.TypeObjet.ObjetDate, VI.TypeObjet.ObjetDateRang, VI.TypeObjet.ObjetHeure
                        FicheMAJ.V_TableauData = TableauData
                    Case Else
                        TableauMaj = New ArrayList
                        For IndiceI = 1 To TableauData.Count - 1
                            TableauMaj.Add(TableauData(IndiceI))
                        Next IndiceI
                        FicheMAJ.V_TableauData = TableauMaj
                End Select
            End Set
        End Property

        Public WriteOnly Property TableauMaj(ByVal NumObjet As Integer, ByVal NumInfo As Integer, ByVal NoRang As Integer) As String
            Set(value As String)
                Dim LstFiches As List(Of VIR_FICHE)
                Dim FicheMAJ As VIR_FICHE
                Dim ConstructeurFiche As VConstructeur
                Dim TableauData As ArrayList

                LstFiches = Liste_Fiches(NumObjet)
                If LstFiches Is Nothing OrElse LstFiches.Count = 0 Then
                    ConstructeurFiche = New VConstructeur(WsParent.PointeurGlobal.VirModele.InstanceProduit.ClefModele)
                    FicheMAJ = ConstructeurFiche.V_NouvelleFiche(NumObjet, WsIdentifiant)
                    Me.Add(FicheMAJ)
                ElseIf NoRang > LstFiches.Count - 1 Then
                    ConstructeurFiche = New VConstructeur(WsParent.PointeurGlobal.VirModele.InstanceProduit.ClefModele)
                    FicheMAJ = ConstructeurFiche.V_NouvelleFiche(NumObjet, WsIdentifiant)
                    Me.Add(FicheMAJ)
                Else
                    FicheMAJ = LstFiches.Item(NoRang)
                End If
                TableauData = FicheMAJ.V_TableauData
                TableauData(NumInfo) = value
                FicheMAJ.V_TableauData = TableauData
            End Set
        End Property

        Public ReadOnly Property Liste_Fiches(ByVal NomTable As String) As List(Of VIR_FICHE)
            Get
                Return (From Fiche In Me Select Fiche Where Fiche.GetType.Name = NomTable).ToList
            End Get
        End Property

        Public ReadOnly Property Liste_Fiches(ByVal NumObjet As Integer) As List(Of VIR_FICHE)
            Get
                Return (From Fiche In Me Select Fiche Where Fiche.NumeroObjet = NumObjet).ToList
            End Get
        End Property

        Public ReadOnly Property Fiche_ParRang(ByVal NumObjet As Integer, ByVal NoRang As Integer) As VIR_FICHE
            Get
                Dim LstFiches As List(Of VIR_FICHE)
                LstFiches = Liste_Fiches(NumObjet)
                If LstFiches Is Nothing OrElse LstFiches.Count = 0 Then
                    Return Nothing
                End If
                If NoRang > LstFiches.Count - 1 Then
                    Return Nothing
                End If
                Return LstFiches.Item(NoRang)
            End Get
        End Property

        Private Sub Complementer_Fiches()
            Dim LstFiches As List(Of VIR_FICHE)
            Dim IndiceO As Integer
            Dim VdateFin As String

            LstFiches = Liste_Fiches("PER_ETATCIVIL")
            If LstFiches Is Nothing Then
                Exit Sub
            End If

            '** Positionnemnt des dates de fin virtuelles
            For IndiceO = 0 To Me.Count - 1
                Select Case Me.Item(IndiceO).GetType.Name
                    Case "PER_COLLECTIVITE"
                        If CType(Me.Item(IndiceO), PER_COLLECTIVITE).VDate_de_Fin = "" Then
                            Select Case Me.Item(IndiceO - 1).GetType.Name
                                Case Is = "PER_COLLECTIVITE"
                                    VdateFin = WsParent.PointeurGlobal.VirRhDates.CalcDateMoinsJour(CType(Me.Item(IndiceO - 1), PER_COLLECTIVITE).Date_d_effet, "0", "1")
                                    CType(Me.Item(IndiceO), PER_COLLECTIVITE).VDate_de_Fin = VdateFin
                            End Select
                        End If
                    Case "PER_SOCIETE"
                        If CType(Me.Item(IndiceO), PER_SOCIETE).VDate_de_Fin = "" Then
                            Select Case Me.Item(IndiceO - 1).GetType.Name
                                Case Is = "PER_SOCIETE"
                                    VdateFin = WsParent.PointeurGlobal.VirRhDates.CalcDateMoinsJour(CType(Me.Item(IndiceO - 1), PER_SOCIETE).Date_d_effet, "0", "1")
                                    CType(Me.Item(IndiceO), PER_SOCIETE).VDate_de_Fin = VdateFin
                            End Select
                        End If
                    Case "PER_STATUT"
                        If CType(Me.Item(IndiceO), PER_STATUT).VDate_de_Fin = "" Then
                            Select Case Me.Item(IndiceO - 1).GetType.Name
                                Case Is = "PER_STATUT"
                                    VdateFin = WsParent.PointeurGlobal.VirRhDates.CalcDateMoinsJour(CType(Me.Item(IndiceO - 1), PER_STATUT).Date_de_Valeur, "0", "1")
                                    CType(Me.Item(IndiceO), PER_STATUT).VDate_de_Fin = VdateFin
                            End Select
                        End If
                    Case "PER_CONTRAT"
                        If CType(Me.Item(IndiceO), PER_CONTRAT).VDate_de_Fin = "" Then
                            Select Case Me.Item(IndiceO - 1).GetType.Name
                                Case Is = "PER_CONTRAT"
                                    VdateFin = WsParent.PointeurGlobal.VirRhDates.CalcDateMoinsJour(CType(Me.Item(IndiceO), PER_CONTRAT).Date_de_Valeur, "0", "1")
                                    CType(Me.Item(IndiceO), PER_CONTRAT).VDate_de_Fin = VdateFin
                            End Select
                        End If
                    Case "PER_POSITION"
                        If CType(Me.Item(IndiceO), PER_POSITION).VDate_de_Fin = "" Then
                            Select Case Me.Item(IndiceO - 1).GetType.Name
                                Case Is = "PER_POSITION"
                                    VdateFin = WsParent.PointeurGlobal.VirRhDates.CalcDateMoinsJour(CType(Me.Item(IndiceO - 1), PER_POSITION).Date_de_Valeur, "0", "1")
                                    CType(Me.Item(IndiceO), PER_POSITION).VDate_de_Fin = VdateFin
                            End Select
                        End If
                    Case "PER_ACTIVITE"
                        If CType(Me.Item(IndiceO), PER_ACTIVITE).VDate_de_fin = "" Then
                            Select Case Me.Item(IndiceO - 1).GetType.Name
                                Case Is = "PER_ACTIVITE"
                                    VdateFin = WsParent.PointeurGlobal.VirRhDates.CalcDateMoinsJour(CType(Me.Item(IndiceO - 1), PER_ACTIVITE).Date_de_Valeur, "0", "1")
                                    CType(Me.Item(IndiceO), PER_ACTIVITE).VDate_de_fin = VdateFin
                            End Select
                        End If

                End Select
            Next IndiceO

        End Sub
        ''********AKR
        Public Function MettreAJour() As Boolean
            Dim SiOK As Boolean
            Dim ChaineLue As String
            Dim ChaineMaj As String
            Dim CodeMaj As String = ""
            Dim SiAfaire As Boolean

            If FicheEtatCivil Is Nothing Then
                Return False
            End If



            ''****** AKR 12/09/2017
            'If WsDateSignature <> "" Then
            '    Return False
            'End If

            'ChaineLue = ""
            'CodeMaj = "C"
            'If WsObjet_150.Ide_Dossier = WsIdentifiant Then
            '    ChaineLue = WsObjet_150.FicheLue
            '    CodeMaj = "M"
            'End If
            'If WsObjet_150.Identifiant_Manager = 0 Then
            '    Call InitialiserManager(WsObjet_150.Date_de_Valeur, 0)
            '    WsObjet_150.Identifiant_Manager = WsIdeManager
            '    If WsObjet_150.Evaluateur = "" Then
            '        WsObjet_150.Evaluateur = WsNomPrenomManager
            '    End If
            'End If
            'ChaineMaj = WsObjet_150.ContenuTable
            'If ChaineMaj <> ChaineLue Then
            '    ''***** AKR test
            '    'SiOK = WsPointeurGlobal.VirServiceServeur.MiseAjour_Fiche(WsNomUtiSGBD, VI.PointdeVue.PVueApplicatif, 150, WsIdentifiant, CodeMaj, ChaineLue, ChaineLue)
            '    SiOK = WsPointeurGlobal.VirServiceServeur.MiseAjour_Fiche(WsNomUtiSGBD, VI.PointdeVue.PVueApplicatif, 150, WsIdentifiant, CodeMaj, ChaineLue, ChaineMaj)
            '    If SiOK = False Then
            '        Return SiOK
            '    End If
            'End If
            'WsObjet_150.ContenuTable = WsIdentifiant.ToString & VI.Tild & ChaineMaj
            'If WsDateSignature = "" Then
            '    If WsObjet_150.Date_de_Signature_Evalue <> "" Then
            '        WsSiEMailAEnvoyer = True
            '    End If
            'End If


            Return SiOK
        End Function
        ''**** FIN AKR
        Public Sub New(ByVal Host As Individuel.EnsembleDossiers)
            WsParent = Host
        End Sub
    End Class
End Namespace