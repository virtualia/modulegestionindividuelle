﻿Option Explicit On
Option Strict Off
Option Compare Text
Namespace VCaches
    <Serializable>
    Public Class CacheCalendrier
        Private WsIdentifiant As Integer
        Private WsSelMois_Alpha As String = ""
        Private WsSelMois_Num As Integer = 1
        Private WsSelAnnee As Integer = 2016
        Private WsDateValeur As String = ""
        Private WsSelCoche As String = ""
        Private WsDateButoir As String = ""
        Private WsCocheButoir As String = ""

        Public Property Ide_Dossier As Integer
            Get
                Return WsIdentifiant
            End Get
            Set(value As Integer)
                WsIdentifiant = value
            End Set
        End Property

        Public Property Annee_Selection As Integer
            Get
                Return WsSelAnnee
            End Get
            Set(value As Integer)
                WsSelAnnee = value
            End Set
        End Property

        Public Property Mois_Selection As Integer
            Get
                Return WsSelMois_Num
            End Get
            Set(value As Integer)
                WsSelMois_Num = value
            End Set
        End Property

        Public Property Mois_Selection_Alpha As String
            Get
                Return WsSelMois_Alpha
            End Get
            Set(value As String)
                WsSelMois_Alpha = value
            End Set
        End Property

        Public Property Date_Valeur As String
            Get
                Return WsDateValeur
            End Get
            Set(value As String)
                WsDateValeur = value
            End Set
        End Property

        Public Property Date_Butoir As String
            Get
                Return WsDateButoir
            End Get
            Set(value As String)
                WsDateButoir = value
            End Set
        End Property

        Public Property Coche_Selection As String
            Get
                Return WsSelCoche
            End Get
            Set(value As String)
                WsSelCoche = value
            End Set
        End Property

        Public Property Coche_Butoir As String
            Get
                Return WsCocheButoir
            End Get
            Set(value As String)
                WsCocheButoir = value
            End Set
        End Property

        Public Sub Initialiser()
            WsSelMois_Alpha = ""
            WsDateValeur = ""
            WsDateButoir = ""
            WsCocheButoir = ""
            WsSelCoche = ""
        End Sub
    End Class
End Namespace