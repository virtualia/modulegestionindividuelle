﻿Option Explicit On
Option Strict Off
Option Compare Text
Namespace VCaches
    <Serializable>
    Public Class CacheArmoire
        Private WsTypeArmoire As String = ""
        Private WsIndex_Liste As Integer = 0
        Private WsObjet_Selection As String = ""
        Private WsIde_Selection As Integer = 0
        Private WsDataPath_Selection = ""
        Private WsListeCriteres As List(Of String)
        Private WsListeIde As List(Of Integer)
        Private WsListeCoches As List(Of String)
        Private WsFiltre As String = ""

        Public Property Type_Armoire As String
            Get
                Return WsTypeArmoire
            End Get
            Set(value As String)
                WsTypeArmoire = value
            End Set
        End Property

        Public Property Index_Liste As Integer
            Get
                Return WsIndex_Liste
            End Get
            Set(value As Integer)
                WsIndex_Liste = value
            End Set
        End Property

        Public Property Objet_Selection As String
            Get
                Return WsObjet_Selection
            End Get
            Set(value As String)
                WsObjet_Selection = value
            End Set
        End Property

        Public Property Ide_Selection As Integer
            Get
                Return WsIde_Selection
            End Get
            Set(value As Integer)
                WsIde_Selection = value
            End Set
        End Property

        Public Property DataPath_Selection As String
            Get
                Return WsDataPath_Selection
            End Get
            Set(value As String)
                WsDataPath_Selection = value
            End Set
        End Property

        Public Property ListeCriteres As List(Of String)
            Get
                Return WsListeCriteres
            End Get
            Set(value As List(Of String))
                WsListeCriteres = value
            End Set
        End Property

        Public Property ListeIde As List(Of Integer)
            Get
                Return WsListeIde
            End Get
            Set(value As List(Of Integer))
                WsListeIde = value
            End Set
        End Property

        Public Property ListeCoches As List(Of String)
            Get
                Return WsListeCoches
            End Get
            Set(value As List(Of String))
                WsListeCoches = value
            End Set
        End Property

        Public Property ValeurFiltre As String
            Get
                Return WsFiltre
            End Get
            Set(value As String)
                WsFiltre = value
            End Set
        End Property
    End Class
End Namespace