﻿Option Explicit On
Option Strict Off
Option Compare Text
Namespace VCaches
    <Serializable>
    Public Class CacheWebControle
        Private WsIdentifiant As Integer
        Private WsPointdeVue As Integer
        Private WsNumObjet As Integer
        Private WsIndexFiche As Integer
        Private WsClef As String = ""
        Private WsSiCreation As Boolean
        Private WsLstDatas As List(Of String) = Nothing
        '
        Private WsTabColonnes As List(Of Integer) = Nothing
        Private WsLstValeursParticulieres As List(Of String) = Nothing
        Private WsChaineLue As String = ""

        Public Property Ide_Dossier As Integer
            Get
                Return WsIdentifiant
            End Get
            Set(value As Integer)
                WsIdentifiant = value
            End Set
        End Property

        Public Property Point_de_Vue As Integer
            Get
                Return WsPointdeVue
            End Get
            Set(value As Integer)
                WsPointdeVue = value
            End Set
        End Property

        Public Property Numero_Objet As Integer
            Get
                Return WsNumObjet
            End Get
            Set(value As Integer)
                WsNumObjet = value
            End Set
        End Property

        Public Property Index_Fiche As Integer
            Get
                Return WsIndexFiche
            End Get
            Set(value As Integer)
                WsIndexFiche = value
            End Set
        End Property

        Public Property Clef As String
            Get
                Return WsClef
            End Get
            Set(value As String)
                WsClef = value
            End Set
        End Property

        Public Property SiCreation As Boolean
            Get
                Return WsSiCreation
            End Get
            Set(value As Boolean)
                WsSiCreation = value
            End Set
        End Property

        Public Property TabColonnes() As List(Of Integer)
            Get
                If WsTabColonnes Is Nothing Then
                    WsTabColonnes = New List(Of Integer)
                    Dim I As Integer
                    For I = 0 To 9
                        WsTabColonnes.Add(-1)
                    Next I
                End If
                Return WsTabColonnes
            End Get
            Set(value As List(Of Integer))
                WsTabColonnes = value
            End Set
        End Property

        Public Property Liste_Datas() As List(Of String)
            Get
                Return WsLstDatas
            End Get
            Set(value As List(Of String))
                WsLstDatas = value
            End Set
        End Property

        Public Property Liste_Valeurs_Particulieres() As List(Of String)
            Get
                Return WsLstValeursParticulieres
            End Get
            Set(value As List(Of String))
                WsLstValeursParticulieres = value
            End Set
        End Property

        Public Property Chaine_Lue As String
            Get
                Return WsChaineLue
            End Get
            Set(value As String)
                WsChaineLue = value
            End Set
        End Property
    End Class
End Namespace