﻿Option Strict On
Option Explicit On
Option Compare Text
Public Class VirtualiaMain
    Inherits System.Web.UI.MasterPage
    
    Public Function GetApplication() As HttpApplicationState
        Return Me.Application
    End Function

    Public Function GetSession() As HttpSessionState
        Return Me.Session
    End Function

    Public Property V_SelDate As String
        Get
            If Session.Item("SelDate") IsNot Nothing Then
                Return Session.Item("SelDate").ToString
            Else
                Return ""
            End If
        End Get
        Set(ByVal value As String)
            If Session.Item("SelDate") IsNot Nothing Then
                Session.Remove("SelDate")
            End If
            Session.Add("SelDate", value)
        End Set
    End Property

    Public Property V_SelEtablissement As String
        Get
            If Session.Item("SelEta") IsNot Nothing Then
                Return Session.Item("SelEta").ToString
            Else
                Return ""
            End If
        End Get
        Set(ByVal value As String)
            If Session.Item("SelEta") IsNot Nothing Then
                Session.Remove("SelEta")
            End If
            Session.Add("SelEta", value)
        End Set
    End Property

    Public Property V_SelNiveau1 As String
        Get
            If Session.Item("SelN1") IsNot Nothing Then
                Return Session.Item("SelN1").ToString
            Else
                Return ""
            End If
        End Get
        Set(ByVal value As String)
            If Session.Item("SelN1") IsNot Nothing Then
                Session.Remove("SelN1")
            End If
            Session.Add("SelN1", value)
        End Set
    End Property

    Public WriteOnly Property V_SiCadreCommandeVisible As Boolean
        Set(value As Boolean)
            CadreCommandes.Visible = value
        End Set
    End Property

    Public WriteOnly Property V_Info1 As String
        Set(value As String)
            EtiInfoN1.Text = value
        End Set
    End Property

    Public WriteOnly Property V_Info2 As String
        Set(value As String)
            If value = "(Tous)" Then
                EtiInfoN2.Text = ""
            Else
                EtiInfoN2.Text = value
            End If
        End Set
    End Property

    Public WriteOnly Property V_Info3 As String
        Set(value As String)
            If value = "(Tous)" Then
                EtiInfoN3.Text = ""
            Else
                EtiInfoN3.Text = value
            End If
        End Set
    End Property

    Private Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Dim WebFct As Virtualia.Net.Controles.WebFonctions = New Virtualia.Net.Controles.WebFonctions(Me, 2)
        Dim Chaine As String

        EtiDateJour.Text = WebFct.ViRhDates.ClairDate(WebFct.ViRhDates.DateduJour, True)
        EtiHeure.Text = Format(TimeOfDay, "t")
        If WebFct.PointeurUtilisateur Is Nothing Then
            Exit Sub
        End If
        EtiTitre.Text = "Module de gestion individuelle" 'WebFct.PointeurUtilisateur.PointeurContexte.TitreModuleActif
        EtiUtilisateur.Text = WebFct.PointeurGlobal.NomPrenomUtilisateur(Session.SessionID)
        CmdAccueil.PostBackUrl = WebFct.PointeurGlobal.UrlDestination(Session.SessionID, 0, "") 'Retour à l'accueil Virtualia.Net
        Chaine = "~/Fenetres/Commun/FrmAttente.aspx"
        Cmd01.PostBackUrl = Chaine & "?Index=1&Lien=~/Fenetres/Module/FrmInfosPersonnelles.aspx&Param=Armoire"
        Cmd02.PostBackUrl = Chaine & "?Index=2&Lien=~/Fenetres/Module/FrmInfosPersonnelles.aspx&Param=GestionIndividuelle"

    End Sub

    Private Sub HorlogeVirtuelle_Tick(ByVal sender As Object, ByVal e As System.EventArgs) Handles HorlogeVirtuelle.Tick
        Dim WebFct As Virtualia.Net.Controles.WebFonctions = New Virtualia.Net.Controles.WebFonctions(Me, 2)
        EtiDateJour.Text = WebFct.ViRhDates.ClairDate(WebFct.ViRhDates.DateduJour, True)
        EtiHeure.Text = Format(TimeOfDay, "t")
    End Sub

    Public WriteOnly Property Titre As String
        Set(value As String)

        End Set
    End Property

    Public ReadOnly Property WebFonction As Virtualia.Net.Controles.WebFonctions
        Get
            Dim WebFct As Virtualia.Net.Controles.WebFonctions = New Virtualia.Net.Controles.WebFonctions(Me, 2)
            Return WebFct
        End Get
    End Property
End Class