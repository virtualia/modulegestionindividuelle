﻿Public Class FrmInfosPersonnelles
    Inherits System.Web.UI.Page
    Private WebFct As Virtualia.Net.Controles.WebFonctions
    Private WsParam As String = ""
    Private WsDossierPer As Virtualia.Net.Individuel.DossierIndividu
    Private Enum Ivue As Integer
        IGestionIndividuelle = 0
        IArmoire = 1
        IVPER = 2
        IVDoss = 3
    End Enum
    Private ReadOnly Property V_WebFonction() As Virtualia.Net.Controles.WebFonctions
        Get
            If WebFct Is Nothing Then
                WebFct = New Virtualia.Net.Controles.WebFonctions(Me, 1)
            End If
            Return WebFct
        End Get
    End Property

    Private ReadOnly Property V_Contexte As Virtualia.Net.Individuel.LocalNavigation
        Get
            Return CType(V_WebFonction.PointeurGlobal, Virtualia.Net.Session.ObjetGlobal).ContexteSession(Session.SessionID)
        End Get
    End Property

    Private ReadOnly Property V_ListeDossiers As Virtualia.Net.Individuel.EnsembleDossiers
        Get
            Return CType(V_WebFonction.PointeurGlobal, Virtualia.Net.Session.ObjetGlobal).EnsemblePER
        End Get
    End Property

    Private ReadOnly Property V_DossierPer As Virtualia.Net.Individuel.DossierIndividu
        Get
            If V_Contexte Is Nothing Then
                Return Nothing
            End If
            Return V_Contexte.DossierPER
        End Get
    End Property
    Public Property V_Identifiant() As Integer
        Get
            Return CInt(HSelIde.Value)
        End Get
        Set(ByVal value As Integer)
            If value = CInt(HSelIde.Value) Then
                Exit Property
            End If
            Dim Chaine As String = ""
            HSelIde.Value = value.ToString
            If V_Contexte IsNot Nothing Then
                V_ListeDossiers.Identifiant(False) = value
                V_Contexte.DossierPER = V_ListeDossiers.ItemDossier(value)
                V_Contexte.Identifiant_Courant = value
                Chaine = V_Contexte.DossierPER.FicheEtatCivil.Qualite
                Chaine &= Strings.Space(1) & V_Contexte.DossierPER.FicheEtatCivil.Nom
                Chaine &= Strings.Space(1) & V_Contexte.DossierPER.FicheEtatCivil.Prenom
                Chaine &= " (Ide V : " & value & ")"
            End If
            EtiIdentite.Text = Chaine
        End Set
    End Property

    Private Sub FrmInfosPersonnelles_PreRender(sender As Object, e As EventArgs) Handles Me.PreRender
        If V_Contexte IsNot Nothing Then
            'V_Identifiant = V_Contexte.Identifiant_Courant
            'V_Identifiant = 12665 'REMOND
            'V_Identifiant = 20361  'BAPAUME Aprevenir

            If V_Identifiant = 0 Then
                If WsParam = "" Then
                    WsParam = Server.HtmlDecode(Request.QueryString("Param"))
                    'V_WebFonction.PointeurUtilisateur.V_PointeurContexte.ModuleActif = WsParam
                    Select Case WsParam
                        Case "Armoire"
                            MultiVues.ActiveViewIndex = Ivue.IArmoire
                        Case "GestionIndividuelle"
                            V_ListeDossiers.Identifiant(False) = 0
                            V_Contexte.DossierPER = V_ListeDossiers.ItemDossier(0)

                            V_Contexte.Identifiant_Courant = 0

                            '  MultiVues.ActiveViewIndex = Ivue.IVPER
                            ' MultiVues.ActiveViewIndex = Ivue.IVDoss
                            MultiVues.ActiveViewIndex = Ivue.IGestionIndividuelle
                        Case Else
                            MultiVues.ActiveViewIndex = Ivue.IArmoire
                    End Select
                End If

            End If
        End If

    End Sub

    Private Sub Armoire_Dossier_Click(ByVal sender As Object, ByVal e As Systeme.Evenements.DossierClickEventArgs) Handles ArmoirePER.Dossier_Click
        If e.Identifiant = 0 Then
            Exit Sub
        End If
        V_Identifiant = e.Identifiant
        VFenetrePER.V_Identifiant = V_Identifiant
        MultiVues.ActiveViewIndex = Ivue.IVPER

    End Sub
    Private Sub MenuChoix_MenuItemClick(sender As Object, e As MenuEventArgs) Handles MenuChoix.MenuItemClick
        Select Case e.Item.Value
            Case "DONPER"
                MultiOnglets.SetActiveView(VueGenerale)
            Case "DIPLOME"
                MultiOnglets.SetActiveView(VueDiplome)
            Case "SITADM"
                MultiOnglets.SetActiveView(VueSitAdm)
            Case "AFFECTATION"
                MultiOnglets.SetActiveView(VueAffectation)
            Case "FORMATION"
                MultiOnglets.SetActiveView(VueFormation)
        End Select
    End Sub

    Protected Sub CommandeOK_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles CommandeOK.Click
        Dim Cretour As Boolean

        WsDossierPer = V_Contexte.DossierPER
        If WsDossierPer Is Nothing Then
            Exit Sub
        End If

        Cretour = WsDossierPer.MettreAJour
        'If Cretour = True And (DossierPER.SiEMailAEnvoyer = True Or DossierPER.SiEtape1Valide = True Or DossierPER.SiEtape2Valide = True Or DossierPER.SiEtape3Valide = True Or DossierPER.SiEtape4Valide = True) Then
        '    V_WebFonction.ContexteSession(Session.SessionID).SiMailAfaire = True
        'End If

        ''**************

        'Dim TitreMsg As String = "Enregistrement des entretiens professionnels"
        'Dim Msg As String
        'Dim Evenement As Virtualia.Systeme.Evenements.MessageSaisieEventArgs
        'If Cretour = True Then
        '    Msg = "L'enregistrement a bien été effectué."
        'Else
        '    Msg = "Incident au moment de l'enregistrement."
        'End If
        'CadreDateJour.Visible = False
        'Evenement = New Virtualia.Systeme.Evenements.MessageSaisieEventArgs("MajDossier", MultiOnglets.ActiveViewIndex, DossierPER.LibelleIdentite, "OK", TitreMsg, Msg)
        'CadreSaisie.BackImageUrl = ""
        'CadreSaisie.BackColor = V_WebFonction.ConvertCouleur("#216B68")
        'HPopupMsg.Value = "1"
        'CellMessage.Visible = True
        'MsgVirtualia.AfficherMessage = Evenement
        'PopupMsg.Show()

        'Dim TableauErreur As List(Of String) = SiErreurSpecifique()
        'If TableauErreur Is Nothing Then
        '    Call V_MajFiche()
        '    CadreCmdOK.Visible = False

        '    Dim urlRelative As String
        '    urlRelative = Request.RawUrl

        '    If InStr(urlRelative, "NouveauDossier") > 0 Then
        '        Dim CacheData As List(Of String) = V_CacheMaj
        '        Dim TabOK As New List(Of String)
        '        TabOK.Add("Le dossier de " & CacheData.Item(2) & Strings.Space(1) & CacheData.Item(3))
        '        TabOK.Add(" a été créé.")
        '        Dim TitreMsg As String = "Nouveau dossier"
        '        Call V_MessageDialog(V_WebFonction.Evt_MessageInformatif(WsNumObjet, TitreMsg, TabOK, V_Identifiant))
        '    End If
        'Else
        '    Dim CacheData As List(Of String) = V_CacheMaj
        '    Dim TitreMsg As String = "Contrôle préalable à l'enregistrement du dossier de " & CacheData.Item(2) & Strings.Space(1) & CacheData.Item(3)
        '    Call V_MessageDialog(V_WebFonction.Evt_MessageBloquant(WsNumObjet, TitreMsg, TableauErreur))
        'End If


    End Sub

    ''***  AKR   
    Protected Sub MessageDialogue(ByVal sender As Object, ByVal e As Virtualia.Systeme.Evenements.MessageSaisieEventArgs) _
Handles Per_InfoGen.MessageDialogue
        HPopupMsg.Value = "1"
        CellMessage.Visible = True
        MsgVirtualia.AfficherMessage = e
        PopupMsg.Show()
    End Sub
    ''*** FIN AKR
End Class